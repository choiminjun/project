#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct PHONE
{
	char name[20], model[20], vendor[10],tele[5],screen[20], os[100],cpuType[200];
	float display, cpuClock, memory;
	int price;
	struct PHONE *next;

}PHONE;

struct Iname //index of name. it will be sorted by 'SortName' function.
{
	char name[20];
	PHONE *p;
}*in;
struct Iscreen//index of screen. it will be sorted by 'SortScreen' function.
{
	int scre;
	PHONE *p;
}*is;
struct Iprice//index of price. it will be sorted by 'SortPrice' function.
{
	int price;
	PHONE *p;
}*ip;
struct Idisplay//index of display. it will be sorted by 'SortDisplay' function.
{
	float disp;
	PHONE *p;
}*id;
struct Iclock//index of cpuClock. it will be sorted by 'SortClock' function.
{
	float clock;
	PHONE *p;
}*ic;
struct Imemory//index of memory. it will be sorted by 'SortMemory' function.
{
	float mem;
	PHONE *p;
}*im;
	 /*
'Link' is the function to link the value maintaining order in file.
'Print' is the function to print all data in the linked list.
'Insert' is the function to insert a new data into the data structure.
'Exist' is the function to confirm whether informations are unique/exist or not.
'Delete' is the function to receive model name to delete existing data.
'Update' is the function to save to the smartphone file to allow the use of the latest data at the next launch of the program.
'Search' is the function that receives multiple search parameters and find all matched data.
'Screen' is the function to change screen resolution(char) to (int).
'SearchStar' is the funtion to find '*'. if '*' is found then call 'partialStar'function ,is not found then call 'partial'function.
'partialStar' is the function to search string using '~*~' (ex) Galaxy*S.
'partial' is the function to search string using '~' (ex) Galaxy.
	*/
void Link(PHONE *tmp, PHONE *head, PHONE**last);
void Print(PHONE *head);
void Insert(PHONE *head, PHONE **last);
int Exist(PHONE *head, PHONE *node);
void Delete(PHONE *head);
void Update(PHONE *head);
void Search(PHONE *head);
int Screen(char string[]);
int SearchStar(char string[], char find[]);
int partialStar(char string[], char find[]);
int partial(char string[], char find[]);

void Sort(int cnt,PHONE *head);//choose the contents you want to sort

//Below 6 functions are funtion to sort each index array! ex) SortName function sort name index array *in
void SortName(int N);
void SortScreen(int N);
void SortPrice(int N);
void SortDisplay(int N);
void SortClock(int N);
void SortMemory(int N);
//Below 3 functions are funtions to ASending Desending
void choice(int N, int num);//choice ascend or Descend.
void Ascend(int N);
void Descend(int N);

void main()
{
	PHONE tmp;
	FILE *f;
	int cnt, num;
	PHONE *last= NULL,*head = (PHONE*)malloc(sizeof(PHONE));//It will use in Linked list
	head->next = NULL;

	////[1]SETUP////
	//[1]a.Create a smartphone file.
	/*File open && depensive coding- file error is offen occured so we have to check it!*/
	if((f = fopen("total.txt","r")) == NULL){
		printf("file error\n");
		exit(1);
	}
	//[1]b.Read the file & Store the file in a linked list.
	/*Count the number of smart phone informations in file 'f'.*/
	cnt=0;
	while(fscanf(f,"%s , %s , %s , %s , %f , %s , %s , %s , %f , %f , %d"
		, tmp.name, tmp.model, tmp.vendor, tmp.tele
		, &tmp.display, tmp.screen, tmp.os, tmp.cpuType
		, &tmp.cpuClock, &tmp.memory, &tmp.price) != EOF)
	{			
		cnt++;
		Link(&tmp,head,&last);
	}
	/*- The smartphone file must include at least 100 smartphones.*/
	//printf("\n=====%d",cnt);//total number of smartphone information in original file is '102'
	fclose(f);
	num = -1;
	while(num != 0)
	{
		printf("===================MENU===================\n");
		printf("		1.Insert\n		2.Delete\n		3.Search\n		4.Sort\n		5.Print All\n		0.Exit\n");
		printf("==========================================\n");
		printf("Input number : ");
		scanf("%d",&num);
		if(num == 1){
			//[2]a.Write a function that receives necessary information to insert a new data into the data structure.
			Insert(head, &last);
			Print(head);
			Update(head);
		}
		else if(num == 2){
			//[2]b.Write a function that receives model name to delete existing data from the data structure.
			Delete(head);
			Update(head);
		}
		else if(num == 3){
			//[3]a. Write a function that receives multiple search parameters and find all matched data.
			Search(head);
		}

		else if(num == 4){
			//[3]b. Write a function that receives key parameter for sorting and the order and print all data by given parameters.
			Sort(cnt,head);
		}
		else if(num == 5){
			Print(head);
		}
		else if(num == 0){
			break;
		}
		else{
			printf("You input wrong number\n");
		}	
	}
}
void Link(PHONE *tmp, PHONE *head, PHONE**last)
{
	PHONE *node;
	/*Malloc allocation && Depensive code*/
	if((node = (PHONE*)malloc(sizeof(PHONE))) == NULL){
		printf("malloc error\n");
		exit(1);
	}
	strcpy(node->name, tmp->name);
	strcpy(node->model, tmp->model);
	strcpy(node->vendor, tmp->vendor);
	strcpy(node->tele, tmp->tele);
	strcpy(node->screen , tmp->screen);	
	strcpy(node->os , tmp->os);
	strcpy(node->cpuType , tmp->cpuType);
	node->display = tmp->display;
	node->cpuClock = tmp->cpuClock;
	node->memory = tmp->memory;
	node->price = tmp->price;
	node->next = NULL;

	if(head->next == NULL)
		head->next = node;	
	else
		(*last)->next = node;		
	*last = node;
}
void Print(PHONE *head)
{
	PHONE *p = head->next;
	while(p){
		printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
			,p->name, p->model, p->vendor, p->tele
			, p->display, p->screen, p->os, p->cpuType
			, p->cpuClock, p->memory, p->price);
		p = p->next;
	}
}
	////[2] Insert & Delete functionalities////
void Insert(PHONE *head, PHONE** last)
{
	PHONE *node;
	/*Malloc allocation && Depensive code*/
	if((node = (PHONE*)malloc(sizeof(PHONE))) == NULL){
		printf("malloc error\n");
		exit(1);
	}
	printf("==>Input new information : \n");
	printf("NAME				: ");scanf("%s",node->name); 
	printf("MODEL				: ");scanf("%s", node->model); 
	printf("VENDOR				: ");scanf("%s", node->vendor); 
	printf("TELE_COMPANY		: ");scanf("%s", node->tele);
	printf("DISPLAY_SIZE(Float)	: ");scanf("%f", &node->display);
	printf("SCREEN_RESOLUTION	: ");scanf("%s", node->screen); 
	printf("OPERATING_SYSTEM	: ");scanf("%s", node->os); 
	printf("CPU_TYPE			: ");scanf("%s", node->cpuType); 
	printf("CPU_CLOCK(Float)	: ");scanf("%f", &node->cpuClock); 
	printf("MEMORY_SIZE(Float)	: ");scanf("%f", &node->memory); 
	printf("PRICE(INT)			: ");scanf("%d", &node->price); 
	node->next=NULL;

	//confirm whether informations are unique or not.
	if ( Exist(head, node) == 1){
		(*last)->next = node;
		*last = node;
	}
	//Keep a constraint that model name should be unique.
	else{
		printf("\n!!![Warning]Model name should be unique!!!\n");
		Insert(head, last);//
	}
}
int Exist(PHONE*head, PHONE *node)
{
	PHONE *p = head->next;
	while(p)
	{
		if(strcmp(p->model,node->model) == 0)
			return 0;
		p = p->next;
	}
	return 1;
}
void Delete(PHONE *head)
{
	PHONE *p = head->next, *prev = head;
	char key[20];
	printf("\nInput model name that you want to delete : \n");
	scanf("%s",key);

	while(p)
	{
		if( strcmp(p->model,key) == 0 )
		{
			prev->next = p->next;
			printf("!!!Delete %s is completed!!!\n",key);
			break;
		}
		prev = p;
		p = p->next;
	}
	if(p==NULL)//If the received model name does not exist, print error message.
		printf("!!!There is no model that you want to delete!!!\n");
}
void Update(PHONE *head)
{
	FILE *f;
	PHONE *p = head->next;
	if ( (f = fopen("total.txt","w")) == NULL)
	{
		printf("file error\n");
		exit(1);
	}
	while(p)
	{			
		fprintf(f,"%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
			,p->name, p->model, p->vendor, p->tele
			, p->display, p->screen, p->os, p->cpuType
			, p->cpuClock, p->memory, p->price);
		p = p->next;
	}

	fclose(f);
}
	////[3] Search & Sort functionalities////
void Search(PHONE *head)
{
	float minDisplay=-1, maxDisplay=-1, minCpuClock=-1, maxCpuClock=-1, minMemory=-1, maxMemory=-1;
	int minPrice=-1, maxPrice=-1;
	int minScreen=-1, maxScreen=-1;
	char fname[20]="-1", fmodel[20]="-1", fvendor[10]="-1",ftele[5]="-1", fos[100]="-1",fcpuType[200]="-1";
	char Ename[20]="-1";//f is find E is except
	struct PHONE* p = head->next;
	int num=-1;
		printf("==================SEARCH==================\n");
		printf("\n		1.Name\n		2.Model\n		3.Vendor\n		4.Tele_Company\n		5.Display\n		6.Screen\n		7.OS\n		8.CpuType\n		9.CpuClock\n		10.Memory\n		11.Price\n");
		printf("==========================================\n");

	while(num!=0)
	{
		printf("\nInput number : ");
		scanf("%d",&num);
		
		if(num == 1){
			printf("\nname that you want to find: ");
			scanf("%s",fname);
			
			if(strcmp(fname,"-1") != 0){
				printf("\nname that you want to reject: ");
				scanf("%s",Ename);
			}
		}
		else if(num == 2){
			printf("\nmodel that you want to find: ");
			scanf("%s",fmodel);
		}
		else if(num == 3){
			printf("\nvendor that you want to find: ");
			scanf("%s",fvendor);
		}
		else if(num == 4){
		printf("\ntele that you want to find: ");
		scanf("%s",ftele);
		}
		else if(num == 7){
			printf("\nos that you want to find: ");
			scanf("%s",fos);
		}
		else if(num == 8){
			printf("\ncpuType that you want to find: ");
			scanf("%s",fcpuType);
		}
		//numeric search
		else if(num == 5){
			printf("\nminDisplay(float) : ");
			scanf("%f",&minDisplay);

			printf("\nmaxDisplay(float) : ");
			scanf("%f",&maxDisplay);
		}
		//special - screen resolution

		else if(num == 6){
			printf("\nminScreen(int) : ");
			scanf("%d",&minScreen);

			printf("\nmaxScreen(int) : ");
			scanf("%d",&maxScreen);
		}

		else if(num == 9){
			printf("\nminCpuClock(float) : ");
			scanf("%f",&minCpuClock);

			printf("\nmaxCpuClock(float) : ");
			scanf("%f",&maxCpuClock);
		}
		else if(num == 10){
			printf("\nminMemory(float) : ");
			scanf("%f",&minMemory);

			printf("\nmaxMemory(float) : ");
			scanf("%f",&maxMemory);
		}
		else if(num == 11){
			printf("\nminPrice(int) : ");
			scanf("%d",&minPrice);

			printf("\nmaxPrice(int) : "); 
			scanf("%d",&maxPrice);
		}
		else if(num == 0)
			break;
		
		else
			printf("Tou input wrong number\n");
	}

	while(p)
	{
		if(
			((strcmp(Ename,"-1") == 0)||(SearchStar(p->name,Ename)))&&
			((strcmp(fname,"-1")== 0 )||(SearchStar(p->name,fname) == 0 ))&&
			((strcmp(fmodel,"-1")== 0 )||(SearchStar(p->model,fmodel) == 0 ))&&
			((strcmp(fvendor,"-1")== 0 )||(SearchStar(p->vendor,fvendor) == 0 ))&&
			((strcmp(ftele,"-1")== 0 )||(SearchStar(p->tele,ftele) == 0 ))&&
			((strcmp(fos,"-1")== 0 )||(SearchStar(p->os,fos) == 0 ))&&
			((strcmp(fcpuType,"-1")== 0 )||(SearchStar(p->cpuType,fcpuType) == 0 ))&&
			((minScreen == -1)||(minScreen <= Screen(p->screen)))&&
			((maxScreen == -1)||(maxScreen >= Screen(p->screen)))&&
			((minDisplay == -1)||(minDisplay <= p->display))&&
			((maxDisplay == -1)||(maxDisplay >= p->display))&&
			((minCpuClock == -1)||(minCpuClock <= p->cpuClock))&&
			((maxCpuClock == -1)||(maxCpuClock >= p->cpuClock))&&
			((minMemory == -1)||(minMemory <= p->memory))&&
			((maxMemory == -1)||(maxMemory >= p->memory))&&
			((minPrice == -1)||(minPrice <= p->price))&&
			((maxPrice == -1)||(maxPrice >= p->price))
			)
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,p->name, p->model, p->vendor, p->tele
				, p->display, p->screen, p->os, p->cpuType
				, p->cpuClock, p->memory, p->price);
		}
		p = p->next;
	}

}
int Screen(char string[])
{
	int i, strNum, cnt, change[20],multi, tot;
	char str1[20],str2[20];
	strNum = strlen(string);

	for(i=0 ; i < strNum ; i++){
		if(string[i] == 'x' )
			break;
	}
	strcpy(str1,string);
	str1[i] = 0;
	strcpy(str2,&string[i+1]);

	cnt = i;
	multi = 1;
	for(i=0 ; i<cnt ;i++)
	{
		change[i] = str1[i]-'0';
		multi *= 10;
	}
	tot = 0;
	for(i=0 ; i<cnt ; i++)
	{
		multi /= 10;
		tot = tot+(change[i]*multi);	
	}
	return tot;
}
int SearchStar(char string[], char find[])
{
	int i, finNum, check=0;

	finNum = strlen(find);

	for(i=0 ; i<finNum ; i++){
		if(find[i] == '*'){
			check = 1;
			break;
		}
	}
	if(check==0)
		return partial(string,find);
	else
		return partialStar(string,find);

}
int partial(char string[], char find[])
{
	int i, j, strNum, finNum,cnt=0;

	strNum = strlen(string);

	finNum = strlen(find);
	for(i=0 ; i<strNum-finNum+1; i++){
		for(j=0 ; j<finNum ; j++){
			if(string[i+j] != find[j]){
				break;
			}
		}
		if(j == finNum){
			cnt++;
			i=i+j-1;
		}
	}
	if(cnt>0)
		return 0;
	else
		return 1;	
}
int partialStar(char string[], char find[])
{
	int i, j, k, l, strNum, finNum;
	char str1[20],str2[20];

	strNum = strlen(string);
	finNum = strlen(find);

	for(i=0 ; i < finNum ; i++){
		if(find[i] == '*' )
			break;
	}

	strcpy(str1,find);
	str1[i] = 0;
	strcpy(str2,&find[i+1]);

	for(i=0 ; i < strNum - strlen(str1) +1 ; i++){
		for( j=0 ; j< strlen(str1) ; j++ ){
			if(string[i+j] != str1[j])
				break;
		}
		if(j == strlen(str1))//str1 is founded.
		{
			for( k = i+j ; k < strNum - strlen(str2) +1 ; k++){
				for(l=0 ; l< strlen(str2) ; l++){
					if(string[k+l] != str2[l])
						break;
				}
				if(l == strlen(str2)){
					return 0;
				}
			}
		}
		else
		{
			return 1;
		}
	}
	return 1;//cannot find
}
void Ascend(int N, int num)
{
	int i;
	printf("/name//model//vendor//tele//display//screen//os//cpuType//cpuClock//memory//price/\n");
	switch (num)
	{
	case 1:
		//Print (to terminal) list of people sorted by name using the first index
		printf("=====Name=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,in[i].p->name, in[i].p->model, in[i].p->vendor, in[i].p->tele
				, in[i].p->display, in[i].p->screen, in[i].p->os, in[i].p->cpuType
				, in[i].p->cpuClock, in[i].p->memory, in[i].p->price);
		}
		break;
	case 2:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====Screen=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,is[i].p->name, is[i].p->model, is[i].p->vendor, is[i].p->tele
				, is[i].p->display, is[i].p->screen, is[i].p->os, is[i].p->cpuType
				, is[i].p->cpuClock, is[i].p->memory, is[i].p->price);
		}
		break;
	case 3:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====Price=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,ip[i].p->name, ip[i].p->model, ip[i].p->vendor, ip[i].p->tele
				, ip[i].p->display, ip[i].p->screen, ip[i].p->os, ip[i].p->cpuType
				, ip[i].p->cpuClock, ip[i].p->memory, ip[i].p->price);
		}
		break;
	case 4:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====Display=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,id[i].p->name, id[i].p->model, id[i].p->vendor, id[i].p->tele
				, id[i].p->display, id[i].p->screen, id[i].p->os, id[i].p->cpuType
				, id[i].p->cpuClock, id[i].p->memory, id[i].p->price);
		}
		break;
	case 5:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====CpuClock=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,ic[i].p->name, ic[i].p->model, ic[i].p->vendor, ic[i].p->tele
				, ic[i].p->display, ic[i].p->screen, ic[i].p->os, ic[i].p->cpuType
				, ic[i].p->cpuClock, ic[i].p->memory, ic[i].p->price);
		}
		break;
	case 6:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====Memory=====\n");
		for( i=N-1 ; i>=0 ; i-- )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,im[i].p->name, im[i].p->model, im[i].p->vendor, im[i].p->tele
				, im[i].p->display, im[i].p->screen, im[i].p->os, im[i].p->cpuType
				, im[i].p->cpuClock, im[i].p->memory, im[i].p->price);
		}
		break;
	}
}
void Descend(int N, int num)
{
	int i;
		printf("/name//model//vendor//tele//display//screen//os//cpuType//cpuClock//memory//price/\n");
	switch (num)
	{
	case 1:
		//Print (to terminal) list of people sorted by name using the first index
		printf("=====Name=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,in[i].p->name, in[i].p->model, in[i].p->vendor, in[i].p->tele
				, in[i].p->display, in[i].p->screen, in[i].p->os, in[i].p->cpuType
				, in[i].p->cpuClock, in[i].p->memory, in[i].p->price);
		}break;
	case 2:
		//Print (to terminal) list of people sorted by screen using the second index
		printf("=====Screen=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,is[i].p->name, is[i].p->model, is[i].p->vendor, is[i].p->tele
				, is[i].p->display, is[i].p->screen, is[i].p->os, is[i].p->cpuType
				, is[i].p->cpuClock, is[i].p->memory, is[i].p->price);
		}break;
	case 3:
		printf("=====Price=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,ip[i].p->name, ip[i].p->model, ip[i].p->vendor, ip[i].p->tele
				, ip[i].p->display, ip[i].p->screen, ip[i].p->os, ip[i].p->cpuType
				, ip[i].p->cpuClock, ip[i].p->memory, ip[i].p->price);

		}

		break;
	case 4:
		printf("=====Display=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,id[i].p->name, id[i].p->model, id[i].p->vendor, id[i].p->tele
				, id[i].p->display, id[i].p->screen, id[i].p->os, id[i].p->cpuType
				, id[i].p->cpuClock, id[i].p->memory, id[i].p->price);
		}

		break;
	case 5:
		printf("=====CpuClock=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,ic[i].p->name, ic[i].p->model, ic[i].p->vendor, ic[i].p->tele
				, ic[i].p->display, ic[i].p->screen, ic[i].p->os, ic[i].p->cpuType
				, ic[i].p->cpuClock, ic[i].p->memory, ic[i].p->price);
		}

		break;
	case 6:
		printf("=====Memory=====\n");
		for( i=0 ; i<N ; i++ )
		{
			printf("%-15s , %-8s , %-10s , %-3s , %0.3f , %-10s , %-10s , %-54s , %0.1f , %0.1f , %-3d \n"
				,im[i].p->name, im[i].p->model, im[i].p->vendor, im[i].p->tele
				, im[i].p->display, im[i].p->screen, im[i].p->os, im[i].p->cpuType
				, im[i].p->cpuClock, im[i].p->memory, im[i].p->price);
		}

		break;
	}
}
void Sort(int N, PHONE *head)
{
	PHONE *p = head->next;
	int i, num=0;
		printf("===================SORT==================\n");
		printf("		1.Name\n		2.Screen\n		3.Price\n		4.Display\n		5.Clock\n		6.Memory\n");
		printf("==========================================\n");
	printf("\nInput number : ");
	scanf("%d",&num);

	switch (num)
	{
	case 1:
		if((in = (struct Iname*)malloc(sizeof(struct Iname)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){

			in[i].p = p;
			strcpy(in[i].name,p->name);
			p = p->next;
		}
		SortName(N);
		choice(N,num);
		free(in);
		break;
	case 2:
		if((is = (struct Iscreen*)malloc(sizeof(struct Iscreen)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){
			is[i].p = p;
			is[i].scre = Screen(p->screen);
			p = p->next;
		}
		SortScreen(N);
		choice(N,num);
		free(is);
		break;

	case 3:
		if((ip = (struct Iprice*)malloc(sizeof(struct Iprice)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){
			ip[i].p = p;
			ip[i].price = p->price;
			p = p->next;
		}
		SortPrice(N);
		choice(N,num);
		free(ip);
		break;

	case 4:
		if((id = (struct Idisplay*)malloc(sizeof(struct Idisplay)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){
			id[i].p = p;
			id[i].disp = p->display;
			p = p->next;
		}
		SortDisplay(N);
		choice(N,num);
		free(id);
		break;

	case 5:
		if((ic = (struct Iclock*)malloc(sizeof(struct Iclock)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){
			ic[i].p = p;
			ic[i].clock = p->cpuClock;
			p = p->next;
		}
		SortClock(N);
		choice(N,num);
		free(ic);
		break;
	case 6:
		if((im = (struct Imemory*)malloc(sizeof(struct Imemory)*N))==NULL){
			printf("malloc error!\n");
			exit(1);
		}
		for(i=0 ; i< N ; i++){
			im[i].p = p;
			im[i].mem = p->memory;
			p = p->next;
		}
		SortMemory(N);
		choice(N,num);
		free(im);
		break;
	}
}
void choice(int N, int num)
{
	int choose = 0;
	printf("Choose the number \n1. Ascending 2. Descending\n");
	scanf("%d",&choose);
	if(choose == 1)
		Ascend(N,num);
	else if(choose == 2)
		Descend(N,num);
	else{
		printf("You input wrong number\n");
		choice(N,num);
	}
}
void SortPrice(int N)
{
	struct Iprice temp;
	int i,j;
	int max, key;
	int idx;

	for( i=0 ; i<N ; i++)
	{
		max = ip[i].price;
		idx = i;
		for(j=i+1 ; j<N ; j++ )
		{
			key = ip[j].price;
			if(max < key)
			{
				max = key;
				idx = j;
			}
		}
		temp = ip[i];
		ip[i] = ip[idx];
		ip[idx] = temp;
	}
}
void SortDisplay(int N)
{
	struct Idisplay temp;
	int i,j;
	float max, key;
	int idx;

	for( i=0 ; i<N ; i++)
	{
		max = id[i].disp;
		idx = i;
		for(j=i+1 ; j<N ; j++ )
		{
			key = id[j].disp;
			if(max < key)
			{
				max = key;
				idx = j;
			}
		}
		temp = id[i];
		id[i] = id[idx];
		id[idx] = temp;
	}
}
void SortName(int N)
{
	struct Iname temp;
	int i,j;
	char max[20], key[20];
	int idx;

	for( i=0 ; i<N ; i++)
	{
		strcpy(max, in[i].name);
		idx = i;

		for(j=i+1 ; j<N ; j++ )
		{
			strcpy(key,in[j].name);

			if(strcmp(max,key) < 0)
			{
				strcpy(max,key);
				idx = j;
			}
		}
		temp = in[i];
		in[i] = in[idx];
		in[idx] = temp;
	}
}
void SortScreen(int N)
{
	struct Iscreen temp;
	int i,j;
	int max, key;
	int idx;

	for( i=0 ; i<N ; i++)
	{
		max = is[i].scre;
		idx = i;
		for(j=i+1 ; j<N ; j++ )
		{
			key = is[j].scre;
			if(max < key)
			{
				max = key;
				idx = j;
			}
		}
		temp = is[i];
		is[i] = is[idx];
		is[idx] = temp;
	}
}
void SortClock(int N)
{
	struct Iclock temp;
	int i,j;
	float max, key;
	int idx;

	for( i=0 ; i<N ; i++)
	{
		max = ic[i].clock;
		idx = i;
		for(j=i+1 ; j<N ; j++ )
		{
			key = ic[i].clock;
			if(max < key)
			{
				max = key;
				idx = j;
			}
		}
		temp = ic[i];
		ic[i] = ic[idx];
		ic[idx] = temp;
	}
}
void SortMemory(int N)
{
	struct Imemory temp;
	int i,j;
	float max, key;
	int idx;

	for( i=0 ; i<N ; i++)
	{
		max = im[i].mem;
		idx = i;
		for(j=i+1 ; j<N ; j++ )
		{
			key = im[i].mem;
			if(max < key)
			{
				max = key;
				idx = j;
			}

		}
		temp = im[i];
		im[i] = im[idx];
		im[idx] = temp;
	}
}	