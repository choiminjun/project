
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Iterator;



public class ChatServerP {
    private static final int PORT = 9001;
    private static HashSet<String> names = new HashSet<String>();//중복불가능하게 HashSet으로!
    //client로부터 오는 name들은 다 여기 저장될 것입니다.
    private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
    //모든 client에게 보낼 수 있게 생성하였습니다.

    public static void main(String[] args) throws Exception {
        System.out.println("The chat server is running.");
        ServerSocket listener = new ServerSocket(PORT);
        try {
            while (true) {//server가 켜있는 동안은 계속 Client의 접촉을 기다립니다.
                new Handler(listener.accept()).start();//inner class Handler자체가 thread이고
                								//Client가 접촉한다면 Thread가 시작됩니다.
            }
        } finally {
            listener.close();
        }
    }
    private static class Handler extends Thread {
        private String name;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        public Handler(Socket socket) {
            this.socket = socket;		//main에서 Client가 접촉하면 constructor로 인해 
            							//socket을 만들어줍니다.
        }
        public void run() {
            try {
            	// Create character streams for the socket.
                in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream())); //Client에서 Server로 오는 in.
                out = new PrintWriter(socket.getOutputStream(), true);
                //Server에서 Client로 나가는 out.
                // Request a name from this client.  Keep requesting until
                // a name is submitted that is not already used.  Note that
                // checking for the existence of a name and adding the name
                // must be done while locking the set of names.

                while (true) {
                    out.println("SUBMITNAME");	//맨처음에 Client가 접촉하면 Server에서 SUBMITNAME을 보냅니다.
                    name = in.readLine();		//그리고 Client의 name을 얻습니다.
                    if (name == null) {			//만약 이름이 null이라면 return합니다.
                        return;
                    }
                    synchronized (names) {	//names안에 있는이름과 중복되는것이 있는지 확인합니다.
                        if (!names.contains(name)) {//중복검사. 들어오는이름이 이미있는 이름인지?
                            names.add(name);	//만약 중복되는게없다면 hashset에 add합니다.
                            break;  //그리고 break로 while루프를 빠져나옵니다.
                        }
                    }
                }

                // Now that a successful name has been chosen, add the
                // socket's print writer to the set of all writers so
                // this client can receive broadcast messages.
                out.println("NAMEACCEPTED"); //client로 NAMEACCEPTED를 보냅니다.
                writers.add(out);  //writers에 out을 추가한다. 모든 Client에게 보내기 위해서입니다.
                //Hashset의<printwriter> 로 각 client에서 오는 out의 stream방향을
                //차곡차곡 add시켜서 결국 모든 client에게 line이 보내지기 위함입니다.
                for (PrintWriter writer : writers)	//writers안에 있는 모든 Client에게 보냅니다.
                	writer.println("Entrance " + name + "님이 입장하였습니다. " );
                for (PrintWriter writer : writers){
                	Iterator<String> iterator = names.iterator();
                	//각 Client마다 Hashset안의 name들을 보내주기 위해 iterator를 사용하였습니다. 
                	while(iterator.hasNext()){
                		String combo_name =iterator.next(); 
                		writer.println("SetCombo " + combo_name); //Client의 combobox를 update해주기 위함입니다.
                	}
                }

                // Accept messages from this client and broadcast them.

                // Ignore other clients that cannot be broadcasted to.
                while (true) {
                    String input = in.readLine(); //Client로부터 message를 받습니다.
                    String wisper[] = input.split(" "); //space를 기준으로 문장을 나눠줍니다.
                    input = input.substring(wisper[0].length()+1); //맨 앞의 누구에게 보내는지를 나타내는
                    								//wisper를 제외하고 나머지 String을 input에 저장합니다.
                    if (input == null) { //아무것도 들어온것이 없으면 return해주고.
                    	return;
                    }
                    for (PrintWriter writer : writers) {//들어온것이 있으면 
                        writer.println("MESSAGE " + wisper[0] + " " + name + ": " + input);
                    }	//writers에 있는 모든 client의 out stream방향으로 message를 보내줍니다.
                }
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                // This client is going down!  Remove its name and its print
                // writer from the sets, and close its socket.
            	for (PrintWriter writer : writers){ //writers에 있는 모든 Client에게 퇴장을 알려줍니다. 
                	writer.println("Exit " + name + "님이 퇴장하였습니다. " );
           	}  

            
            
            	for (PrintWriter writer : writers){ //다시 Client의 Combobox를 퇴장한 Client를 제외하고 만들어주기 위해
            										//모든 Client에게 보냅니다.
                	writer.println("removeCombo " + name);
                }

                if (name != null) { //만약 name이 null이 아니면 해당 Thread는 끝났기 때문에
                    names.remove(name);//Hashset의 name을 제거합니다.
                }
                if (out != null) {  //만약 client가 닫혔다면 해당 Thread는 끝났기 때문에
                    writers.remove(out);//writers에 들어가있는 out stream도 제거한다.
                }
                try {
                    socket.close();		//그리고 해당 Thread의 Client와 연결된 Socket을 닫습니다.
                } catch (IOException e) {
                }
            }
        }
    }
}