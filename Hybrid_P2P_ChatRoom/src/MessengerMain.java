import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.*;
public class MessengerMain extends JFrame implements ActionListener {
	BufferedReader in;
	PrintWriter out;
	JFrame frame = new JFrame("Messenger");
	Label l1 = new Label("Online User : ");
	JPanel panel = new JPanel();
	JButton send = new JButton("Connection");
	JComboBox<String> combobox = new JComboBox<String>();

	public static String name;
	String temp;
    String add;
	
    public MessengerMain() {
		setLayout(new BorderLayout());
		frame.setSize(300, 80);
		combobox.setSize(5, 1);
		combobox.addItem("User");
		panel.add(l1);
		panel.add(combobox); // Combobox를 추가시켜줍니다.
		panel.add(send);
		frame.getContentPane().add(panel, "North");
		send.addActionListener(this);
	}
	private String getServerAddress2() { // 서버의 address를 얻는 GUI
		add= JOptionPane.showInputDialog(frame,
				"Enter IP Address of the Server:", "Welcome to the Chatter",
				JOptionPane.QUESTION_MESSAGE);
		return add;
	}
	private String getName2() { // 사용자의 이름을 얻는 GUI
		return (name = JOptionPane.showInputDialog(
				// MultiThread이기 때문에 각 Thread마다
				// 귓속말을 위해 name을 저장시켜 놓습니다.
				frame, "Choose a screen name:", "Screen name selection",
				JOptionPane.PLAIN_MESSAGE));
	}
	private void run() throws IOException {
		// Make connection and initialize streams
		String serverAddress = getServerAddress2();
		Socket socket = new Socket(serverAddress, 9001); // server에 연결.
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		// Process all messages from server, according to the protocol.
		while (true) {
			// 문자열이 지정한 문자로 시작하는지 판단 같으면 true반환
			// 아니면 false를 반환한다.(대소문자구별)
			String line = in.readLine(); // 서버로부터 in.readLine으로 읽어온다.
			if (line.startsWith("SUBMITNAME")) { // line이 SUBMITNAME으로시작하면
													// (client마다 1번)
				out.println(getName2()); // server로 사용자이름을 보낸다.
			} else if (line.startsWith("Entrance")) { // line이 Entrance로 시작하면
				combobox.removeAllItems(); // combobox의 Item을 모두 지웁니다.
				combobox.addItem("User"); // 그리고 Item ALL을 넣어줍니다.
			} else if (line.startsWith("SetCombo")) { // SetCombo로 시작하면
				combobox.addItem(line.substring(9)); // Server로부터 받은 모든 name을
														// Item에 넣어줍니다.
			} else if (line.startsWith("removeCombo")) { // 다른 Client Thread가

															// 종료되었을경우

															// cobbobox에서 name을

															// 삭제합니다.
				combobox.removeItem(line.substring(12));
			}
		}
	}
	public static void main(String[] args) throws Exception {
		MessengerMain MAIN = new MessengerMain();
		MAIN.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MAIN.frame.setVisible(true); // GUI를 보이는 상태로 만들고
		MAIN.run();
	}
	public void actionPerformed(ActionEvent e){
		String action = e.getActionCommand();		
		if(action.equalsIgnoreCase("Connection"))
		{
			temp = (String) combobox.getSelectedItem();
			ChattingFile a = new ChattingFile();
		}
	}
	public String getNameFromMain()
{
		return name;
	}
}

