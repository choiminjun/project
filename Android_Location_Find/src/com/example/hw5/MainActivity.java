package com.example.hw5;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener {
	private String secretCode;
	EditText edtPhone, edtSecret;
	Button btnSetSecret, btnSendSMS;
	public static TextView tmp;
	public static SharedPreferences pref_code; // using static because class
												// SMSReceiver need to use this
												// variable.
	private static GoogleMap mMap;
	SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		if (pref_code != null && pref_code.contains("secretCode"))
			applySavedPreferences();

	}

	@SuppressLint("NewApi")
	private void init() {
		edtPhone = (EditText) findViewById(R.id.phone);
		edtSecret = (EditText) findViewById(R.id.secretcode);
		btnSetSecret = (Button) findViewById(R.id.btnSetSecret);
		btnSetSecret.setOnClickListener(this);
		btnSendSMS = (Button) findViewById(R.id.btnSendSMS);
		btnSendSMS.setOnClickListener(this);

		tmp = (TextView) findViewById(R.id.currentaddress);
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();

		mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		pref_code = getSharedPreferences("File_Code", Context.MODE_PRIVATE);
		editor = pref_code.edit();

	}

	/**
	 * get current secret key from sharedPreferences and display in TextView.
	 */
	private void applySavedPreferences() {
		String tempSecretCode = pref_code.getString("secretCode", "NULL");
		edtSecret.setText(tempSecretCode);
	}

	/**
	 * Set Secretcode in SharedPreferences by using editor.
	 */
	@Override
	public void onClick(View v) {
		editor.clear();

		if (v.getId() == btnSetSecret.getId()) {
			secretCode = edtSecret.getText().toString();
			editor.putString("secretCode", secretCode);
			System.out.println(secretCode);
			editor.commit();
		}

		/**
		 * 여기서 유저가 입력한 secret code와 번호를 입력하여 유저가 모르게 background에서 문자를 보내는 것이 옳다고
		 * 판단 하여 Service를 이용.
		 */
		else if (v.getId() == btnSendSMS.getId()) {
			String n = Uri.parse(edtPhone.getText().toString()).toString();
			String t = edtSecret.getText().toString();
			if (n.length() > 0 && t.length() > 0) {
				System.out.println(n + "," + t); // test..

				sendSMS(n, t); // send SMS

			} else {
				Toast.makeText(this, "잘못 입력 하셨습니다. 다시 입력하세요^",
						Toast.LENGTH_SHORT).show();
			}
			/**
			 * 서비스를 사용하기로 변경후 필요 없게 된 코드. Intent intent = new
			 * Intent(Intent.ACTION_SENDTO,n); String t = "SECRET " +
			 * edtSecret.getText().toString(); intent.putExtra("Secret", t);
			 * startActivity(intent);
			 */
		}
	}

	/**
	 * Send SMS to get location who receives message. When messege which
	 * includes secret code is same as the other man's secret code, the other
	 * man will send his location to user who sended message, and app will show
	 * the other man's location with google map.
	 */
	public void sendSMS(String smsNumber, String smsText) {
		PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0,
				new Intent("SMS_SENT_ACTION"), 0);
		PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0,
				new Intent("SMS_DELIVERED_ACTION"), 0);

		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					// 전송 성공
					Toast.makeText(getBaseContext(), "전송 완",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					// 전송 실패
					Toast.makeText(getBaseContext(), "전송 실패",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}

		}, new IntentFilter("SMS_SENT_ACTION"));
		
		/**
		 * 다른 사람에게 secret 키를 알맞게 보냇다면 응답이 올것이다. 응답으로는 다른 사람의 location이 올것이다. 이
		 * location 정보를 이용 하여 구글맵을 띄우면 된다. 
		 */
		SmsManager mSmsManager = SmsManager.getDefault();
		mSmsManager.sendTextMessage(smsNumber, null, smsText, sentIntent,
				deliveredIntent); // send message.
	}

	/**
	 * This is static method accessed by broadcast receiver.
	 * @param str
	 */
	public static void updateAddress(String str) {
		tmp.setText(str);
	}

	/**
	 * Update maps which is accessed by broadcaster.
	 */
	public static void updateGoogleMap(double latitude, double longitude,
			String addr) {
		Marker pos = mMap.addMarker(new MarkerOptions()
				.position(new LatLng(latitude, longitude))
				.title(addr)
				.icon(BitmapDescriptorFactory
						.fromResource(android.R.drawable.arrow_down_float)));
		pos.showInfoWindow();
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
				longitude), 15.0f));

	}
}
