﻿
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

public class P2P_Game_Server_Clients extends Frame implements Runnable, ActionListener {
	/** 새로 삽입한 부분 **//////// 
	private TextArea msgView=new TextArea("", 1,1,1);   // 메시지를 보여주는 영역
	private TextField sendBox=new TextField("");         // 보낼 메시지를 적는 상자
	private TextField nameBox=new TextField();          // 사용자 이름 상자
	private TextField recordBox=new TextField();		// 사용자 전적(승,패)을 나타내는 상자

	private Button startButton=new Button("게임 준비");    // 게임 준비 버튼
	private Button stopButton=new Button("기권");         // 기권 버튼
	private Button toWaitingRoomButton=new Button("대기실로"); // 대기실로 나가는 버튼

	// 각종 정보를 보여주는 레이블
	private Label infoView=new Label("< 일대일 오목 게임 >", 1);
	private OmokBoard board=new OmokBoard(15,30);      // 오목판 객체

	private Thread thread;
	private Kind kind;
	private int port;
	private String name;
	private String serverName;
	private String clientName;
	private String host;
	private Socket socket;
	private ServerSocket ss;
	private volatile PrintWriter out;
	private Scanner in;

	private boolean youAreReady = false;	// 상대방의 게임 준비 여부
	private boolean iAmReady = false;		// 나의 게임 준비 여부
	private int numWin, numLoss;		// 나의 승, 패 횟수
	
	public P2P_Game_Server_Clients(Kind kind, String username, int port, String numberWin, String numberLoss) {		

		this.kind = kind;
		this.name = username;
		this.port = port;
		this.numWin= Integer.parseInt(numberWin);		// 서버에서 가져온 나의 승 기록
		this.numLoss = Integer.parseInt(numberLoss);	// 서버에서 가져온 나의 패 기록		

		setLayout(null);                                
		msgView.setEditable(false);
		infoView.setBounds(10,30,480,30);
		infoView.setBackground(new Color(200,200,255));
		board.setLocation(10,70);
		add(infoView);
		add(board);
		Panel p=new Panel();
		p.setBackground(new Color(200,255,255));
		p.setLayout(new GridLayout(3,1));
		p.add(new Label("이     름", 1));
		p.add(nameBox);
		p.add(recordBox);
		nameBox.setText(name);			
		nameBox.setEditable(false);		
		recordBox.setText(numWin + "승" + " " + numLoss + "패");			
		recordBox.setEditable(false);	

		p.setBounds(500,30, 250,50);
		Panel p2=new Panel();
		p2.setBackground(new Color(255,255,100));
		p2.add(startButton); 
		p2.add(stopButton);
		p2.add(toWaitingRoomButton);
		startButton.setEnabled(true); 
		stopButton.setEnabled(false);
		toWaitingRoomButton.setEnabled(true);
		p2.setBounds(500,90,250,40);
		Panel p3=new Panel();
		p3.setLayout(new BorderLayout());
		p3.add(msgView,"Center");
		p3.add(sendBox, "South");
		p3.setBounds(500,140,250,410);
		add(p); add(p2); add(p3);
		// 이벤트 리스너를 등록
		sendBox.addActionListener(this);
		startButton.addActionListener(this);
		stopButton.addActionListener(this);
		toWaitingRoomButton.addActionListener(this);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (board.isRunning() == true)	// 게임 진행중에 종료하면 패전으로 기록하기 위해 나의 패 증가시고, 상대방 승 증가
				{
					numLoss++;
					out.println("[UPDATE_RECORD]WIN");
				}
				out.println("[EXIT]" + name + " " + numWin + " " + numLoss);	// 상대방이 자신의 이름과 승, 패 기록을 넘겨주어 서버에게 같이 전달할 수 있도록 함 
				thread.stop();
				dispose();
			}
		});

		this.setSize(760,560);
		this.setVisible(true);		

		thread = new Thread(this, kind.toString());	

	}

	public static enum Kind {
		Client("Trying"), Server("Awaiting");
		private String activity;
		private Kind(String activity) {
			this.activity = activity;
		}
	}

	public void actionPerformed(ActionEvent ae){

		if(ae.getSource()==sendBox){		// 채팅메시지박스 - 채팅 메시지가 입력되면 상대방에게 [MSG] 프로토콜을 쏴준다	             
			String msg=sendBox.getText();	
			if(msg.length()==0)
				return;
			if(msg.length()>=30)
				msg=msg.substring(0,30);
			try {  
				out.println("[MSG]"+"["+name+"]"+msg);
				sendBox.setText("");
				msgView.append("["+name+"]"+msg+"\n");
			} catch(Exception ie){}
		}
		else if(ae.getSource()==startButton){          // 게임 준비 버튼 
			try {									    
				iAmReady = true;				// 나의 준비상태가 레디로 바뀐다.
				if (youAreReady == true)		// 상대방이 이미 레디를 한 상태였다면 랜덤으로 나의 돌 색을 정하고 상대방에게 반대 색깔을 보내주어 보드판이 활성되어 게임시작할 수 있음
				{
					play("sound/gamestart.wav");
					int a = new Random().nextInt(2);
					if(a==0){
						board.startGame("BLACK");
						infoView.setText("흑돌을 잡았습니다.");
						out.println("[COLOR]WHITE");
					}
					else {
						board.startGame("WHITE");
						infoView.setText("백돌을 잡았습니다.");
						out.println("[COLOR]BLACK");
					}					
					stopButton.setEnabled(true);          // 기권 버튼 활성화
				}					
				else		// 내가 먼저 레디 버튼을 눌렀다면 상대방에게 내가 준비됏음을 알려준다.
				{
					out.println("[START]");
					infoView.setText("상대의 결정을 기다립니다.");
				}
				startButton.setEnabled(false); 

			} catch(Exception e){}
		}
		else if(ae.getSource()==stopButton){          // 기권 버튼
			try {										
				out.println("[DROPGAME]");			 // 상대방에게 내가 기권했음을 알리는 프로토콜
				endGame("기권하였습니다.");
				out.println("[UPDATE_RECORD]WIN");	// 상대방의 전적 업데이트를 시켜주는 프로토콜
			}catch(Exception e){}
		}
		if(ae.getSource()==toWaitingRoomButton){       // 대기실로 버튼이면
			if (board.isRunning() == true)	// 게임 진행중에 종료하면 패전으로 기록하기 위해 나의 패 증가시고, 상대방 승 증가
			{
				numLoss++;
				out.println("[UPDATE_RECORD]WIN");
			}
			out.println("[EXIT]" + name + " " + numWin + " " + numLoss);	// 상대방에게 자신의 이름과 승, 패 기록을 넘겨주어 상대방이 서버에게 같이 전달할 수 있도록 하기 위한 프로토콜 
			thread.stop();
			setVisible(false);
			dispose();
		}	
	}

	// server용
	public P2P_Game_Server_Clients(Kind kind, int port, String name, String clientName, String numWin, String numLoss) {	
		this(kind, name, port, numWin, numLoss);
		this.serverName = name;
		this.clientName = clientName;
	}

	// client용	
	public P2P_Game_Server_Clients(Kind kind, int port, String ipAddr, String name,	String callerName, String numWin, String numLoss) {
		this(kind, name, port, numWin, numLoss);
		this.clientName = name;
		this.serverName = callerName;
		this.host = ipAddr;
	}

	public void start() {
		thread.start();
	}

	// @Override
	@SuppressWarnings("resource")
	public void run() {
		socket = null;
		try {
			if (kind.equals(Kind.Server)) {
				ss = new ServerSocket(port);
				socket = ss.accept();
				System.out.println(("accepted"));
			} else if (kind.equals(Kind.Client)) {
				socket = new Socket(host, port);
			}

			in = new Scanner( socket.getInputStream());
			out = new PrintWriter(socket.getOutputStream(), true);
			board.setWriter(out);

			String msg;
			while (true) {
				msg = in.nextLine();				
				if(msg.startsWith("[START]")){  		// 상대방이 레디버튼을 눌렀을 때 보내온 [START] 프로토콜
					System.out.println("[START] received");
					if (iAmReady) {
						youAreReady = true;
						play("sound/gamestart.wav");
						
						int a = new Random().nextInt(2);
						if(a==0){
							board.startGame("BLACK");
							infoView.setText("흑돌을 잡았습니다.");
							out.println("[COLOR]WHITE");
						}
						else {
							board.startGame("WHITE");
							infoView.setText("백돌을 잡았습니다.");
							out.println("[COLOR]BLACK");
						}					
						stopButton.setEnabled(true);          // 기권 버튼 활성화된다.
					}					
					else	
					{
						youAreReady = true;			
						infoView.setText("상대방이 게임준비를 완료했습니다.");
					}						
				}													

				else if(msg.startsWith("[STONE]")){     // 상대방이 놓은 돌의 좌표를 보내온 [STONE] 프로토콜 
					System.out.println("[STONE] received");
					play("sound/button.wav");
					String temp=msg.substring(7);
					int x=Integer.parseInt(temp.substring(0,temp.indexOf(" ")));
					int y=Integer.parseInt(temp.substring(temp.indexOf(" ")+1));
					board.putOpponent(x, y);     // 상대편의 돌을 그린다.
					board.setEnable(true);        // 사용자가 돌을 놓을 수 있도록 한다.
				}
				else if(msg.startsWith("[COLOR]")){          // 상대방이 먼저 돌색깔이 정해지고 나한테 보내온 반대 색깔의 돌을 알리는 [COLOR] 프로토콜
					play("sound/gamestart.wav");					
					System.out.println("[COLOR] received: " + msg);
					String color=msg.substring(7);
					board.startGame(color);                      // 게임을 시작한다.
					if(color.equals("BLACK"))
						infoView.setText("흑돌을 잡았습니다.");
					else
						infoView.setText("백돌을 잡았습니다.");
					stopButton.setEnabled(true);                 // 기권 버튼 활성화
				}

				else if(msg.startsWith("[LOSS]")) {           // 상대방이 이기면 나에게 내가 졌음을 알려오는 [LOSS] 프로토콜
					System.out.println("[LOSS] received");
					out.println("[WIN]");					 // 이제 내가 상대방에게 이겼음을 알려주는 [WIN] 프로토콜을 쏴준다.
					endGame("졌습니다.");						
					out.println("[UPDATE_RECORD]WIN");	 // 상대방의 전적 업데이트하는 프로토콜
				}

				else if(msg.startsWith("[WIN]"))  {            // 상대방이 지면 나에게 내가 이겼음을 알려오는 [WIN] 프로토콜
					System.out.println("[WIN] received");
					endGame("이겼습니다.");
					out.println("[UPDATE_RECORD]LOSS");		
				}
				else if(msg.startsWith("[DROPGAME]"))   {   	// 상대방이 기권했을 때 보내온 [DROPGAME] 프로토콜
					System.out.println("[DROPGAME] received");
					endGame("상대가 기권하였습니다.");
					out.println("[UPDATE_RECORD]LOSS");	
				}

				else if(msg.startsWith("[UPDATE_RECORD]")) {          // 상대방이 나에게 나의 승패 결과를 보내온 [UPDATE_RECORD] 프로토콜  
					System.out.println("[UPDATE_RECORD] received");
					if (msg.substring(15).equals("WIN"))		// 상대방이 나에게 이겼다고 보내오면 나의 승수 증가
					{
						numWin++;
						System.out.println("win!! "+ numWin);
						
					}
					else if (msg.substring(15).equals("LOSS"))	// 상대방이 나에게 졌다고 보내오면 나의 패수 증가
					{
						numLoss++;		
						System.out.println("loss!! "+ numLoss);
					}
					recordBox.setText(numWin + "승" + " " + numLoss + "패");		// 게임이 끝나고 나면 승, 패 기록을 승패 기록 박스에 실시간 업데이트
				}

				else if (msg.startsWith("[EXIT]")) {				// 상대방이 대기실로 버튼을 누르거나 종료버튼을 눌러 종료하면서 보내온 [EXIT] 프로토콜
					String tmp[] = msg.substring(6).split(" ");
					String oppName = tmp[0];						// 상대방이 종료하면서 보내온 자신의 이름, 승수, 패수
					int oppNumWin = Integer.parseInt(tmp[1]);		
					int oppNumLoss = Integer.parseInt(tmp[2]);		
					// P2P채팅(게임)간 거둔 승수와 패수를 종료와 함께 업데이트될 수 있도록 서버에게 나와 상대방의 이름,승수,패수를 보내준다.
					ChatClient.end_P2P(serverName, clientName, name, numWin, numLoss, oppName, oppNumWin, oppNumLoss);
					super.dispose();
					return;
				}				
				else if (msg.startsWith("[MSG]"))			// 상대방이 보내온 채팅 메시지를 찍어주기 위한 [MSG] 프로토콜
					msgView.append(msg.substring(5)+"\n");
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {}						
	}

	private void endGame(String msg){                // 게임(게임채팅창의 종료가 아닌 한판의 게임을 의미)을 종료시키는 메소드 
		play("sound/gameover.wav");
		infoView.setText(msg);
		startButton.setEnabled(false);
		stopButton.setEnabled(false);
		try {
			Thread.sleep(2000); 
		} catch(Exception e){
		}    // 2초간 대기
		if(board.isRunning())
			board.stopGame();
		startButton.setEnabled(true);
		iAmReady = false;
		youAreReady = false;
	}

	public void play(String fileName){
		try
		{
			AudioInputStream ais = AudioSystem.getAudioInputStream(new File(fileName));
			Clip clip = AudioSystem.getClip();  
			clip.stop();
			clip.open(ais);
			clip.start();
		}
		catch(Exception ex)
		{
		}
	}

}

