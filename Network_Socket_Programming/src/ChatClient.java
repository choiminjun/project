
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Vector;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class ChatClient {
	private static final int MAIN_SERVER_PORT = 9001;
	private String clientName;
	private String address;
	private int p2pServerSidePort;

	private String P2P_starterName;
	private String P2P_targetName;	
	private String client_Wins;
	private String client_Loses;
	
	private BufferedReader in;
	private static PrintWriter out;

	private JFrame frame = new JFrame("Chatter");
	private JTextField textField = new JTextField(40);
	private JTextField msgField = new JTextField(30);
	private JPanel listPanel = new JPanel();

	JList<String> jlist;


	private JTextArea messageArea = new JTextArea(8, 20);


	public static String serverAddress;
	String temp = null;

	String[] userIDPW; // p2p연결시 server 역할 하는 user 이름
	String newUID;
	String newUPW;
	String newUNAME;
	JFrame frame2 = new JFrame("로그인");
	// 로그인 프레임
	JPanel panelID = new JPanel();
	// 아이디
	JLabel labelID = new JLabel("아이디     ");
	JTextField ID = new JTextField(10);
	JPanel panelPW = new JPanel();
	// 비밀번호
	JLabel labelPW = new JLabel("비밀번호");
	JPasswordField PW = new JPasswordField(10);
	JPanel panelButton2 = new JPanel();
	// 회원가입버튼, 로그인버튼
	JButton newMember = new JButton("회원가입");
	JButton login = new JButton("로그인");
	JButton changeInfo = new JButton("정보수정");
	JFrame frame3 = new JFrame("회원가입");
	JFrame frame8 = new JFrame("이름변경");
	// 회원가입 프레임
	JPanel panelNewID = new JPanel();
	JLabel labelNewID = new JLabel("등록아이디     ");
	JTextField NewID = new JTextField(10);
	// 등록아이디
	JPanel panelNewPW = new JPanel();
	JLabel labelNewPW = new JLabel("등록비밀번호");
	JPasswordField NewPW = new JPasswordField(10);
	// 등록 비밀번호
	JPanel panelNewName = new JPanel();
	JLabel labelNewName = new JLabel("등록이름       ");
	JTextField NewName = new JTextField(10);
	// 등록 이름
	JPanel panelButton3 = new JPanel();
	JButton access = new JButton("등록");
	JButton change = new JButton("이름변경");
	// 등록버튼
	static JFrame frame6 = new JFrame("로그인실패");
	// 로그인 실패 메시지
	static JFrame frame4 = new JFrame("회원가입성공");
	// 회원가입 성공 메시지
	static JFrame frame5 = new JFrame("회원가입오류");
	// 회원가입 오류 메시지

	public ChatClient() {
		frame.setLayout(new BorderLayout());
		//		frame.setBounds(100, 200, 200, 300);
		textField.setActionCommand("field");
		textField.setEditable(false);
		textField.setText("유저들의 대기방");
		textField.setBackground(Color.decode("#EEE8AA"));
		textField.setFont(new Font(textField.getFont().getName(), Font.ITALIC,
				16));
		frame.getContentPane().add(textField, "North");
		messageArea.setEditable(true);
		frame.getContentPane().add(new JScrollPane(messageArea), BorderLayout.CENTER);	
		/**
		 *대기방에서 클라이언트를 더블클릭하면 p2p 게임/채팅이 열리는데 
		 *여기서 더블클릭하는 클라이언트가 p2p채팅/게임에서 서버역할을 하고
		 *또 다른 하나의 클라이언트는 고객 역할을 한다.
		 * 
		 */
		listPanel.setLayout(new GridLayout(1, 1));
		jlist = new JList<String>();
		jlist.setBackground(Color.decode("#EEE8AA"));
		jlist.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				JList<String> theList = (JList<String>) mouseEvent.getSource();
				if (mouseEvent.getClickCount() == 2) {

					// To hide my name in the waiting room during P2P chat.
					int index = theList.locationToIndex(mouseEvent.getPoint());
					if (index >= 0) {
						Object o = theList.getModel().getElementAt(index);
						P2P_targetName = o.toString();

						if (P2P_targetName.equalsIgnoreCase(clientName + "<ME>")
								|| P2P_targetName.equalsIgnoreCase(""))
							return;
						
						out.println("IN_P2P" + "," + clientName);
						//p2p 생성을 위한 랜덤 포트 번호 생성
						p2pServerSidePort = (int) ((Math.random() * 20000) + 10000);
						final int port = p2pServerSidePort;
						EventQueue.invokeLater(new Runnable() { 
							public void run() { 
								//p2p 객체 생성
								new P2P_Game_Server_Clients(
										P2P_Game_Server_Clients.Kind.Server, port, clientName, P2P_targetName, client_Wins, client_Loses).start();
							}
						} );
						/**
						 * 더블 클릭한 서버역할을 클라이언트가 위 쪽에서 자신의 p2p 객체를 선언하면, 
						 * 고객 역할을 하는 클라이언트에게도 p2p 객체를 선언하기 위해서 메인 서버에게
						 * 메시지를 보낸다. 나중에 결국 이 정보를 이용하여 메인서버는 또 다른 하나의 클라이언트에게
						 * 해당 정보를 이용하여 p2p 객체를 생성한다.
						 */
						out.println("INIT_CONNECT" + "," + P2P_targetName + "," + clientName + ","
								+ p2pServerSidePort + "," + address);
					}
				}
			}
		});
		JScrollPane scrollPane1 = new JScrollPane(jlist);
		listPanel.add(scrollPane1);
		frame.getContentPane().add(listPanel, BorderLayout.EAST);		
		msgField.setEditable(false);	
		msgField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				out.println("MESSAGE" + msgField.getText());
				msgField.setText("");
			}
		});
		frame.getContentPane().add(msgField, BorderLayout.SOUTH);
		frame.pack();

		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				out.println(textField.getText());
				textField.setText("");
			}
		});

		try {
			address = InetAddress.getLocalHost().getHostAddress();
			System.out.println("Local Host:");
			System.out.println("\t" + address);
		} catch (UnknownHostException e) {
			System.out.println("Unable to determine this host's address");
		}

		JMenu memoMenu = new JMenu("옵션");		// 종료를 위한 메뉴버튼
		JMenuItem m;
		m = new JMenuItem("종료");
		m.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		memoMenu.add(m);
		JMenuBar mBar = new JMenuBar();
		mBar.add(memoMenu);
		frame.setJMenuBar(mBar);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void run() throws IOException {
		serverAddress = getServerAddress();
		Socket socket = new Socket(serverAddress, MAIN_SERVER_PORT);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		System.out.println("Chatclient and mainserver connection success.."); // for
		login();		//call login class.
		try{
			while (true) {
				String line = in.readLine();
				if (line.startsWith("JOINSUCCESS")) {
					JOptionPane.showMessageDialog(frame4,
							"회원가입을 축하합니다.");
					// 회원가입 성공
					frame3.setVisible(false);
				}
				// 이미 등록된 정보 (회원가입 실패)				
				else if (line.startsWith("JOINFAIL")) {
					if (line.charAt(8) == '2')
					JOptionPane.showMessageDialog(frame5,
							"이미 등록된 아이디와 이름입니다. 다른 아이디와 이름으로 가입해주세요.");					
					else if (line.charAt(8) == '3') 
					JOptionPane.showMessageDialog(frame5,
							"이미 등록된 아이디 입니다. 다른 아이디로 가입해주세요.");
					else   // else if (line.charAt(8) == '4')
					JOptionPane.showMessageDialog(frame5,
							"이미 등록된 이름 입니다. 다른 이름으로 가입해주세요.");								
				}
				else if(line.startsWith("LOGINFAIL_ALREADYLOGINED")){
					String ttmp[] = line.split(" "); //ttmp1 is user name.
					JOptionPane.showMessageDialog(frame6,
							ttmp[1] + " 님은 이미 로그인 하신 유저 입니당^^;");
				}
				else if(line.startsWith("CHANGE_SUCCESS")){
					String ttmp[] = line.split(" ");	//ttmp[1] is prev name, ttmp[3] is new
					JOptionPane.showMessageDialog(frame6,
							ttmp[1] + " 님은 " + ttmp[3] + " 으로 변경 되었습니다");
					frame8.setVisible(false);

				}
				else if(line.startsWith("CHANGE_LOGINFAIL_ALREADYLOGINED")){
					String ttmp[] = line.split(" "); //ttmp1 is user name.
					JOptionPane.showMessageDialog(frame6,
							ttmp[1] + " 님은 이미 로그인 하신 유저 입니다. 정보변경은 보안상 문제로 오로지 로그아웃 이후에 이용 가능 합니다");
				}
				else if(line.startsWith("CHANGE_FAILED")){
					JOptionPane.showMessageDialog(frame6,
							 " 정보 변경 실패!! 다시 한번 확인하여 주세요^_^;;");
				
				}
				else if (line.startsWith("LOGINFAIL")) {
					// 만일 가입되어있지 않은 회원이거나 비밀번호가 틀리다면
					System.out.println("for testing");//for test..
					JOptionPane.showMessageDialog(frame6,
							"아이디 혹은 비밀번호가 일치하지 않습니다.");
					// 오류메시지가 출력된다.
				} else if (line.startsWith("LOGINSUCCESS")) {
					play("sound/welcome.wav");
					
					// 아이디 비밀번호가 일치했다면
					String ttmp[] = line.split(" ");
					System.out.println("success of getting " + ttmp[1]);
					clientName = ttmp[1];		// allocation clientsName.
					client_Wins = ttmp[2];
					client_Loses = ttmp[3];
					System.out.println(" wins!!!!  " + clientName + " " + client_Wins + "  "+client_Loses);
					// client 쓰레드를 시작한다.
					frame.setVisible(true);
					frame2.setVisible(false);

					// 로그인창을 비활성화한다.
					//return ttmp[1];
				}
				else if (line.startsWith("MESSAGE")) {	
					messageArea.append(line.substring(8) + "\n");
				}				
				else if (line.startsWith("READY_CONNECT")) {
					/**p2p 연결 프로토콜 
					 * p2p에서 서버역할을 하는 클라이언트가 메인 서버에 메시지를 보내어
					 * 메인 서버에서 p2p에서 고객역할을 하는 클라이언트에게 도달되는 메시지
					 */
					
					// To hide my name in the waiting room during P2P chat.
					out.println("IN_P2P" + "," + clientName);

					String tmp = line.substring(14); 
					String t[] = tmp.split(",");

					P2P_starterName = t[0];
					int portNum = Integer.parseInt(t[1]);
					String ipAddr = t[2];
					client_Wins = t[3];
					client_Loses= t[4];

					// NEW p2p client side
					final int port = portNum;
					final String ip = ipAddr;
					System.out.println("P2P REQUEST HAD RECEIVED..");
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							/**
							 * p2p에서 고객 열할을 하는 클라이언트가 객체를 생성한다.
							 */
							new P2P_Game_Server_Clients(P2P_Game_Server_Clients.Kind.Client, port,
									ip, clientName, P2P_starterName, client_Wins, client_Loses).start();
						}
					});
				} else if (line.startsWith("UPDATE_LIST")) {
					play("sound/DOORBELL.wav");
					String tmp = line.substring(12);
					String t[] = tmp.split(",");

					ArrayList<String> tmpList = new ArrayList<>();
					for (int i = 0; i < t.length; i++)
						tmpList.add(t[i]);				

					Vector<String> labels = new Vector<String>();

					if (t[0].equals(""))	// if all the clients are on P2P chat 
						labels.clear();
					else
					{
						for (int i = 0; i < tmpList.size(); i++) {
							if (tmpList.get(i).equals(clientName))
								labels.addElement(tmpList.get(i) + "<ME>");
							else
								labels.addElement(tmpList.get(i));
						}
					}
					jlist.setListData(labels);
					msgField.setEditable(true);

				} else if (line.startsWith("MESSAGE")) {
					messageArea.append(line.substring(8) + "\n");
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	private String getServerAddress() {
		return JOptionPane.showInputDialog(frame,
				"Enter IP Address of the Server:", "Welcome to the Chatter",
				JOptionPane.QUESTION_MESSAGE);
	}

	private String getName() {
		return JOptionPane.showInputDialog(frame, "Choose a screen name:",
				"Screen name selection", JOptionPane.PLAIN_MESSAGE);
	}


	public void login() throws UnknownHostException, IOException {
		play("sound/opening.mp3");
		
		panelID.add(labelID);
		panelID.add(ID);
		// 패널ID에 라벨과 버튼을 붙인다.
		panelPW.add(labelPW);
		panelPW.add(PW);
		// 패널PW에 라벨과 버튼을 붙인다.
		panelButton2.add(newMember);
		panelButton2.add(login);
		panelButton2.add(changeInfo);
		// 패널버튼2에 회원가입버튼과 로그인버튼을 붙인다.
		Box IDPW = Box.createVerticalBox();
		IDPW.add(panelID);
		IDPW.add(panelPW);
		IDPW.add(panelButton2);
		// 박스레이아웃을 이용해서 수직으로 다 붙인다.
		frame2.add(IDPW, BorderLayout.CENTER);
		// 프레임 중앙에 붙인다.
		frame2.pack();
		// 프레임크기 자동조절
		frame2.setVisible(true);
		// 메소드 호출되면 보이게한다.
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 종료버튼 활성화
		/*
		 * 
		 * 로그인창 완성
		 */
		newMember.addActionListener(new ActionListener() {
			// 회원가입 버튼글릭시 액션
			public void actionPerformed(ActionEvent e) {
				String action = e.getActionCommand();
				if (action.equalsIgnoreCase("회원가입")) {
					play("sound/button.wav");
					
					newMember();
					// 회원가입 메소드를 호출한다.
					// check = 0;
				}
			}
		});
		login.addActionListener(new ActionListener() {
			// 로그인 버튼 클릭시 액션
			public void actionPerformed(ActionEvent e) {
				String action = e.getActionCommand();
				if (action.equalsIgnoreCase("로그인")) {
					play("sound/button.wav");
					
					temp = null;
					userIDPW = null;
					temp = ID.getText() + " " + PW.getText();
					userIDPW = temp.split(" ");
					out.println("LOGIN " + userIDPW[0] + " " + userIDPW[1]);
					// UserInfo 클래스에 아이디와 패스워드 전달
				}
			}
		});
		changeInfo.addActionListener(new ActionListener() {
			// 정보수정 버튼글릭시 액션
			public void actionPerformed(ActionEvent e) {
				String action = e.getActionCommand();
				if (action.equalsIgnoreCase("정보수정")) {
					play("sound/button.wav");
					
					change_info();
					
				}
			}
		});
	}

	public void newMember() {
		panelNewID.add(labelNewID);
		panelNewID.add(NewID);
		// 패널ID에 라벨과 버튼을 붙인다.
		panelNewPW.add(labelNewPW);
		panelNewPW.add(NewPW);
		// 패널PW에 라벨과 버튼을 붙인다.
		panelNewName.add(labelNewName);
		panelNewName.add(NewName);
		panelButton3.add(access);
		// 패널버튼2에 회원가입버튼과 로그인버튼을 붙인다.
		Box NewIDPW = Box.createVerticalBox();
		NewIDPW.add(panelNewID);
		NewIDPW.add(panelNewPW);
		NewIDPW.add(panelNewName);
		NewIDPW.add(panelButton3);
		frame3.add(NewIDPW, BorderLayout.CENTER);
		// 프레임 중앙에 붙인다.
		frame3.pack();
		// 프레임크기 자동조절
		frame3.setVisible(true);
		access.addActionListener(new ActionListener() {
			// 회원가입 버튼글릭시 액션
			public void actionPerformed(ActionEvent e) {
				String action = e.getActionCommand();
				if (action.equalsIgnoreCase("등록")) {
					play("sound/button.wav");
					
					newUID = NewID.getText();
					newUPW = NewPW.getText();
					newUNAME = NewName.getText();
					NewID.setText("");
					NewPW.setText("");
					NewName.setText("");
					out.println("NEWMEMBER " + newUID + " " + newUPW + " "
							+ newUNAME);
					
				}
			}
		});
	}

	public void change_info() {
		panelNewID.add(labelNewID);
		panelNewID.add(NewID);
		// 패널ID에 라벨과 버튼을 붙인다.
		panelNewPW.add(labelNewPW);
		panelNewPW.add(NewPW);
		// 패널PW에 라벨과 버튼을 붙인다.
		panelNewName.add(labelNewName);
		panelNewName.add(NewName);
		panelButton3.add(change);
		// 패널버튼2에 회원가입버튼과 로그인버튼을 붙인다.
		Box NewIDPW = Box.createVerticalBox();
		NewIDPW.add(panelNewID);
		NewIDPW.add(panelNewPW);
		NewIDPW.add(panelNewName);
		NewIDPW.add(panelButton3);
		frame8.add(NewIDPW, BorderLayout.CENTER);
		// 프레임 중앙에 붙인다.
		frame8.pack();
		// 프레임크기 자동조절
		frame8.setVisible(true);
		change.addActionListener(new ActionListener() {
			// 회원가입 버튼글릭시 액션
			public void actionPerformed(ActionEvent e) {
				String action = e.getActionCommand();
				if (action.equalsIgnoreCase("이름변경")) {
					play("sound/button.wav");
					
					newUID = NewID.getText();
					newUPW = NewPW.getText();
					newUNAME = NewName.getText();
					NewID.setText("");
					NewPW.setText("");
					NewName.setText("");
					out.println("CHANGEINFO " + newUID + " " + newUPW + " "
							+ newUNAME);
					
				}
			}
		});
	}

	public void play(String fileName){
		  try
	        {
	            AudioInputStream ais = AudioSystem.getAudioInputStream(new File(fileName));
	            Clip clip = AudioSystem.getClip();
	            clip.stop();
	            clip.open(ais);
	            clip.start();
	        }
	        catch(Exception ex)
	        {
	        }
	}

	static void end_P2P(String P2P_server, String P2P_client, String myName, int myWin, int myLost, String oppName, int oppWin, int oppLost ) throws IOException {
		out.println("OUT_P2P" + "," + P2P_server + "," + P2P_client + "," + myName + "," + myWin + "," + myLost + "," + oppName + "," + oppWin + "," + oppLost);
	}

	public static void main(String[] args) throws Exception {
		try {
			//UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel");
		   UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
		} catch (Exception e) {
		    e.printStackTrace();
		}
		ChatClient client = new ChatClient();
		client.run();
	}

}
