import java.io.*;
import java.util.Calendar;


public class UserInfo {

	static String ID = null;
	static String PW = null;
	static String name = null;
	static String current = null;	//whether user logined or not.
	static String nID = null;
	static String nPW = null;
	static String nName = null;
	static String[][] user = new String[100][6];
	

	String[] temp = new String[6]; 
	int year, mon, day, hour, min;
	static int check;
	static int length;
	static String date;
	static String returnValue = "failed";
	static String returnInfo[] = new String[6];


	public UserInfo(String tmp1, String tmp2) {
		// 컨스트럭터 불릴때마다 날짜 갱신
		ID = tmp1;
		PW = tmp2;
		Calendar now = Calendar.getInstance();
		year = now.get(Calendar.YEAR);
		mon = now.get(Calendar.MONTH) + 1;
		day = now.get(Calendar.DAY_OF_MONTH);
		hour = now.get(Calendar.HOUR_OF_DAY);
		min = now.get(Calendar.MINUTE);
		date = year + "/" + mon + "/" + day + "/" + hour + "/" + min;
		returnValue = "failed";
	}

	public UserInfo(String tmp1, String tmp2, String tmp3) {
		// 컨스트럭터 불릴때마다 날짜 갱신
		nID = tmp1;
		nPW = tmp2;
		nName = tmp3;

		Calendar now = Calendar.getInstance();
		year = now.get(Calendar.YEAR);
		mon = now.get(Calendar.MONTH) + 1;
		day = now.get(Calendar.DAY_OF_MONTH);
		hour = now.get(Calendar.HOUR_OF_DAY);
		min = now.get(Calendar.MINUTE);
		date = year + "/" + mon + "/" + day + "/" + hour + "/" + min;
		returnValue = "failed";
	}

	public UserInfo(String tmp){
		name = tmp;
	}

	public String[] returnSelectInfo() throws IOException {
		// 클라이언트에서 유저정보 클릭시 일치하는 이름의 유저정보를 리턴해준다.
		BufferedReader reader = new BufferedReader(new FileReader("user.txt"));
		String line;
		int i = 0;
		while ((line = reader.readLine()) != null) // 라인의 값이 null 이 될때까지 읽어냄
		{
			temp = line.split(" ");

			if (name.equalsIgnoreCase(temp[2])) {
				returnInfo = temp;
			}
			i++;
			length = i;
		}
		reader.close();
		return returnInfo;
	}
	public int isDuplicate() throws IOException {
		// 회원가입시 현재 가입되어 있는 아이디가 있는지 검사한다.

		BufferedReader reader = new BufferedReader(new FileReader("user.txt"));
		String line;
		int returnValue = 0;
		int chk = 0;

		int i = 0;

		while ((line = reader.readLine()) != null) // 라인의 값이 null 이 될때까지 읽어냄
		{
			temp = line.split(" ");
			for (int j = 0; j < 6; j++) {
				user[i][j] = temp[j];
			}

			if (nID.equalsIgnoreCase(temp[0]) && nName.equalsIgnoreCase(temp[2]))
				chk = 2;	// ID와 이름 둘 다 중복되는 경우
			else if (nID.equalsIgnoreCase(temp[0]))
				chk = 3;	// ID 중복
			else if (nName.equalsIgnoreCase(temp[2])) 
				chk = 4;	// 이름 중복
			
			i++;
			length = i;
		}

		reader.close();
		try {
			if (chk == 0) {
				returnValue = writeNewUser();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (returnValue != 1)
			returnValue = chk;
		return returnValue;
	} 	

	public String matchUser() throws IOException {
		// 로그인시 아이디를 탐색한다.

		BufferedReader reader = new BufferedReader(new FileReader("user.txt"));
		String line;

		int i = 0;
		while ((line = reader.readLine()) != null) // 라인의 값이 null 이 될때까지 읽어냄
		{
			temp = line.split(" ");
			for (int j = 0; j < 6; j++) {
				user[i][j] = temp[j];
			}
			i++;
			length = i;
		}

		for (i = 0; i < length; i++) {// 로그인 유저가 유저리스트에 존재하는지 탐색
			if (ID.equalsIgnoreCase(user[i][0])) {
				if (PW.equalsIgnoreCase(user[i][1])) {
					try {
						writeFile(i);// 있다면 날짜갱신을 위해 파일 다시써준다.
					} catch (Exception e) {
						e.printStackTrace();
					}
					returnValue = user[i][2];// 리텀값으로 유저이름을 준다.
				}
			}
		}
		reader.close();
		System.out.println("!!!!" + returnValue);
		return returnValue;
	}
	public static void writeFile(int check) throws Exception {
		//로그인을 하게되면 최신로그인일자로 다시 txt파일에 기록한다.
		PrintWriter writer = new PrintWriter(new FileWriter("user.txt"));
		for (int i = 0; i < length; i++) {

			for (int j = 0; j < 6; j++) {
				if (j == 5 && check == i) {
					writer.print(date);
					System.out.println(date);
				} else {
					writer.print(user[i][j] + " ");
					System.out.println(user[i][j] + " ");
				}
			}
			writer.println();
		}
		writer.close();
	}

	public static int writeNewUser() throws Exception {
		//새로운 유저가 들어오면 txt파일에 쓴다.
		PrintWriter writer = new PrintWriter(new FileWriter("user.txt"));
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < 6; j++) {
				writer.print(user[i][j] + " ");
				System.out.println(user[i][j] + " ");
			}
			writer.println();
		}
		writer.print(nID + " " + nPW + " " + nName + " " + "0" + " " + "0"
				+ " " + date);
		writer.println();
		writer.close();
		return 1;
	}
	
	public static int writeNewUser(String prev , String newName) throws Exception {
		//유저의 이름 변경.
		PrintWriter writer = new PrintWriter(new FileWriter("user.txt"));
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < 6; j++) {
				if(user[i][j].equalsIgnoreCase(prev))
					user[i][j] = newName;
				writer.print(user[i][j] + " ");
				System.out.println(user[i][j] + " ");
			}
			writer.println();
		}
		writer.close();
		return 1;
	}
	public String getWin(String findName) throws IOException{
		String temp [] = new String[6];
		BufferedReader reader = new BufferedReader(new FileReader("user.txt"));
		String line;
		int i = 0;
		String returnvalue = null;
		while ((line = reader.readLine()) != null) // 라인의 값이 null 이 될때까지 읽어냄
		{
			temp = line.split(" ");
			for (int j = 0; j < 6; j++) {
				user[i][j] = temp[j];
			}
			i++;
			length = i;
		}

		for (i = 0; i < length; i++) {// 로그인 유저가 유저리스트에 존재하는지 탐색
			if(user[i][2].equalsIgnoreCase(findName))
				returnvalue = user[i][3];	//
				
		}
		reader.close();
		return returnvalue;
	}

	public String getLose(String findName) throws IOException{
		String temp [] = new String[6];
		BufferedReader reader = new BufferedReader(new FileReader("user.txt"));
		String line;
		int i = 0;
		String returnvalue = null;
		while ((line = reader.readLine()) != null) // 라인의 값이 null 이 될때까지 읽어냄
		{
			temp = line.split(" ");
			for (int j = 0; j < 6; j++) {
				user[i][j] = temp[j];
			}
			i++;
			length = i;
		}

		for (i = 0; i < length; i++) {// 로그인 유저가 유저리스트에 존재하는지 탐색
			if(user[i][2].equalsIgnoreCase(findName))
				returnvalue = user[i][4];	//user[i][4] 는 패를 의미함.
				
		}
		reader.close();
		return returnvalue;
	}
	public static int writeNewUser(String a_name,String a_W,String a_L
	        ) throws Exception {
		PrintWriter writer = new PrintWriter(new FileWriter("user.txt"));
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < 6; j++) {
				if(user[i][j].equalsIgnoreCase(a_name)){
					user[i][j+1] = a_W;
					user[i][j+2] = a_L;
				}
			
				writer.print(user[i][j] + " ");
				System.out.println(user[i][j] + " ");
			}
			writer.println();
		}
		writer.close();
		return 1;
	}
	
}
