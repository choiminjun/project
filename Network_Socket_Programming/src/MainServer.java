//신유철 최민준 이석훈 김정민


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;

public class MainServer {

	private static final int PORT = 9001;

	private static HashMap<String, PrintWriter> clients = new HashMap<String, PrintWriter>();
	private static HashSet<String> login_clients = new HashSet<String>();
	private static HashSet<String> inP2P = new HashSet<String>();

	public static void main(String[] args) throws Exception {
		System.out.println("The chat server is running.");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				new Handler(listener.accept()).start(); 
			}
		} finally {
			listener.close();
		}
	}
	// 서버에 연결되어있는 클라이언트 들에게 오버라이딩 하는 핸들러
	private static class Handler extends Thread {
		private String name;
		private Socket socket;
		private BufferedReader in;
		private PrintWriter out;
		String[] INFO;

		public Handler(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			try {
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);

				while (true) {
					String line2 = in.readLine();
					if(line2.startsWith("NEWMEMBER")){// 클라이언트가 회원가입을 요청한 경우
						INFO = line2.split(" ");
						// 유저의 정보가 중복되는지 판단하기 위하여 컨스트럭터 생성						UserInfo userInfo = new UserInfo(INFO[1],INFO[2],INFO[3]);
						UserInfo userInfo = new UserInfo(INFO[1],INFO[2],INFO[3]);
						try{
							int chk_duplicateID = userInfo.isDuplicate();
							if(chk_duplicateID == 1){
								out.println("JOINSUCCESS");// 중복되지 않는다면 클라이언트에게
							}
							else {
								if (chk_duplicateID == 2) 
									out.println("JOINFAIL" + "2");
								else if (chk_duplicateID == 3) 
									out.println("JOINFAIL" + "3");
								else 
									out.println("JOINFAIL" + "4");
							}
						}
						catch(Exception e1){
							e1.printStackTrace();
						}
					}
					else if(line2.startsWith("LOGIN")){// 클라이언트가 로그인을 요청한경우
						INFO = line2.split(" ");	//LOGIN +ID + PW.
						UserInfo user = new UserInfo(INFO[1] , INFO[2]); //ID and PW.
						try{
							name = user.matchUser();
							System.out.println("!!!!!!" + name); //for testing.
							if(name.equalsIgnoreCase("failed")) {
								// 만일 가입되어있지 않은 회원이거나 비밀번호가 틀리다면
								out.println("LOGINFAIL");
								// 오류메시지가 출력된다.
							} else {
								if(login_clients.contains(name))	//if clients already logined.
									out.println("LOGINFAIL_ALREADYLOGINED " + name );
								else{
									// 아이디 비밀번호가 일치했다면
									login_clients.add(name);
									
									out.println("LOGINSUCCESS "+ name+" "+user.getWin(name) +" " +user.getLose(name));
									//name = in.readLine();
									if (name == null) {
										return;
									}
									System.out.println(name);
									clients.put(name, out);
									break;
								}
							}
						}
						catch (IOException e1) {
							e1.printStackTrace();
						}
					}
					else if(line2.startsWith("CHANGEINFO")){
						System.out.println("TESTING..");
						int result_check = 0;
						INFO = line2.split(" ");	//LOGIN +ID + PW.

						//INFO[3] has changing new NAME.
						UserInfo user = new UserInfo(INFO[1] , INFO[2]); //ID and PW.
						try{
							name = user.matchUser();	//변경전 기존의 네임.
							if(name.equalsIgnoreCase("failed")) {
								// 만일 가입되어있지 않은 회원이거나 비밀번호가 틀리다면
								out.println("LOGINFAIL");		//정보수정 실패.
								// 오류메시지가 출력된다.
							} else {
								if(login_clients.contains(name))	//if clients already logined.
									out.println("CHANGE_LOGINFAIL_ALREADYLOGINED " + name );
								else{
									// 아이디 비밀번호가 일치했다면
									//login_clients.add(name);
									result_check =	user.writeNewUser(name, INFO[3]);
									if(result_check == 1)
										out.println("CHANGE_SUCCESS "+ name +" -> " + INFO[3] + "로 변경 되었습니다");
									else
										out.println("CHANGE_FAILED ");
									//name = in.readLine();
									if (name == null) {
										return;
									}
									System.out.println(name);
								}
							}
						}
						catch (IOException e1) {
							e1.printStackTrace();
						}
						//break;
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
				/**
				 * 새로운 클라이언트가 온경우, 위에 while loop를 빠져나와 현제 서버에 접속한 클라이언트들에게 자신이
				 * 왔음을 알린다.
				 */
				String tmp = "";
				for (String key : clients.keySet())
				{
					if (inP2P.contains(key))	// While some clients are having P2P chat, a new client can connect.  
						continue;				// This can hide names of clients on P2P chat when updating the list.
					tmp += key + ",";		
				}	
				for (String key : clients.keySet()) {
					PrintWriter out = clients.get(key);
					out.println("UPDATE_LIST,"
							+ tmp.substring(0, tmp.length() - 1));
				}


				// Greeting an user who entered & broadcasting his/her entrance to other users already in the waiting room.
				for (String keyName : clients.keySet() ) 
				{
					if (clients.get(keyName) == out)	// if it's current thread 'out'
						clients.get(keyName).println("MESSAGE " + "*** Hello " + name + " to our chat room. ***");
					else				// if it's other thread users except for 'out'
						clients.get(keyName).println("MESSAGE " + "*** A new user " + name + " entered the chat room !!! ***");
				}				

				// ///////////////////////////////////////////////////////

				while (true) {
					String input = in.readLine();
					if (input == null) {
						return;
					}
					/**
					 * 대기방에서 한명의 클라이언트가 이외의 다른 클라이언트를 더블 클릭한 경우 클라이언트 클래스이있는
					 * actionperformed 가 수행되어 INIT_CONNECT를 보낸다. 접속을 시도한 자는 P2P
					 * 채팅/게임에서 서버역항을 하고 , 더블클릭 당한 클라이언트는 클라이언트 역할을 한다.
					 */
					if (input.startsWith("INIT_CONNECT")) {
						// "INIT_CONNECT,name,p2pServerSidePort,address"

						String str = input.substring(13);
						String t[] = str.split(",");
						String targetName = t[0]; // target client that's going to be a client on P2P. 
						String callerName = t[1]; // caller that started P2P and is going to be a server on P2P.
						int port = Integer.parseInt(t[2]); // port
						String addr = t[3]; // ip addr
						UserInfo user = new UserInfo(targetName);
						//해당하는 클라이언트에게 메시지를 보내기위한 아웃풋 버퍼생성.
						PrintWriter outStream = clients.get(targetName);
						System.out.println("P2P TEST... "+ targetName);
						//callerName에게 접속준비하라고 메시지를 보낸다.
						outStream.println("READY_CONNECT" + "," + callerName + "," + port + ","
								+ addr+","+user.getWin(targetName)+","+user.getLose(targetName));

						// ///////////////////////////////////////////////////////

					} else if (input.startsWith("MESSAGE")) {												
						for (String keyName : clients.keySet() ) 
							clients.get(keyName).println("MESSAGE " + name + ": " + input.substring(7));						
					} else if (input.startsWith("IN_P2P")) {
						//새로운 p2p 채팅방/게임이 생성되었을 경우 hash셋에 추가하여준다.
						inP2P.add(input.substring(7));	

						// [ "UPDATE LIST" protocol : When a client gets into P2P chat ]
						tmp = "";
						for (String key : clients.keySet())
						{
							if (inP2P.contains(key))	   
								continue;												
							tmp += key + ",";		
						}
						if (tmp.isEmpty())		// To avoid 'StringIndexOutOfBoundsException' when there gets to be no one remaining on the waiting room list.
						{
							for (String key : clients.keySet()) {
								PrintWriter out = clients.get(key);
								out.println("UPDATE_LIST,");
							}							
						}
						else {
							for (String key : clients.keySet()) {
								PrintWriter out = clients.get(key);
								out.println("UPDATE_LIST,"
										+ tmp.substring(0, tmp.length() - 1));
							}
						}
					} else if (input.startsWith("OUT_P2P")) {
						// Remove a client's name in hashset 'inP2P' to make his/her name displayed on the list again
						String[] p2pOUT = input.substring(8).split(",");
						inP2P.remove(p2pOUT[0]);						
						inP2P.remove(p2pOUT[1]);						
						/**
						 * 클라이언트가 p2p채팅방에 들어갈때는 물론이거니와 다시 나올 경우에 업데이트 해준다.
						 */
						String tempMyName = p2pOUT[2];
						String tempMyWin = p2pOUT[3];
						String tempMyLoss = p2pOUT[4];
						String tempOppName = p2pOUT[5];
						String tempOppWin = p2pOUT[6];
						String tempOppLoss = p2pOUT[7];
						System.out.println("test!!!!   " + tempMyName +" "+tempMyWin+" "+ tempMyLoss+" "+tempOppName+" "+ tempOppWin+" "+ tempOppLoss);
						UserInfo user = new UserInfo(INFO[1] , INFO[2]); //ID and PW.
						
						int aa = user.writeNewUser(tempMyName, tempMyWin, tempMyLoss); 
						aa = user.writeNewUser(tempOppName, tempOppWin, tempOppLoss);



						// [ "UPDATE LIST" protocol : When a client finishes P2P chat and gets back to waiting room ]

						tmp = "";
						for (String key : clients.keySet())
						{
							if (inP2P.contains(key))	  
								continue;
							tmp += key + ",";	
						}
						if (tmp.isEmpty())		// To avoid 'StringIndexOutOfBoundsException' when there gets to be no one remaining on the waiting room list.
						{
							for (String key : clients.keySet()) {
								PrintWriter out = clients.get(key);
								out.println("UPDATE_LIST,");
							}							
						}
						else {
							// send update list protocol 
							// to all who is connected with main Server.
							for (String key : clients.keySet()) {
								PrintWriter out = clients.get(key);
								out.println("UPDATE_LIST,"
										+ tmp.substring(0, tmp.length() - 1));
							}
						}
					}
				}
			} catch (IOException e) {
				System.out.println(e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// 클라이언트가 나갈때 알리는 메세지
				for (String keyName : clients.keySet() ) {
					if (clients.get(keyName) != out)
						clients.get(keyName).println("MESSAGE " + "*** The user " + name + " is leaving the chat room !!! ***");
				}

				if (name != null) {
					clients.remove(name);			
					inP2P.remove(name);
                    login_clients.remove(name);
					String tmp = "";
					for (String key : clients.keySet())
					{
						if (inP2P.contains(key))	// While some clients are having P2P chat, some clients can disconnect. 
							continue;
						tmp += key + ",";
					}

					if (tmp.isEmpty())
					{
						for (String key : clients.keySet()) {
							PrintWriter out = clients.get(key);
							out.println("UPDATE_LIST,");
						}							
					}
					else {// 현제 메인서버에 연결되어있는 클라이언트들에게 업데이트 메시지를 보낸다.
						for (String key : clients.keySet()) {
							PrintWriter out = clients.get(key);
							out.println("UPDATE_LIST,"
									+ tmp.substring(0, tmp.length() - 1));
						}
					}
				}
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}