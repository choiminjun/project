/**
Program SimpleTipCalculator.
Author : Choi Min Jun.
Email Address: choiminjun0720@gmail.com
Hw 2-b.
Last Changed : 19/03/2014.
 */
package com.example.simpletipcalculator;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentsActivity extends FragmentActivity implements Fragment1.CustomOnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.main);

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager()
				.beginTransaction();
		// ---get the current display info---
		WindowManager wm = getWindowManager();
		Display d = wm.getDefaultDisplay();
		if (d.getWidth() > d.getHeight()) {
			// ---landscape mode---
			setContentView(R.layout.main2);
			// Fragment1 fragment1 = new Fragment1(); // android.R.id.content
			// refers to the content // view of the activity
			// fragmentTransaction.replace(
			// fragmentTransaction.replace( android.R.id.content, fragment1);
		} else {
			setContentView(R.layout.main);
			// Fragment2 fragment2 = new Fragment2();
			// fragmentTransaction.replace( android.R.id.content, fragment2);
		}
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

	}

	@Override
	public void onClicked(int id) {
		Fragment2 fragment2 = (Fragment2)getFragmentManager().findFragmentById(R.id.fragment2);
		
	}

}
