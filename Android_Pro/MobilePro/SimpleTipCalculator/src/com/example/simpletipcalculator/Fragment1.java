package com.example.simpletipcalculator;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Fragment1 extends Fragment{
	Button calBtn;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.fragment1, container, false);
	}
	
	public void onStart(){
		calBtn = (Button)this.getActivity().findViewById(R.id.calBtn);
		calBtn.setOnClickListener(onClickListener);
		super.onStart();
	}
	private CustomOnClickListener customListener;
	
	public interface CustomOnClickListener{
		public void onClicked(int id);
	}
	
	OnClickListener onClickListener = new OnClickListener(){
		public void onClick(View v){
			customListener.onClicked(v.getId());
		}
	};
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		customListener = (CustomOnClickListener)activity;
	}
	
}
