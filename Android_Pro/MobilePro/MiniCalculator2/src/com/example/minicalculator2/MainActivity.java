package com.example.minicalculator2;
//기본적인 연산이 되는 상태.

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	String str = ""; // for calculating.
	String temp = "";
	String prev_op = "EMPTY";
	int x1 = 0;
	int x2 = 0;
	int tot = 0;
	int temp_num = 0;
	int prev_tot = 0;
	boolean fractal = false;
	boolean checking_operation = true;
	boolean initial_checking = true;
	// boolean fractal2 = false;
	TextView main;
	Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
	Button btnC, btnDivide, btnMultiply, btnDeduct, btnAdd, btnCalculate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		main = (TextView) findViewById(R.id.Main);
		btn0 = (Button) findViewById(R.id.Btn00);
		btn0.setOnClickListener(this);
		btn1 = (Button) findViewById(R.id.Btn01);
		btn1.setOnClickListener(this);
		btn2 = (Button) findViewById(R.id.Btn02);
		btn2.setOnClickListener(this);
		btn3 = (Button) findViewById(R.id.Btn03);
		btn3.setOnClickListener(this);
		btn4 = (Button) findViewById(R.id.Btn04);
		btn4.setOnClickListener(this);
		btn5 = (Button) findViewById(R.id.Btn05);
		btn5.setOnClickListener(this);
		btn6 = (Button) findViewById(R.id.Btn06);
		btn6.setOnClickListener(this);
		btn7 = (Button) findViewById(R.id.Btn07);
		btn7.setOnClickListener(this);
		btn8 = (Button) findViewById(R.id.Btn08);
		btn8.setOnClickListener(this);
		btn9 = (Button) findViewById(R.id.Btn09);
		btn9.setOnClickListener(this);
		// operating buttons.
		btnC = (Button) findViewById(R.id.Btn_Cancel);
		btnC.setOnClickListener(this);
		btnDivide = (Button) findViewById(R.id.Btn_Divide);
		btnDivide.setOnClickListener(this);
		btnMultiply = (Button) findViewById(R.id.Btn_Multiply);
		btnMultiply.setOnClickListener(this);
		btnDeduct = (Button) findViewById(R.id.Btn_Deduct);
		btnDeduct.setOnClickListener(this);
		btnAdd = (Button) findViewById(R.id.Btn_Add);
		btnAdd.setOnClickListener(this);
		btnCalculate = (Button) findViewById(R.id.Btn_Calculate);
		btnCalculate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// temp_num = Integer.parseInt(main.getText().toString());
		/*
			if (v.getId() == btn0.getId() || v.getId() == btnDivide.getId()
					|| v.getId() == btnMultiply.getId()
					|| v.getId() == btnDeduct.getId()
					|| v.getId() == btnAdd.getId()
					|| v.getId() == btnCalculate.getId()) {
				
				Toast.makeText(this, "적합한 버튼을 누르시오 ", Toast.LENGTH_LONG).show();
			}
		} else {
			*/
			if (fractal == true) {
				/**
				 * prev_tot = Integer.parseInt(main.getText().toString()); //
				 * save tot // for next // calculating.
				 * **/
				if (prev_op.equals("CALCULATED")) { // when button(=) had
													// clicked,
					// text field need to maintain
					// values for next
					// calculation.if program
					// doesn't, it makes error
					// because that program tries to
					// get text, but text field is
					// empty
					;
				} else if (prev_op.equals("ADD") || prev_op.equals("DEDUCT")
						|| prev_op.equals("DIVIDE")
						|| prev_op.equals("MULTIPLY")) {
					try {
						main.setText("");
					} catch (RuntimeException ae) {
						;
					}
				} else { // when user puts '=' button not approperly.
					main.setText("");
					Toast.makeText(this, "적합한 버튼을 누르시오 ", Toast.LENGTH_LONG)
							.show();
				}
			}
			else if(fractal == false){
				
			}
			

			if (v.getId() == btn0.getId()) {
				str = main.getText() + "0";
				main.setText(str);

			}
			if (v.getId() == btn1.getId()) {
				str = main.getText() + "1";
				main.setText(str);
			}
			if (v.getId() == btn2.getId()) {
				str = main.getText() + "2";
				main.setText(str);
			}
			if (v.getId() == btn3.getId()) {
				str = main.getText() + "3";
				main.setText(str);
			}
			if (v.getId() == btn4.getId()) {
				str = main.getText() + "4";
				main.setText(str);
			}
			if (v.getId() == btn5.getId()) {
				str = main.getText() + "5";
				main.setText(str);
			}
			if (v.getId() == btn6.getId()) {
				str = main.getText() + "6";
				main.setText(str);
			}
			if (v.getId() == btn7.getId()) {
				str = main.getText() + "7";
				main.setText(str);
			}
			if (v.getId() == btn8.getId()) {
				str = main.getText() + "8";
				main.setText(str);
			}
			if (v.getId() == btn9.getId()) {
				str = main.getText() + "9";
				main.setText(str);
			}
			if (v.getId() == btnC.getId()) {
				str = "";
				main.setText("");
				initial_checking = false;
				prev_op = "EMPTY";
			}

			if (v.getId() == btnAdd.getId() || v.getId() == btnDeduct.getId()
					|| v.getId() == btnMultiply.getId()
					|| v.getId() == btnDivide.getId()
					|| v.getId() == btnCalculate.getId()) {

				temp = main.getText().toString();
				// x1 = Integer.parseInt(temp);
				/*
				 * if(temp.equals("")) //when there is no number to operate.
				 * Toast.makeText(this,"You have to click button first!!" ,
				 * Toast.LENGTH_LONG).show();
				 */

				main.setText(""); // ""

				if (prev_op.equals("EMPTY")) {
					try {
						tot = Integer.parseInt(temp);
					} catch (RuntimeException ae) {
						;
					}
					if (v.getId() == btnAdd.getId())
						prev_op = "ADD";
					else if (v.getId() == btnDeduct.getId())
						prev_op = "DEDUCT";
					else if (v.getId() == btnMultiply.getId())
						prev_op = "MULTIPLY";
					else if (v.getId() == btnDivide.getId())
						prev_op = "DIVIDE";
				} else {
					try {
						x2 = Integer.parseInt(temp);
					} catch (NumberFormatException ae) {
						Toast.makeText(
								this,
								"Hey Hey are you kidding? before you have inputted "
										+ prev_op
										+ "!! Operation can't come sequently!! PUT C BUTTON and RESTART! ",
								Toast.LENGTH_LONG).show();

						tot = 0; // initializing total number
					}
					if (prev_op.equals("ADD")) {
						tot += x2;
						main.setText(Integer.toString(tot));
					} else if (prev_op.equals("DEDUCT")) {
						tot -= x2;
						main.setText(Integer.toString(tot));
					} else if (prev_op.equals("MULTIPLY")) {
						tot *= x2;
						main.setText(Integer.toString(tot));
					} else if (prev_op.equals("DIVIDE")) {
						tot /= x2;
						main.setText(Integer.toString(tot));
					}

					// new allocating operation.
					if (v.getId() == btnCalculate.getId()) {
						prev_op = "CALCULATED";
						// main.setText(Integer.toString(i));
					} else if (v.getId() == btnAdd.getId())
						prev_op = "ADD";
					else if (v.getId() == btnDeduct.getId())
						prev_op = "DEDUCT";
					else if (v.getId() == btnMultiply.getId())
						prev_op = "MULTIPLY";
					else if (v.getId() == btnDivide.getId())
						prev_op = "DIVIDE";

				}
				fractal = true; // this is when operation button pressed. by
								// this
								// boolean variable, we can catch what
								// type(number,operation) of approperate buttons
								// are
								// next.

			} else
				fractal = false;
			str = "";
		}
	
}
