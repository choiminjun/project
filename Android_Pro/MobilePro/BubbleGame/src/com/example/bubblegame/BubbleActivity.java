/**
 Program Bubble Game.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 HW 4- 1.
 Last Changed : 13/05/2014.
 */

package com.example.bubblegame;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

public class BubbleActivity extends Activity {
	ActionBar actionbar = null;
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		actionbar = getActionBar();
		actionbar.hide();	
		setContentView(new BubbleView(this)	); //display view.
		
	}

}
