package com.example.bubblegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class BubbleView extends View {
	//get display.
	Display display = ((WindowManager) this.getContext().getSystemService(
			Context.WINDOW_SERVICE)).getDefaultDisplay();

	private int boxWidth = display.getWidth();
	private int boxHeight = display.getHeight() + 50;

	private Bitmap state1 = BitmapFactory.decodeResource(getResources(),
			R.drawable.state1);
	private Bitmap state2 = BitmapFactory.decodeResource(getResources(),
			R.drawable.state2);
	private Bitmap state3 = BitmapFactory.decodeResource(getResources(),
			R.drawable.state3);
	private Bitmap finish_state = BitmapFactory.decodeResource(getResources(),
			R.drawable.finish_state);
/**
 *  click_count : save how many times had clicked for each box_bubbles[][] Rect.
 *  box_bubble  : for drawing bitmaps.
 */
	private int[][] click_count;
	private Rect[][] box_bubble;
	MediaPlayer sound; // for button sound.
/**
 * Constructor called by BubbleActivity.
 * @param context
 */
	public BubbleView(Context context) {
		super(context);
		box_bubble = new Rect[3][3];
		click_count = new int[3][3];
		sound = MediaPlayer.create(getContext(), R.raw.button); // referencing
																// sound.
		this.init(); // call init method in constructor not to make error.
	}
/**
 * init method called by constructor, it initialize variables.
 */
	private void init() {
		for (int row = 0; row < box_bubble.length; row++) {
			for (int col = 0; col < box_bubble[row].length; col++) {
				box_bubble[row][col] = new Rect();
				click_count[row][col] = 0;
			}
		}

		state1 = Bitmap.createScaledBitmap(state1, boxWidth / 3, boxHeight / 5,
				true);
		state2 = Bitmap.createScaledBitmap(state2, boxWidth / 3, boxHeight / 5,
				true);
		state3 = Bitmap.createScaledBitmap(state3, boxWidth / 3, boxHeight / 5,
				true);
		finish_state = Bitmap.createScaledBitmap(finish_state, boxWidth,
				boxHeight, true);
	}
/**
 * draw bitmaps depend on variable of click_count.
 * program judges and draw what bitmaps to draw.
 */
	public void onDraw(Canvas canvas) {
		Paint Title = new Paint();
		Title.setColor(Color.BLACK);
		Title.setTextSize(boxWidth / 13);
		canvas.drawColor(Color.WHITE);
		canvas.drawText("Minjun's BUBBLE GAME", boxWidth / 60, boxHeight / 8, Title);
		for (int row = 0; row < box_bubble.length; row++) {
			for (int col = 0; col < box_bubble[row].length; col++) {
				if (click_count[row][col] == 0) {
					canvas.drawBitmap(state1, boxWidth / 3 * row, boxHeight / 5
							* (col + 1), null);
				}
				if (click_count[row][col] == 1) {
					canvas.drawBitmap(state2, boxWidth / 3 * row, boxHeight / 5
							* (col + 1), null);
				}
				if (click_count[row][col] == 2) {
					canvas.drawBitmap(state3, boxWidth / 3 * row, boxHeight / 5
							* (col + 1), null);
				}
				
				//when all button had clicked, finish program.
				if (check_finish() == true) {
					System.exit(1);
				}
			}
		}
	}
/**
 * When user clicks, this method redraw by invalidate() method.
 * and decide which button had clicked by .contains(x,y) method.
 */
	public boolean onTouchEvent(MotionEvent event) {
		// get x,y coordinate.
		int x_pos = (int) event.getX();
		int y_pos = (int) event.getY();

		for (int row = 0; row < box_bubble.length; row++) {
			for (int col = 0; col < box_bubble[row].length; col++) {
				// set position.
				box_bubble[row][col].set(boxWidth / 3 * row, boxHeight / 5
						* (col + 1), boxWidth / 3 * row + state1.getWidth(),
						boxHeight / 5 * (col + 1) + state1.getHeight());

				if (box_bubble[row][col].contains(x_pos, y_pos)) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						break;
					case MotionEvent.ACTION_UP:
						sound.start();
						click_count[row][col]++;
						invalidate();
						break;
					}
				}
			}
		}
		return true;
	}

	/**
	 * return false if only there is entirely not finished Rect.
	 * @return
	 */
	public boolean check_finish() {
		for (int row = 0; row < box_bubble.length; row++) {
			for (int col = 0; col < box_bubble[row].length; col++) {
				if(click_count[row][col] < 3)
					return false;
			}
		}
		return true;
	}
}
