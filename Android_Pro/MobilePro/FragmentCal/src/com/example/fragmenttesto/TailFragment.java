package com.example.fragmenttesto;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.fragmentcal.R;
 
public class TailFragment extends Fragment{
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tail_fragment, container, false);
    }
     /**
      * show result.
      * @param text
      */
    public void setResult(String text){
        TextView result = (TextView)getView().findViewById(R.id.result);
        result.setText(text);
    }
}
