package com.example.fragmenttesto;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import com.example.fragmentcal.R;

/**
 * for exception handling
 */
class AExceptionOne extends Exception {
}

class AExceptionTwo extends Exception {
}

class AExceptionThree extends Exception {
}

public class HeadFragment extends Fragment {

	TextView Name;
	EditText Price_Input;
	RadioButton rb1, rb2, rb3;
	EditText rb3_Input; // which gets other rate of tax.
	TextView result; // result that show adding TAX and PRICE.

	String display_result = null;
	private Button calBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.head_fragment, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		Price_Input = (EditText) getActivity().findViewById(R.id.value);
		rb1 = (RadioButton) getActivity().findViewById(R.id.radio01);
		rb2 = (RadioButton) getActivity().findViewById(R.id.radio02);
		rb3 = (RadioButton) getActivity().findViewById(R.id.radio03);
		rb3_Input = (EditText) getActivity().findViewById(R.id.radio03_Text);
		result = (TextView) getActivity().findViewById(R.id.result);

		calBtn = (Button) this.getActivity().findViewById(R.id.calBtn);
		// 버튼에 OnClickListener를 설정
		calBtn.setOnClickListener(onClickListener);
	}

	/**
	 * Activity 로 데이터를 전달할 커스텀 리스너
	 */
	private CustomOnClickListener customListener;

	/**
	 * Activity 로 데이터를 전달할 커스텀 리스너의 인터페이스
	 */
	public interface CustomOnClickListener {
		public void onClicked(int id);
	}

	/**
	 * 버튼에 설정한 OnClickListener의 구현, 버튼이 클릭 될 때마다 Activity의 커스텀 리스너를 호출함
	 */
	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				/**
				 * 여기에서 display() 를 호출하는 이유는 이곳이 calculate 버튼이 클릭될시에 발생되는 공간이이기
				 * 때문이다. 버튼이 클릭될 시 MainActivity에서 감지를하는데, 해당 정보를 수집하여 TIP과
				 * RESULT를 미리 계산하여 전역 변수인 display_result에 setting 시켜놓아
				 * MainActivity 에서 get_Result() 함수의 호출을 기다린다.
				 */
				display();
			} catch (AExceptionTwo e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AExceptionThree e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			customListener.onClicked(v.getId());
		}
	};

	/**
	 * Activity 로 데이터를 전달할 커스텀 리스너를 연결
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		customListener = (CustomOnClickListener) activity;
	}

	/**
	 * MainActivity로의 반환.
	 * 
	 * @return
	 */
	public String getResult() {
		return display_result;
	}

	static void checking(int i, int price) throws AExceptionOne, AExceptionTwo,
			AExceptionThree {
		if (i < 0)
			throw new AExceptionOne();
		else if (i == 0)
			throw new AExceptionTwo();
		else if (price <= 0)
			throw new AExceptionThree();
	}

	/**
	 * get result of display.
	 */
	public void display() throws AExceptionTwo, NumberFormatException,
			AExceptionThree {
		String price = Price_Input.getText().toString();
		String rate = "";
		int tip_rate;
		double tip;
		double total; // price + tax.

		try {

			if (rb1.isChecked())
				rate = rb1.getText().toString().substring(0, 2); // get rate.
			if (rb2.isChecked())
				rate = rb2.getText().toString().substring(0, 2);
			if (rb3.isChecked()) {
				rate = rb3_Input.getText().toString();
			}
			checking(Integer.parseInt(rate), Integer.parseInt(price));
			tip_rate = Integer.parseInt(rate); // convert to int because
												// EditText can read only
												// String.

			tip = Integer.parseInt(price) * (double) (tip_rate / 100.0); // calculating
																			// tip.
			total = tip + Integer.parseInt(price);
			display_result = "Tip : " + Double.toString(tip) + "\nTotal : "
					+ Double.toString(total);

			// result.setText(display);
			// Toast.makeText(this, display, Toast.LENGTH_LONG).show();

		} catch (AExceptionOne o) {
			result.setText("You Can not negative number for rate!!");
		} catch (AExceptionTwo t) {
			result.setText("You Can not put 0 for rate");
		} catch (AExceptionThree s) {
			result.setText("Price must bigger than 0!");

		}
	}
}
