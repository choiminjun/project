package com.example.hw5;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import android.widget.TextView;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {

	/**
	 * Onreceive method is called when user receives SMS message.
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		// getting SharedPrefernce for compare secret key with own's
		// and from secret key whic came from secret key.
		SharedPreferences prefs = context.getSharedPreferences("File_Code",
				Context.MODE_PRIVATE); // File_Code is MainActivty's
										// sharedPreferences.
		String mySecretKey = prefs.getString("secretCode", "NULL"); // get
																	// secret
																	// code.

		// ---get the SMS message passed in-
		Bundle bundle = intent.getExtras();

		SmsMessage[] msgs = null;
		String str = "";
		if (bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];

			for (int i = 0; i < msgs.length; i++)
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

			/**
			 * If SMS message starts with LOCATION, then get latitude and
			 * longitude and translate to address kind 대한민국 서울시 송파구 .. and call
			 * updateAddress/updateGoogleMap static method which exist in
			 * MainActivity.class.
			 */
			if (msgs[0].getMessageBody().startsWith("LOCATION")) {
				String receivedMsg = msgs[0].getMessageBody();
				System.out.println("TEST SUCEESS!!" + receivedMsg);
				Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
				try {
					/**
					 * 위도와 경도를 이용하여 주소를 구한다음, 주소를 표시해준다 텍스트 형식으로 주소도 표시해주기!
					 */
					String[] split = receivedMsg.split(",");
					double latitude = Double.parseDouble(split[1]);
					double longitude = Double.parseDouble(split[2]);
					List<Address> addresses = geoCoder.getFromLocation(
							latitude, longitude, 1);
					System.out.println(addresses.get(0).getAddressLine(0)
							.toString());
					/**
					 * updateAddress is show current address which received
					 * current. updateGoogleMap is showing GoogleMap which
					 * dervied from location.
					 */
					MainActivity.updateAddress(addresses.get(0)
							.getAddressLine(0).toString());
					MainActivity.updateGoogleMap(latitude, longitude, addresses
							.get(0).getAddressLine(0).toString());
					// Toast.makeText(context, addresses.toString(),
					// Toast.LENGTH_LONG);
					String add = "";
					if (addresses.size() > 0) {
						for (int i = 0; i < addresses.get(0)
								.getMaxAddressLineIndex(); i++)
							add += addresses.get(0).getAddressLine(i) + "\n";
						System.out.println(add);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			/**
			 * When user received temporal secretkey from other user by SMS
			 * message.
			 */
			else {
				// test/
				String callerNumber = msgs[0].getOriginatingAddress();
				String carriedSecretKey = msgs[0].getMessageBody().toString();

				/**
				 * Comparing two secret keys..
				 */
				if (mySecretKey.equals(carriedSecretKey)) {
					Intent myIntent = new Intent(context, MinjunService.class);

					/*
					 * put callerNumber in intent because this is used to send
					 * my current location(latitude,longitude). If the other man
					 * sends his location to user, then invoking a google map
					 * with location
					 */
					myIntent.putExtra("callerNumber", callerNumber);
					context.startService(myIntent);
				} else {
					// keys are not equal..
					;
				}
			}
		}
	}
}
