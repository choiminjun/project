package com.example.orderingapp2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment2 extends Fragment {
	EditText t1,t2;
	RadioButton rb1,rb2,rb3,rb4,rb5;
	CheckBox cb1,cb2,cb3;
	Button btn;
	String str1,str2,str3;	//str1 is for name and phone 2 is for menu, 3 is for service		
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment2, container, false);
	}
	
	public void onStart(){
		super.onStart();
		
		t1 = (EditText)getActivity().findViewById(R.id.Name);
		t2 = (EditText)getActivity().findViewById(R.id.Phone);
		rb1 = (RadioButton)getActivity().findViewById(R.id.radio01);
		rb2 = (RadioButton)getActivity().findViewById(R.id.radio02);
		rb3 = (RadioButton)getActivity().findViewById(R.id.radio03);
		rb4 = (RadioButton)getActivity().findViewById(R.id.radio04);
		rb5 = (RadioButton)getActivity().findViewById(R.id.radio05);
		cb1 = (CheckBox)getActivity().findViewById(R.id.check01);
		cb2 = (CheckBox)getActivity().findViewById(R.id.check02);
		cb3 = (CheckBox)getActivity().findViewById(R.id.check03);
		
		btn = (Button)getActivity().findViewById(R.id.button);
		btn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				display();
			}
		});
	}
	public void display(){
		str1 =t1.getText().toString();      //get text.
		str1 += "님, 연락처:  ";
		str1 +=t2.getText().toString() +"\n";       //for toast message.
		if(rb1.isChecked())
			str1 +="메뉴 : " +  rb1.getText().toString() + "\n ";
		if(rb2.isChecked())
			str1 +="메뉴 : " +  rb2.getText().toString() + "\n ";
		if(rb3.isChecked())
			str1 +="메뉴 : " +  rb3.getText().toString()+ "\n ";
		if(rb4.isChecked())
			str1 +="사이즈  : " +  rb4.getText().toString() + "\n";
		if(rb5.isChecked())
			str1 +="사이즈 : " + rb5.getText().toString() + "\n";
		
		str1 += "서비스 요구사항 : ";
		if(cb1.isChecked())
			str1 += cb1.getText().toString() +" ";
		if(cb2.isChecked())
			str1 += cb2.getText().toString() + " ";
		if(cb3.isChecked())
			str1 += cb3.getText().toString()+ " ";
		String display = str1;
		
		TextView showResult = (TextView)getActivity().findViewById(R.id.showResult);
		showResult.setText(display);		//show result.
		
	//	Toast.makeText(getActivity(), display, Toast.LENGTH_LONG).show();
	}
}