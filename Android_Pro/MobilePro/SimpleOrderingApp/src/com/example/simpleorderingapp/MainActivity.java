/**
Program SimpleOrderingApp.
Author : Choi Min Jun.
Email Address: choiminjun0720@gmail.com
Hw 2-a.
Last Changed : 20/03/2014.
*/

package com.example.simpleorderingapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	EditText t1,t2;
	RadioButton rb1,rb2,rb3,rb4,rb5;
	CheckBox cb1,cb2,cb3;
	Button btn;
	String str1,str2,str3;	//str1 is for name and phone 2 is for menu, 3 is for service		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		t1 = (EditText)findViewById(R.id.Name);
		t2 = (EditText)findViewById(R.id.Phone);
		rb1 = (RadioButton)findViewById(R.id.radio01);
		rb2 = (RadioButton)findViewById(R.id.radio02);
		rb3 = (RadioButton)findViewById(R.id.radio03);
		rb4 = (RadioButton)findViewById(R.id.radio04);
		rb5 = (RadioButton)findViewById(R.id.radio05);
		cb1 = (CheckBox)findViewById(R.id.check01);
		cb2 = (CheckBox)findViewById(R.id.check02);
		cb3 = (CheckBox)findViewById(R.id.check03);
		
		btn = (Button)findViewById(R.id.button);
		btn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				display();
			}
		});
	}
	public void display(){
		str1 =t1.getText().toString();      //get text.
		str1 += "님, 연락처:  ";
		str1 +=t2.getText().toString() +"\n";       //for toast message.
		if(rb1.isChecked())
			str1 +="메뉴 : " +  rb1.getText().toString() + "\n ";
		if(rb2.isChecked())
			str1 +="메뉴 : " +  rb2.getText().toString() + "\n ";
		if(rb3.isChecked())
			str1 +="메뉴 : " +  rb3.getText().toString()+ "\n ";
		if(rb4.isChecked())
			str1 +="사이즈  : " +  rb4.getText().toString() + "\n";
		if(rb5.isChecked())
			str1 +="사이즈 : " + rb5.getText().toString() + "\n";
		
		str1 += "서비스 요구사항 : ";
		if(cb1.isChecked())
			str1 += cb1.getText().toString() +" ";
		if(cb2.isChecked())
			str1 += cb2.getText().toString() + " ";
		if(cb3.isChecked())
			str1 += cb3.getText().toString()+ " ";
		String display = str1;
		Toast.makeText(this, display, Toast.LENGTH_LONG).show();
	}
}
