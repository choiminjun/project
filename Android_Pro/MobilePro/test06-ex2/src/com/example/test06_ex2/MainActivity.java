package com.example.test06_ex2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewICS.OnItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	String[] items = { "Data - 0", "Data - 1", "Data - 2", "Data - 3",
			"Data - 4", "Data - 5", "Data - 6", "Data - 7" };
	ListView myListView;
	TextView txtMsg;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		/*
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1,android.R.id.text1, items));
	*/	
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,items);
		myListView.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> av, View v, int position, long id){
				String text = "Position : " + position + " \nDats: " + items[position];
				txtMsg.setText(text);
			}
		});
		txtMsg = (TextView) findViewById(R.id.txtMsg);
	}

	/*
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		String text = " Position: " + position + " " + items[position];
		txtMsg.setText(text);
	}
	*/
	
	
}
