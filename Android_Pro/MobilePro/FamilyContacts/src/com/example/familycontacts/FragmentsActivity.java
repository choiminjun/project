package com.example.familycontacts;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.WindowManager;

public class FragmentsActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager()
				.beginTransaction();
		// ---get the current display info---
		WindowManager wm = getWindowManager();
		Display d = wm.getDefaultDisplay();
		if (d.getWidth() > d.getHeight()) {
			// ---landscape mode---
			setContentView(R.layout.fragment_width);
		} else {
			setContentView(R.layout.fragment_height);
		}
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

	}

}
