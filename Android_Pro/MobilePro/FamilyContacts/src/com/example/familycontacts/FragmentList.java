package com.example.familycontacts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentList extends Fragment  {
	private static final int REQUEST_CODE = 1001;
	TextView name, email, tel, description;
	ListView namelist;
	
	// row0: name, row1:email, row2:tel, row3:description.
	String[][] info = new String[4][10];

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_list, container, false);
	}

	public void onStart() {
		super.onStart();
		// saving info in 2dimensional array.
		info[0] = getResources().getStringArray(R.array.friend_name);
		info[1] = getResources().getStringArray(R.array.friend_email);
		info[2] = getResources().getStringArray(R.array.friend_tel);
		info[3] = getResources().getStringArray(R.array.friend_desc);
		ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_1, info[0]);
		namelist = (ListView) getActivity().findViewById(R.id.list);

		namelist.setAdapter(nameAdapter);
		name = (TextView) getActivity().findViewById(R.id.name);
		email = (TextView) getActivity().findViewById(R.id.email);
		tel = (TextView) getActivity().findViewById(R.id.tel);
		description = (TextView) getActivity().findViewById(R.id.description);

		namelist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// TODO Auto-generated method stub
				/**
				 * send data using Intent by this we dont have to reassign
				 * values in new activity class.
				 */
				Bundle data = new Bundle();
				data.putString("name", info[0][position]);
				data.putString("email", info[1][position]);
				data.putString("tel", info[2][position]);
				data.putString("description", info[3][position]);

				FragmentManager fragmentManager = getActivity()
						.getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = getActivity()
						.getSupportFragmentManager().beginTransaction();
				WindowManager wm = getActivity().getWindowManager();
				Display d = wm.getDefaultDisplay();
				/**
				 * If 가로,then send bundle for using method then this method shows information.
				 * but if 세로,then make intent and it starts Activity using InfoActivity.java.
				 */
				if (d.getWidth() > d.getHeight()) {
					FragmentInfo fragmentInfo = (FragmentInfo)getFragmentManager().findFragmentById(R.id.fragment_info);
					fragmentInfo.showData(data);
				} else {
					Intent infoIntent = new Intent(getActivity(),
							InfoActivity.class);
					infoIntent.putExtras(data);
					startActivityForResult(infoIntent, REQUEST_CODE);
				}
			}

		});
	}

	public void onActivityResult(int requestCode, int resultCode) {
		if ((requestCode == REQUEST_CODE) && (resultCode == Activity.RESULT_OK)) {
			Toast.makeText(this.getActivity(), "새로운 친구를 클릭하세요",
					Toast.LENGTH_LONG);
			// finish();
		}
	}

}
