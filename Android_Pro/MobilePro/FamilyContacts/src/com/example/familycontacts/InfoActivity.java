package com.example.familycontacts;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class InfoActivity extends Activity implements OnClickListener {

	TextView name, email, tel, description;
	ListView namelist;
	Button sendEmailBtn, sendTelBtn, sendSmsBtn, backBtn;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_info);
		/**
		 * This class has appeared from FragmentList's
		 * startActivityForResult(Intent), intent which includes data for
		 * showing, so make intent and recevie bundle.
		 */
		Intent receiveIntent = new Intent();
		receiveIntent = getIntent();
		Bundle receiveData = new Bundle();
		receiveData = receiveIntent.getExtras();

		// matching with buttons.
		name = (TextView) findViewById(R.id.name);
		email = (TextView) findViewById(R.id.email);
		tel = (TextView) findViewById(R.id.tel);
		description = (TextView) findViewById(R.id.description);
		sendEmailBtn = (Button) findViewById(R.id.sendEmailBtn);
		sendEmailBtn.setOnClickListener(this);
		sendTelBtn = (Button) findViewById(R.id.sendCallBtn);
		sendTelBtn.setOnClickListener(this);
		sendSmsBtn = (Button) findViewById(R.id.sendSmsBtn);
		sendSmsBtn.setOnClickListener(this);

		backBtn = (Button) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(this);

		// show data which is from Bundle.
		name.setText(receiveData.getString("name"));
		email.setText(receiveData.getString("email"));
		tel.setText(receiveData.getString("tel"));
		description.setText(receiveData.getString("description"));
		
	}

	@Override
	public void onClick(View v) {

		// when user click smsBtn.
		if (v.getId() == sendEmailBtn.getId()) {
			if (email.getText().toString().equals("")) {
				Toast.makeText(this, "Thers no Input", Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_SENDTO,
						Uri.parse("mailto:" + email.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user click smsBtn.
		else if (v.getId() == sendTelBtn.getId()) {
			if (tel.getText().toString().equals("")) {
				Toast.makeText(this, "Thers no Input", Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_DIAL,
						Uri.parse("tel:" + tel.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user clicks smsBtn.
		else if (v.getId() == sendSmsBtn.getId()) {
			if (tel.getText().toString().equals("")) {
				Toast.makeText(this, "Thers no Input", Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_SENDTO,
						Uri.parse("smsto:" + tel.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user clicks backBtn.
		else if (v.getId() == backBtn.getId()) {
			setResult(Activity.RESULT_OK); // don't need to send intent.
			finish();
		}
	}

}
