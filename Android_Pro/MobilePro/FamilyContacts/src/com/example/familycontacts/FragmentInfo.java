package com.example.familycontacts;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentInfo extends Fragment implements OnClickListener {
	TextView name, email, tel, description;
	Button sendEmailBtn, sendTelBtn, sendSmsBtn, backBtn;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_info, container, false);
	}

	public void onStart() {
		super.onStart();
		name = (TextView) getActivity().findViewById(R.id.name);
		email = (TextView) getActivity().findViewById(R.id.email);
		tel = (TextView) getActivity().findViewById(R.id.tel);
		description = (TextView) getActivity().findViewById(R.id.description);
		sendEmailBtn = (Button) getActivity().findViewById(R.id.sendEmailBtn);
		sendEmailBtn.setOnClickListener(this);
		sendTelBtn = (Button) getActivity().findViewById(R.id.sendCallBtn);
		sendTelBtn.setOnClickListener(this);
		sendSmsBtn = (Button) getActivity().findViewById(R.id.sendSmsBtn);
		sendSmsBtn.setOnClickListener(this);
		backBtn = (Button) getActivity().findViewById(R.id.backBtn);
		backBtn.setOnClickListener(this);
	}

	/**
	 * This method show data which is derived from Bundle(In 세로 case). by using
	 * this, method can get data.
	 */
	public void showData(Bundle receiveData) {
		name.setText(receiveData.getString("name"));
		email.setText(receiveData.getString("email"));
		tel.setText(receiveData.getString("tel"));
		description.setText(receiveData.getString("description"));

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// when user click smsBtn.
		if (v.getId() == sendEmailBtn.getId()) {
			if (email.getText().length() == 0) {
				Toast.makeText(this.getActivity(), "Thers no Input",
						Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_SENDTO,
						Uri.parse("mailto:" + email.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user click smsBtn.
		else if (v.getId() == sendTelBtn.getId()) {
			if (tel.getText().length() == 0) {
				Toast.makeText(this.getActivity(), "Thers no Input",
						Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_DIAL,
						Uri.parse("tel:" + tel.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user clicks smsBtn.
		else if (v.getId() == sendSmsBtn.getId()) {
			if (tel.getText().length() == 0) {
				Toast.makeText(this.getActivity(), "Thers no Input",
						Toast.LENGTH_SHORT);
			} else {
				Intent newIntent = new Intent(Intent.ACTION_SENDTO,
						Uri.parse("smsto:" + tel.getText().toString()));
				startActivity(newIntent);
			}
		}
		// when user clicks backBtn.
		else if (v.getId() == backBtn.getId()) {
			Toast.makeText(this.getActivity(), "가로 모드에서는 뒤로 돌아가기 버튼이 필요 없습니다.",
					Toast.LENGTH_SHORT);

		}

	}

}
