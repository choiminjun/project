/**
Program SimpleCounter.
Author : Choi Min Jun.
Email Address: choiminjun0720@gmail.com
Hw 2-a.
Last Changed : 19/03/2014.
*/
package com.example.simplecounter;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	public static int value = 0;
	public static TextView text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text = (TextView) findViewById(R.id.Value);
		Button upBtn = (Button) findViewById(R.id.upBtn);
		upBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				update(1);
			}
		});
		Button downBtn = (Button) findViewById(R.id.DownBtn);
		downBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				//when button clicked.
				update(-1);
			}
		});
	}
/**
 * when user inputs up button it carry on variable 1 so it count up.
 * when user inputs down button it carry on variable -1 so it can count down.
 * @param n
 */
	private void update(int n) {
		try{
		if (n == 1)
			value++;
		else
			value--;
		System.out.println(value);
		text.setText(Integer.toString(value));		//set text.
		}catch(RuntimeException e){
			;
		}
	}

}
