package com.example.lab14_thread;

import android.util.Log;

public class MyRunnableClass implements Runnable {
	@Override
	public void run() {
		try {
			for (int i = 100; i < 105; i++) {
				Thread.sleep(1000);
				Log.e("<<runnable>>", "runnable talking: " + i);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}// run }//class
}