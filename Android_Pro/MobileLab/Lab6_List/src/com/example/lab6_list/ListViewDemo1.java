package com.example.lab6_list;

//import android.R;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListViewDemo1 extends ListActivity {

	String[] items = { "data1", "data2", "data3", "data4", "data5", "data6",
			"data7" };
	TextView txtMsg;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/**
		LayoutInflater inflater = null;
		ViewGroup container = null;
		*/
		//View rootView = inflater.inflate(R.layout.activity_main, container,false);
		// setContentView(R.layout.activity_main);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, items));
		txtMsg = (TextView)findViewById(R.id.txtMsg);
	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		try {
			String text = "Position : " + position + "     " + items[position];
			txtMsg.setText(text);
		} catch (Exception e) {
			Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
		}
	}

}
