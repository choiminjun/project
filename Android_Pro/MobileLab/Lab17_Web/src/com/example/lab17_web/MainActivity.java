package com.example.lab17_web;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends ActionBarActivity {
	WebView browser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		browser = (WebView) findViewById(R.id.webkit);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.setWebViewClient(new WebViewClient());
		// browser.loadUrl("http://www.eBay.com");
		showMyHomeMadeHtmlPage();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}// onCreateOptionsMenu

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.goback:
			browser.goBack();
			return true;
		case R.id.gofront:
			browser.goForward();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		/**
		 * String option = item.getTitle().toString();
		 * 
		 * if (option.equals("Forward Page")) browser.goForward(); if
		 * (option.equals("Back Page")) browser.goBack(); return true;
		 */
	}// onOptionsItemSelected

	private void showMyHomeMadeHtmlPage() {
		String aGoogleMapImage = "<img src=\"http://maps.googleapis.com/maps/api/"
				+ "staticmap?center=37.45115,127.12932&"
				+ "zoom=14&size=350x450&sensor=false\"> ";
		String myLocalHtmlPage = "<html> " + "<body> Hello, world! "
				+ "<br> Greetings from Gachon University" + aGoogleMapImage
				+ "</body> " + "</html>";
		browser.loadData(myLocalHtmlPage, "text/html", "UTF-8");
	}
}
