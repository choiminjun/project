package com.example.lab13_transition;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

public class TranstionActivity extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transtion);
		
		ImageButton button = (ImageButton)findViewById(R.id.button);
		TransitionDrawable drawable =
		            (TransitionDrawable) button.getDrawable();
		drawable.startTransition(10000);
		
		Resources res = getResources();
		Drawable shape = res. getDrawable(R.drawable.gradient_box);
		TextView tv = (TextView)findViewById(R.id.textview); 
		tv.setBackground(shape);

	}
}
