package com.example.lab14_suface;

import android.app.Activity;
import android.os.Bundle;
public class MySurfaceActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawingSurface(this));
    }
}
