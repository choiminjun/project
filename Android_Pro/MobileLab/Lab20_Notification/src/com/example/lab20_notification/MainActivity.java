package com.example.lab20_notification;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity implements OnClickListener {
	NotificationManager notificationManager;
	final int NOTIFICATION_ID = 12345;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.btnBig).setOnClickListener(this);
		findViewById(R.id.btnCancel).setOnClickListener(this);
	}// onCreate

	@SuppressLint("NewApi")
	public void createBigNotification(View view) {
		Intent intent = new Intent(this, NotificationReceiverActivity.class);
		intent.putExtra("callerIntent", "main");
		intent.putExtra("notificationID", NOTIFICATION_ID);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

		// better way to do previous work
		PendingIntent pIntent1 = makePendingIntent(
				NotificationReceiverActivity1.class, "Action1");
		PendingIntent pIntent2 = makePendingIntent(
				NotificationReceiverActivity2.class, "Action2");
		PendingIntent pIntent3 = makePendingIntent(
				NotificationReceiverActivity3.class, "Action3");
		// a bitmap to be added in the notification view
		Bitmap myBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.my_large_bitmap);
		Notification.Builder baseNotification = new Notification.Builder(this)
				.setContentTitle("TITLE goes here ...")
				.setContentText("Second Line of text goes here")
				.setTicker("Ticker tape1...Ticker tape2...")
				.addAction(R.drawable.icon1, "Action1", pIntent1)
				.addAction(R.drawable.icon2, "Action2", pIntent2)
				.addAction(R.drawable.icon3, "Action3", pIntent3)
				.setSmallIcon(R.drawable.icon0).setLargeIcon(myBitmap)
				.setLights(0xffcc00, 1000, 500).setContentIntent(pIntent);
		Notification noti = new Notification.InboxStyle(baseNotification)
				.addLine("Line-1").addLine("Line-2").addLine("Line-2")
				.setSummaryText("SUMMARY-Line-1 here").build();
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE); // Hide
																							// the
																							// notification
																							// after
																							// its
																							// selected
		noti.flags |= Notification.FLAG_AUTO_CANCEL;
		// notification ID is 12345
		notificationManager.notify(12345, noti);
	}// createBigNotification

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBig:
			createBigNotification(v);
			break;

		case R.id.btnCancel:
			try {
				if (notificationManager != null) {
					notificationManager.cancel(NOTIFICATION_ID);
				}
			} catch (Exception e) {
				Log.e("<<MAIN>>", e.getMessage());
			}
			break;
		}
	}// onClick

	public PendingIntent makePendingIntent(Class partnerClass, String callerName) {
		Intent intent = new Intent(this, partnerClass);
		intent.putExtra("callerIntent", callerName);
		intent.putExtra("notificationID", NOTIFICATION_ID);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
		return pIntent;
	}
}// class