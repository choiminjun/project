package com.example.lab11_prefex2;

import java.util.Date;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class Preference1 extends Activity {

	public static final String MYPREFS = "MySharedPreferences001";
	String custName = "n.a.";
	int custAge = 0;
	float custCredit = 0;
	long custNumber = 0;
	String custDateLastCall;
	TextView captionBox;
	EditText txtPref;
	final int mode = Activity.MODE_PRIVATE;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txtPref = (EditText) findViewById(R.id.txtPref);
		captionBox = (TextView) findViewById(R.id.captionBox);
		captionBox.setText("SharedPreference Container: \n\n"
				+ "we are working on customer Macarena \n"
				+ "fake an interruption, press 'Back Button' \n"
				+ "re-execute the application.");

		int mode = Activity.MODE_PRIVATE;
		SharedPreferences mySharedPreferences = getSharedPreferences(MYPREFS,
				mode);

		if (mySharedPreferences != null
				&& mySharedPreferences.contains("custName")) {
			showSavedPreferences();
		}
	}

	public void onPause() {
		savePreferences();
		super.onPause();
	}

	protected void savePreferences() {
		SharedPreferences mySharedPreferences = getSharedPreferences(MYPREFS,
				mode);
		SharedPreferences.Editor myEditor = mySharedPreferences.edit();

		myEditor.putString("custName", "Maria Macarena");
		myEditor.putInt("custAge", 21);
		myEditor.putFloat("custCredit", 1500000.00F);
		myEditor.putLong("custNumber", 9876543210L);
		myEditor.putString("custDateLastCall", new Date().toLocaleString());
		myEditor.commit();
	}

	private void showSavedPreferences() {
		// TODO Auto-generated method stub
		SharedPreferences mySharedPreferences = getSharedPreferences(MYPREFS,
				mode);

		custName = mySharedPreferences.getString("custName", "defNameValue");
		custAge = mySharedPreferences.getInt("custAge", 18);
		custCredit = mySharedPreferences.getFloat("custCredit", 1000.00F);
		custNumber = mySharedPreferences.getLong("custNumber", 1L);
		custDateLastCall = mySharedPreferences.getString("custDateLastCall",
				new Date().toLocaleString());
		String msg = "name: " + custName + "\nAge: " + custAge + "\nCredit: "
				+ custCredit + "\nLastCall: " + custDateLastCall;
		txtPref.setText(msg);
	}
}
