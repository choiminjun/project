package com.example.lab092_0415;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ContextMenuTest extends Activity {

	Button mBtn;
	EditText mEdit;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mBtn = (Button) findViewById(R.id.button);
		mBtn.setOnCreateContextMenuListener(this);
		/**
		 * registerForContextMenu(mBtn);
		 */
		mEdit = (EditText) findViewById(R.id.edittext);
		registerForContextMenu(mEdit);
		mBtn.setOnCreateContextMenuListener(this); // ������..
	}

	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		// MenuInflater inflater = getMenuInflater();
		// inflater.inflate(R.menu.context_menu, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == mBtn.getId()) {
			menu.setHeaderTitle("Button Menu");
			menu.add(0, 1, 0, "Red");
			menu.add(0, 2, 0, "Green");
			menu.add(0, 3, 0, "Blue");
		}
		if (v == mEdit) {
			menu.add(0, 4, 0, "Translate");
			menu.add(0, 5, 0, "Recognition");
		}
	}

	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1:
			mBtn.setTextColor(Color.RED);
			return true;
		case 2:
			mBtn.setTextColor(Color.GREEN);
			return true;
		case 3:
			mBtn.setTextColor(Color.BLUE);
			return true;
		case 4:
			Toast.makeText(this, "Translation", Toast.LENGTH_SHORT).show();
			return true;
		case 5:
			Toast.makeText(this, "Recognition!", Toast.LENGTH_SHORT).show();
			return true;
		}
		return true;
	}
}
