package com.example.intent2;

import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

public abstract class Activity2 extends ActionBarActivity implements OnClickListener, android.view.View.OnClickListener  {
	Button btn;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);
		btn = (Button)findViewById(R.id.backBtn);
		btn.setOnClickListener(this);
	}
	public void onClick(View v){
		if(v.getId() == btn.getId()){
			finish();
		}
	}

}