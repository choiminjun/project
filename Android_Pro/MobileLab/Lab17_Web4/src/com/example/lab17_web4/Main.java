package com.example.lab17_web4;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Main extends Activity implements LocationListener {
	private WebView browser;
	LocationManager locationManager;
	MyLocater locater = new MyLocater();

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// cut location service requests
		locationManager.removeUpdates(this);
	}

	private void getLocation() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE); // use GPS device
														// //criteria.setAccuracy(Criteria.ACCURACY_COARSE);
														// // towers, wifi
		String provider = locationManager.getBestProvider(criteria, true);
		// In order to make sure the device is getting the location, request
		// updates [wakeup after changes of: 5 sec. or 10 meter]
		locationManager.requestLocationUpdates(provider, 5, 10, this);
		locater.setNewLocation(locationManager.getLastKnownLocation(provider));
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getLocation();
		setupBrowser();
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}// onCreate

	/** Set up the browser object and load the page's URL **/
	@SuppressLint("SetJavaScriptEnabled")
	private void setupBrowser() {
		final String centerMapURL = "javascript:centerAt("
				+ locater.getLatitude() + "," + locater.getLongitude() + ")";
		browser = (WebView) findViewById(R.id.webview);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.addJavascriptInterface(locater, "locater");
		browser.loadUrl("file:///android_asset/webview_map.html");

		browser.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				browser.loadUrl(centerMapURL);
			}
		});
	}

	@Override
	public void onLocationChanged(Location location) {
		String lat = String.valueOf(location.getLatitude());
		String lon = String.valueOf(location.getLongitude());
		Toast.makeText(getApplicationContext(), lat + "\n" + lon, 1).show();
		locater.setNewLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) { // needed by Interface.
														// Not used
	}

	@Override
	public void onProviderEnabled(String provider) { // needed by Interface. Not
														// used
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) { // needed
																				// by
																				// Interface.
																				// Not
																				// used
	}

	public class MyLocater {
		private Location mostRecentLocation;

		public void setNewLocation(Location newCoordinates) {
			mostRecentLocation = newCoordinates;
		}

		public double getLatitude() {
			if (mostRecentLocation == null)
				return (0);
			else
				return mostRecentLocation.getLatitude();
		}

		public double getLongitude() {
			if (mostRecentLocation == null)
				return (0);
			else
				return mostRecentLocation.getLongitude();
		}
	}// MyLocater
}