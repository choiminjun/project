package com.example.ex_intent;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener, android.view.View.OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btn = (Button)findViewById(R.id.Btn);
		btn.setOnClickListener(this);
	}

}
