package com.example.customview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

public class CustomView extends Activity{
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		MyView vw = new MyView(this);
		setContentView(vw);
	}
	protected class MyView extends View{
		public MyView(Context context){
			super(context);
		}
		public void onDraw(Canvas canvas){
			Paint pnt = new Paint();
			pnt.setColor(Color.BLUE);
			canvas.drawColor(Color.WHITE);
			canvas.drawCircle(100,199,80,pnt);
		}
	}
}