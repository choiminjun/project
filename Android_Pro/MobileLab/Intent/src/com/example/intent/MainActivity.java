package com.example.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements OnClickListener {
	Button btn2;

	Button btn1;
	Intent myIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn1 = (Button) findViewById(R.id.Btn1);
		btn2 = (Button) findViewById(R.id.Btn2);
		btn2.setOnClickListener(this);
		btn1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == btn2.getId())
			myIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://m.google.com"));
		startActivity(myIntent);
		if (v.getId() == btn1.getId()) {
			Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW,
					Uri.parse("content://contacts/people"));
			startActivity(myIntent);
		}
	}
}
