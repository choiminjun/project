package com.example.lab1_practice;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	EditText txtVal1;
	EditText txtVal2;
	TextView lblResult;
	Button btnAdd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txtVal1 = (EditText) findViewById(R.id.EditText01);
		txtVal2 = (EditText) findViewById(R.id.EditText02);
		lblResult = (TextView) findViewById(R.id.TextView01);
		btnAdd = (Button) findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// get values from the UI
				Double v1 = Double.parseDouble(txtVal1.getText().toString());
				Double v2 = Double.parseDouble(txtVal2.getText().toString());
				// create intent to call Activity2
				Intent myIntentA1A2 = new Intent(MainActivity.this,
						Activity2.class);
				// create a container to ship data
				Bundle myData = new Bundle();
				// add <key,value> data items to the container
				
				myData.putDouble("val1", v1);
				myData.putDouble("val2", v2);
				// attach the container to the intent
				myIntentA1A2.putExtras(myData);
				// call Activity2, tell your local listener to wait for response
				startActivityForResult(myIntentA1A2, 101);
			}
		});
	}// onCreate

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if ((requestCode == 101) && (resultCode == Activity.RESULT_OK)) {
				Bundle myResults = data.getExtras();
				Double vresult = myResults.getDouble("vresult");
				lblResult.setText("Sum is " + vresult);
			}
		} catch (Exception e) {
			lblResult.setText("Problems ‐ " + requestCode + " " + resultCode);
		}
	}// onActivityResult
	/**
	EditText txtval1,txtval2;
	TextView showResult;
	Button btn;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txtval1 = (EditText)findViewById(R.id.Editval1);
		txtval2 = (EditText)findViewById(R.id.Editval2);
		showResult= (TextView)findViewById(R.id.showResult);
		btn = (Button)findViewById(R.id.calculateBtn);
		
		Double v1 = Double.parseDouble(txtval1.getText().toString());
		Double v2 = Double.parseDouble(txtval2.getText().toString());
		

		Intent intent2 = new Intent(MainActivity.this,Activity2.class);
		Bundle mydata = new Bundle();
		
		mydata.putDouble("val1", v1);
		mydata.putDouble("val2", v2);
		
		intent2.putExtras(mydata);
		startActivityForResult(intent2,101);
	}
	
	protected void onActivityResult(int requestCode, int resultCode,Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		try{
			if( (requestCode == 101) && (resultCode == Activity.RESULT_OK) ){
				Bundle myResults = data.getExtras();
				Double vresult = myResults.getDouble("result");
				showResult.setText("Sum is " + vresult);
			}
		}catch(Exception e){
			showResult.setText("ERROR");
		}
	}
	*/
}
