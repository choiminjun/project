package com.example.lab1_practice2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	// public static final int REQUEST_CODE = 1001;
	EditText txt;
	TextView txtview;
	Button btn;
	Intent myintent;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		txt = (EditText) findViewById(R.id.DataReceived);
		txtview = (TextView) findViewById(R.id.TextView01);
		btn = (Button) findViewById(R.id.btnAdd);
		btn.setOnClickListener(this);

		// myintent = getIntent();

	}

	@Override
	public void onClick(View v) {

		myintent = new Intent(this, Activity2.class);
		startActivityForResult(myintent, 101);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ((requestCode == 101) && (resultCode == Activity.RESULT_OK)) {
			Bundle mydata = new Bundle();
			mydata = data.getExtras();
			
		}
	}

}
