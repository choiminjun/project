
public class ConcatTest{
	public static String concatWithString(){
		String t = "CAt";
		for(int i = 0 ; i < 10000; i++){
			t= t+"Dog";
		}
		return t;
	}
	public static String concatWithStringBuffer(){
		StringBuffer sb = new StringBuffer("Cat");
		for(int i = 0 ; i < 10000  ; i++){
			sb.append("Dog");
		}
		return sb.toString();
	}
	public static void main(String[] args){
		StringBuffer sb = new StringBuffer(); sb.append("Java StringBuffer");
		System.out.println("Output1 :"+sb);
		sb.append(" Append StringBuffer");
		System.out.println("Output2 :"+sb);
	/**
		long start = System.currentTimeMillis();
		concatWithString();
		System.out.println("Concat string : " + (System.currentTimeMillis() - start)+"ms");
		start = System.currentTimeMillis();
		concatWithStringBuffer();

		System.out.println("Concat stringBuffer : " + (System.currentTimeMillis() - start)+"ms");
	*/
	}
}