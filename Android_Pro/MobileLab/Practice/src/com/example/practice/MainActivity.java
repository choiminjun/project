package com.example.practice;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	TextView captionBox;
	EditText txtPref;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SharedPreferences sh_Pref = getSharedPreferences("FILE", 0);
		if (sh_Pref != null && sh_Pref.contains("name")) {
			showState();
		}

	}

	private void showState() {
		// TODO Auto-generated method stub
		txtPref = (EditText)findViewById(R.id.txtPref);
		SharedPreferences sh_Pref = getSharedPreferences("FILE", 0);
		String str = sh_Pref.getString("name", "defValue") + sh_Pref.getString("age", "defValue");
		txtPref.setText(str);
	}

	public void onPause() {
		super.onPause();
		savedState();// call method before app killed.
	}

	private void savedState() {
		// TODO Auto-generated method stub
		SharedPreferences sh_Pref = getSharedPreferences("FILE", 0);
		SharedPreferences.Editor editor = sh_Pref.edit();
		editor.putString("name", "minjun");
		editor.putString("age", "25");
		/**
		 * This is really important!!.
		 */
		editor.commit();
	}
}
