package com.example.quiz0410choi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MinjunsActivity extends Activity implements OnClickListener {
	RadioGroup group01;
	RadioButton btn01, btn02, btn03;
	TextView txt01;
	EditText edittxt01;
	Button btn1;

	/**
	 * RadioButton에는 setOnClickListener를 두면 안된다 왜냐하면 그것들은 클릭할때 무슨 일이 일어나야 하는 것이
	 * 아니고 다른 Button 애트리뷰트가 클릭되었을시에 발생하는 사건이기 때문에 setOnclickListener를 두지 않아야 한다.
	 */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.minjun);
		btn01 = (RadioButton) findViewById(R.id.radio0);
		// btn01.setOnClickListener(this);

		btn02 = (RadioButton) findViewById(R.id.radio1);
		// btn02.setOnClickListener(this);

		btn03 = (RadioButton) findViewById(R.id.radio2);
		// btn03.setOnCheckedChangeListener(this);

		txt01 = (TextView) findViewById(R.id.textView1);
		edittxt01 = (EditText) findViewById(R.id.editText1);

		btn1 = (Button) findViewById(R.id.button001);
		btn1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		try {
			String str = null;
			String str2 = null;
			if (btn01.isChecked())
				str = btn01.getText().toString();
			if (btn02.isChecked())
				str = btn02.getText().toString();
			if (btn03.isChecked())
				str = btn03.getText().toString();

			str2 = edittxt01.getText().toString();

			Intent myintent = new Intent();
			Bundle mydata = new Bundle();
			mydata.putString("tel", str);
			mydata.putString("number", str2);
			myintent.putExtras(mydata);
			setResult(Activity.RESULT_OK, myintent);
			finish();

		} catch (Exception e) {
			;
		}

	}
}
