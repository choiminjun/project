package com.example.lab17_web2;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class JavaScriptInterface {
	Context mContext;

	/** Instantiate the interface and set the context */
	JavaScriptInterface(Context c) {
		mContext = c;
	}

	/** Show a toast from the web page */
	@JavascriptInterface
	public void showToast(String toastMsg) {
		Toast.makeText(mContext, toastMsg, Toast.LENGTH_SHORT).show();
	}
}