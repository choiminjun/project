package com.example.lab17_web2;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebView1B extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view1_b);
		WebView browser = (WebView) findViewById(R.id.webView1);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.addJavascriptInterface(new JavaScriptInterface(this),"Android");
		// if the html file is in the app's memory space use:
		browser.loadUrl("file:///android_asset/my_local_webpage1.html");
		// if the file is in the app's SD card use:
		browser.loadUrl("file:///sdcard/my_local_webpage1.html");
		// CAUTION: Manifest must include
		// <uses-permission android:name="android.permission.INTERNET"/>
		// <uses-permission
		// android:name="android.permission.READ_EXTERNAL_STORAGE"/>
	}// onCreate
}// class



