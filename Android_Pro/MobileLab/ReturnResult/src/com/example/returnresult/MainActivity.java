package com.example.returnresult;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	TextView label1;
	EditText text1;
	Button btnCallActivity2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_main);
			label1 = (TextView) findViewById(R.id.label1);
			text1 = (EditText) findViewById(R.id.text1);
			btnCallActivity2 = (Button) findViewById(R.id.btnPickContact);
			btnCallActivity2.setOnClickListener(new ClickHandler());
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}// onCreate

	private class ClickHandler implements OnClickListener {
		@Override
		public void onClick(View v) {
			try {
				// myData refer to: content://contacts/people/
				String myData = text1.getText().toString();
				// you may also try ACTION_VIEW instead
				Intent myActivity2 = new Intent(Intent.ACTION_PICK,
						Uri.parse(myData)); // start myActivity2.
				// Tell it that our requestCodeID (or nickname) is 222
				 startActivityForResult(myActivity2, 222);
				// Toast.makeText(getApplicationContext(),
				// "I can't wait for you", 1).show();
			} catch (Exception e) {
				label1.setText(e.getMessage());
			}
			;
		}// onClick
	}// ClickHandler

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if ((requestCode == 0) && (resultCode == Activity.RESULT_OK)) {
			String selectedImage = intent.getDataString();
			Toast.makeText(this, selectedImage, 1).show();
			// show a 'nice' screen with the selected image
			Intent myAct3 = new Intent(Intent.ACTION_VIEW, Uri.parse(selectedImage)); startActivity(myAct3);
			      }
		
		try {
			// use requestCode to find out who is talking back to us
			switch (requestCode) {
			case (222): {
				// 222 is our friendly contact-picker activity
				if (resultCode == Activity.RESULT_OK) {
					String selectedContact = data.getDataString(); // it will
																	// return an
																	// URI that
																	// looks
																	// like:
					// content://contacts/people/n
					// where n is the selected contacts' ID
					 label1.setText(selectedContact.toString()); //show a
					// 'nice' screen with the selected contact Intent myAct3 =
					// new Intent (Intent.ACTION_VIEW,
					Intent myAct3 = new Intent(Intent.ACTION_VIEW,
							Uri.parse(selectedContact));
					startActivity(myAct3);
				} else {
					// user pressed the BACK button
					 label1.setText("Selection CANCELLED " + requestCode + " "
					 + resultCode);
				}
				break;
			}
			}// switch
		}

		catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}// onActivityResult
}// IntentDemo2

