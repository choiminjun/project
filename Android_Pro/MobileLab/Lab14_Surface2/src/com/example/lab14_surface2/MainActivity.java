package com.example.lab14_surface2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends Activity {
	private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

	private float initX, initY, radius;
	private boolean drawing = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); // setContentView(R.layout.main);
		MySurfaceView mySurfaceView = new MySurfaceView(this);
		setContentView(mySurfaceView);
	}

	public class MySurfaceThread extends Thread {
		private SurfaceHolder myThreadSurfaceHolder;
		private MySurfaceView myThreadSurfaceView;
		private boolean myThreadRun = false;

		public MySurfaceThread(SurfaceHolder surfaceHolder,
				MySurfaceView surfaceView) {
			myThreadSurfaceHolder = surfaceHolder;
			myThreadSurfaceView = surfaceView;
		}

		public void setRunning(boolean b) {
			myThreadRun = b;
		}

		public void run() {
			// TODO Auto-generated method stub //super.run();
			while (myThreadRun) {
				Canvas c = null;
				try {
					c = myThreadSurfaceHolder.lockCanvas(null);
					synchronized (myThreadSurfaceHolder) {
						myThreadSurfaceView.draw(c);
					}
				} finally {
					if (c != null) {
						myThreadSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}
	}

	public class MySurfaceView extends SurfaceView implements
			SurfaceHolder.Callback {
		private MySurfaceThread thread;

		public MySurfaceView(Context context) {
			super(context);
			init();
		}

		public MySurfaceView(Context context, AttributeSet attrs) {
			super(context, attrs);
			init();
		}

		public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
			init();
		}

		private void init() {
			getHolder().addCallback(this);
			thread = new MySurfaceThread(getHolder(), this);
			setFocusable(true); // make sure we get key events
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(3);
			paint.setColor(Color.WHITE);
		}

		@Override
		public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
				int arg3) {
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			thread.setRunning(true);
			thread.start();
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			boolean retry = true;
			thread.setRunning(false);
			while (retry) {
				try {
					thread.join();
					retry = false;
				} catch (InterruptedException e) {
				}
			}
		}

		public void draw(Canvas canvas) {
			if (drawing) {
				canvas.drawCircle(initX, initY, radius, paint);
			}
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) { // TODO Auto-generated
															// method stub
															// //return
															// super.onTouchEvent(event);
			int action = event.getAction();
			if (action == MotionEvent.ACTION_MOVE) {
				float x = event.getX();
				float y = event.getY();
				radius = (float) Math.sqrt(Math.pow(x - initX, 2)
						+ Math.pow(y - initY, 2));
			} else if (action == MotionEvent.ACTION_DOWN) {
				initX = event.getX();
				initY = event.getY();
				radius = 1;
				drawing = true;
			} else if (action == MotionEvent.ACTION_UP) {
				drawing = false;
			}
			return true;
		}
	}
}