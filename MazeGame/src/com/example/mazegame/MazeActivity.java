/**
 Program Maze Game.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 HW 4- 2.
 Last Changed : 14/05/2014.
 */

/**
 * Make success mazeboard Randomically by using MakeMazeBoard Class,
 * and setView by using MazeView Class.
 */
package com.example.mazegame;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

public class MazeActivity extends Activity {
	private static final int maze_dimension = 9; // program can set ���Ƿ�, but
													// it's hard to set location
													// for now.
	public static boolean makemazeboardclass_check = false;
	ActionBar actionbar = null;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		actionbar = getActionBar();
		actionbar.hide();
		int[][] maze_board = new int[maze_dimension][maze_dimension];
		while (true) {
			//Make object of MakeMazeBoard and sets by using constructor.
			MakeMazeBoard maze = new MakeMazeBoard(maze_dimension,
					maze_dimension);
			maze.dfs(0, 0);
			/**
			 * This is important.
			 * makemazeboardclass_check == true means MakeMazeBoard class
			 * has finally made success mazeboard so it doesn't need to make
			 * mazeboard randomically any more.
			 */
			if (makemazeboardclass_check == true) {
				maze_board = maze.getmaze();
				break;
			}
		}
		//set View.
		setContentView(new MazeView(this, maze_board, maze_dimension));
	}
}
