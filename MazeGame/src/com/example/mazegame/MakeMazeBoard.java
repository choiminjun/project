/**
 * Make success mazeboard randomically.
 */
package com.example.mazegame;

public class MakeMazeBoard {
	int maze[][]; // maze with walls that block
	int check_visited[][]; // memorization to avoid duplicates
	int dest_x;
	int dest_y;

	/**
	 * Constructor which calls init method.
	 */
	public MakeMazeBoard(int row, int col) {
		maze = new int[row][col];
		check_visited = new int[row][col];
		this.init(row, col); // initialize maze
	}

	/**
	 * initialize variable
	 */
	private void init(int row, int col) {
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				maze[i][j] = check_visited[i][j] = 0;
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				if ((int) (Math.random() * 4) % 3 != 0)
					maze[i][j] = 1;
		dest_x = row - 1;
		dest_y = col - 1;
		maze[dest_x][dest_y] = 0;
		maze[0][0] = 0;
	}

	/**
	 * Find right maze by using Depth first search method.
	 */
	public boolean dfs(int row, int col) {
		if ((row < 0) || (col < 0) || row > dest_x || col > dest_y)
			return false;
		if (maze[row][col] == 1 || check_visited[row][col] != 0)
			return false; // failure
		System.out.println(" Visit:(" + row + "," + col + ")"); // logcat
																// testing..
		if (row == dest_x && col == dest_y) { // goal coordinates found
			/**
			 * This is important!! MazeActivity 클래스의 makemazeboardclass_check 라는
			 * static 변수를 이용함으로써 올바른 maze를 찾았을 시에 MazeActivity의 while loop를 빠져
			 * 나간다. 만약 boolean return을 사용할시에, while문에서 빠져나가지 못하는 현상이 발생하여 static
			 * 변수를 이용하였다.
			 */
			MazeActivity.makemazeboardclass_check = true;
			System.out.println("dest find success");
			return true;
		}
		check_visited[row][col] = 1; // memorize, not to be check_visited
										// again

		try {
			dfs(row, col + 1);
			dfs(row, col - 1);
			dfs(row + 1, col);
			dfs(row - 1, col);
		} catch (Exception ArrayIndexOfBoundsException) {
			;// for index out of array.
		}
		return false;
	}

	/**
	 * return maze.
	 * 
	 * @return
	 */
	public int[][] getmaze() {
		return maze;
	}

}
