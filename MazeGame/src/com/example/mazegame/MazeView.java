package com.example.mazegame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class MazeView extends View {
	// get display.
	Display display = ((WindowManager) this.getContext().getSystemService(
			Context.WINDOW_SERVICE)).getDefaultDisplay();
	private int boxWidth = display.getWidth();
	private int boxHeight = display.getHeight() + 150; // I don't know why 240
														// is used,but this must
														// be 240 otherwise,
														// some blocks are hided
														// from screen.
	private Bitmap wall = BitmapFactory.decodeResource(getResources(),
			R.drawable.wall);
	private Bitmap path = BitmapFactory.decodeResource(getResources(),
			R.drawable.path);
	private Bitmap start = BitmapFactory.decodeResource(getResources(),
			R.drawable.start);
	private Bitmap end = BitmapFactory.decodeResource(getResources(),
			R.drawable.end);

	private Rect[][] board_maze;
	private int[][] board_check;
	private int dimension;

	private Path mPath;
	private Paint mPaint;
	Point mPoint;

	private boolean isChecked = false; // for checking wheather fingers detached
										// from display or not.
	private boolean dialboxChecked = false;	//by using this, it prevent from displaying dial box duplicately.
	
	/**
	 * Constructor setting and call init method.
	 */
	public MazeView(Context context, int[][] random_maze, int temp_dimension) {
		super(context);
		dimension = temp_dimension;
		board_maze = new Rect[dimension][dimension];
		board_check = new int[dimension][dimension];
		this.init(random_maze);
	}

	private void init(int[][] random_maze) {
		for (int row = 0; row < dimension; row++) {
			for (int col = 0; col < dimension; col++) {
				board_maze[row][col] = new Rect(); // make Rect for Bitmaps.
				board_check[row][col] = random_maze[row][col];
			}
		}
		wall = Bitmap.createScaledBitmap(wall, boxWidth / 9, boxHeight / 15,
				true);
		path = Bitmap.createScaledBitmap(path, boxWidth / 9, boxHeight / 15,
				true);
		start = Bitmap.createScaledBitmap(start, boxWidth / 9, boxHeight / 15,
				true);
		end = Bitmap
				.createScaledBitmap(end, boxWidth / 9, boxHeight / 15, true);

		// drawing initializing
		mPath = new Path();
		mPaint = new Paint();
		mPaint.setDither(true);
		mPaint.setColor(0xFFFF00FF);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
		mPoint = new Point();
	}

	public void onDraw(Canvas canvas) {
		Paint Title = new Paint();
		Title.setColor(Color.BLACK);
		Title.setTextSize(boxWidth / 13);
		canvas.drawColor(Color.WHITE);
		canvas.drawText("Minjun's Maze", boxWidth / 60, boxHeight / 40, Title);
		// draw maze.
		for (int row = 0; row < dimension; row++) {
			for (int col = 0; col < dimension; col++) {

				if (row == 0 && col == 0) { // draw start
					canvas.drawBitmap(start, boxWidth / 9 * row, boxHeight / 15
							* (col + 1), null);
				} else if (row == dimension - 1 && col == dimension - 1) { // draw
																			// end.
					canvas.drawBitmap(end, boxWidth / 9 * row, boxHeight / 15
							* (col + 1), null);
				} else if (board_check[row][col] == 0) {
					canvas.drawBitmap(path, boxWidth / 9 * row, boxHeight / 15
							* (col + 1), null);
				} else if (board_check[row][col] == 1) {
					canvas.drawBitmap(wall, boxWidth / 9 * row, boxHeight / 15
							* (col + 1), null);
				}
			}
		}
		canvas.drawPath(mPath, mPaint); // draw line following fingers.
	}

	public boolean onTouchEvent(MotionEvent event) {
		float eventX = event.getX();
		float eventY = event.getY();

		for (int row = 0; row < dimension; row++) {
			for (int col = 0; col < dimension; col++) {
				/**
				 * set location of board_maze (Rect) for reaction by using pos.
				 */
				board_maze[row][col].set(boxWidth / 9 * row, boxHeight / 15
						* (col + 1), boxWidth / 9 * row + wall.getWidth(),
						boxHeight / 15 * (col + 1) + wall.getHeight());

				
				/**
				 * When user attaches start Rect, it changes boolean value of
				 * isChecked to true, so user can play game.
				 */
				if (isChecked == false
						&& board_maze[0][0]
								.contains((int) eventX, (int) eventY)) {
					mPath.moveTo(eventX,eventY);
					isChecked = true;
					
					return true;
				}
				/**
				 * This prevents from user's fingers attach other Rect except
				 * start Rect when first push so when user initially pushes
				 * other rect, it doesn't happen any more.
				 */
				if (isChecked == false)
					return false;

				/**
				 * When Finger faces with wall.
				 */
				if ((board_check[row][col] == 1)
						&& board_maze[row][col].contains((int) eventX,
								(int) eventY)) {
					
					if(dialboxChecked == false){
						dialboxChecked = true;
						AlertDialog dialBox = createDialogBox();
						dialBox.show();
					}
					return true;
				}
				/**
				 * When Finger faces with Finish block.
				 */
				else if (board_maze[dimension - 1][dimension - 1].contains(
						(int) eventX, (int) eventY)) {
					
					if(dialboxChecked == false){
						dialboxChecked = true;
					AlertDialog dialBox = createDialogBox();
					dialBox.show();
					
					}
					return true;
				}
				/**
				 * When Finger faces with path.
				 */
				else {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						mPath.moveTo(eventX, eventY);
						break;
					case MotionEvent.ACTION_MOVE:
						mPath.quadTo(eventX, eventY, (mPoint.x + eventX) / 2,
								(mPoint.y + eventY) / 2);
						break;
					case MotionEvent.ACTION_UP:
						isChecked = false;
						mPath.reset();
						break;
					}
					if(isChecked == false){
						mPoint.x = 0;
						mPoint.y = 0;
					}
					else{
					// reset poits.
					mPoint.x = (int) eventX;
					mPoint.y = (int) eventY;
					}
					invalidate();
				}
			}
		}
		return true;
	}

	/**
	 * Dial Box.
	 * It shows dial box whether user wants to replay game or quit.
	 * @return
	 */
	private AlertDialog createDialogBox() {
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getContext())
		.setTitle("Maze Game")
		.setMessage("Do you want play maze more?")
		.setIcon(R.drawable.ic_launcher)
		.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,
							int whichButton) {
						mPath.reset();
						dialboxChecked = false; 
						invalidate();
					}
				})
		.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				System.exit(1);
			}
		}).create();
		return myQuittingDialogBox;

	}
}
