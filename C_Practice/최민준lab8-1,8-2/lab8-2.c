//�ֹ���(lab8-2)
//insert,delete,empty.

#include <stdio.h>
#include <stdlib.h>
struct NODE{
	int key;
	struct NODE *next;
};
struct NODE* front = NULL;
int empty(){
	if(front == NULL)
		return 0;
	else
		return 1;
}
struct NODE* DELETE(){
	struct NODE *del = front;
	if(empty() == 0)
		printf("QUEUE is EMPTY!\n");
	else{
		if(front ->next == NULL){	//if only one nodes have.
			printf("%d is DELETED \n",front->key);
			free(del);		//if program free, pointer's value is not null so 
			front = NULL;  //we have to initialize null for decide empty or not.
		}
		else{
			printf("%d is DELETED \n" , del->key);
			if(front ->next == NULL){
				del = NULL;
				free(del);
				del = NULL;
			}
			else{
				front = front ->next;
				free(del);
				del = NULL;
			}
		}
	}
	return front;
}
struct NODE* insert(struct NODE *rear,int new_key){
	struct NODE *node = NULL;
	if(( node = (struct NODE*)malloc(sizeof(struct NODE))) == NULL)
		printf("malloc failed\n"),exit(-1);
	node ->key = new_key;
	node ->next = NULL;
	if(front == NULL)		//when there are no nodes in queue, front need to point out new node.
		front = rear = node;
	else{
		rear ->next = node;	//current end position is rear so, new_nodes proper position is rear->next.
		rear = rear ->next; //get rear position
	}
	return rear;			//return end position.
}
void main(){
	struct NODE *rear;
	struct NODE *del;
	int menu , key;
	del = rear = NULL;
	menu = key = 0;
	while(1){
		printf("INSERT	INPUT : 1 \n");
		printf("DELETE  INPUT : 2 \n");
		scanf("%d",&menu);
		if(menu == 1){
			printf("put key : ");
			scanf("%d",&key);
			rear = insert(rear , key);		//each time when insert function calls, update rear position.
		}
		else if(menu == 2){
			DELETE();				//each time when delete function calls , update front position.
		}
		else 
			break;
	}
	while(front){			//print.
		printf("%d	",front->key);
		del = front;
		front = front ->next;
		free(del);
	}
}
