﻿//최민준
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
const char HEXTABLE[16] = {'0', '1', '2', '3' ,'4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
char* bin(int num){
	int i;
	int bin_mask = 0x800;	//for 1 to 1000 program only need this . //0100 0000 0000
	char *temp_bin = (char*)malloc(12);
	if(temp_bin == NULL)
		printf("malloc fail"),exit(-1);
	for(i = 0 ; i < 12 ; i++){
		if(bin_mask & num)
				temp_bin[i] = '1'; 
			else
				temp_bin[i] = '0';
			bin_mask >>= 1;				//shift right.
	}
	return temp_bin;
}

void hex(int num){
	int i,j,temp_count;	//temp_count is for 16진수는 2진수를 4자리로 끊어 읽는 대 사용.
	//int bin_length = strlen(bin(num));
	char *b = bin(num);
	char temp_hex[4] = {NULL,};
	for(i= 0 ; i<=12; i+=4){
		for(j = i; j < i + 4 ; j++){	
			temp_hex[i/4] = (temp_hex[i/4] << 1) | (b[j] - '0');
		}
		temp_hex[i/4] = HEXTABLE[temp_hex[i/4]];	//get from hexa table.
	}
	printf("HEX : %s \n",temp_hex);
}

void main (){
	int num = 1;
	while(num <= 1000){
		printf("DEC : %3d   ", num); 
		printf("BIN : %d   ", atoi(bin(num)));
		hex(num);
		num++;
	} 
}