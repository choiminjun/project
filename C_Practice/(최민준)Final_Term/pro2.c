//최민준

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 10		
struct POINT{
	int row;
	int col;
};
char** make_table(char **table , int N){
	int i,j;
	table = (char**)malloc(sizeof(char*) * N);
	for(i = 0 ; i < N ; i++)
		table[i] = (char*)malloc(sizeof(char) * N);
	if(table == NULL)
		printf("malloc failed\n"),exit(-1);
	for(i = 0 ; i < N ; i++ )			//initializing table.
		for(j = 0 ; j < N ; j++)
			table[i][j] = NULL;
	return table;
}
int decide_way(int row,int col,int N){	// which wat to right or down? , but when row or col is N-1, the situation is traversed.
	if( (row == 0 || col == 0) && ( row != N-1 && col != N-1) ){	
		if( (row == 0 && col != 0) || (row == 0 && col == 0)  )	//this means 가로 벽. in starting, it goes left so it is same as this mechanism.
			return 1;
		else if(row != 0 && col == 0)
			return -1;				//this means 세로 벽.
	}
	else if(row == N - 1 || col == N - 1){	//situation is traversed.
		if(col == N - 1 )
			return -2;
		else if(row == N-1)
			return 2;
	}
	else
		return 0;
}
char **get_table(int row , int col , char **table , char c){	//assign char in table[row][col]
	table[row][col] = c;
	return table;
}
char **zig_zag(char **table , int N){
	struct POINT current_p;
	char str[100] = {NULL,};
	int str_len, str_count, i , j , check;
	int remember_direction;
	str_len = str_count = i = j = check = remember_direction = 0;
	printf("PUT STRING : ");
	scanf("%s",str),fflush(stdin);
	str_len = strlen(str);			//this is useful because if string length is bigger than table SIZE,program doesn't need use while loop.
	current_p.row = 0 , current_p.col = 0;
	
		while(1){
			
			
			table = get_table(current_p.row, current_p.col , table , str[str_count++]);	
			if(str_count == str_len) str_count = 0;
			check = decide_way(current_p.row , current_p.col , N);
			if(check == 1){		//when position is 가로 wall.
				table = get_table(current_p.row , ++current_p.col , table , str[str_count++]);	//assign table right.
				if(str_count == str_len) str_count = 0;
				current_p.row++ , current_p.col--;
				remember_direction = check;
			}
			else if(check == -1){	//when position is 세로 wall.
				table = get_table(++current_p.row , current_p.col , table , str[str_count++]);
				if(str_count == str_len) str_count = 0;
				current_p.row-- , current_p.col++;
				remember_direction = check;
			}
			else if(check == 2){	//check 2 or -2 case is ending point, so i had assigned break command here.
				table = get_table(current_p.row , ++current_p.col , table , str[str_count++]);
				if(str_count == str_len) str_count = 0;
				if(current_p.row == N - 1 && current_p.col == N -1)
				break;
				current_p.row-- , current_p.col++;
				remember_direction = check;
			}
			else if(check == -2){	
				table = get_table(++current_p.row , current_p.col , table , str[str_count++]);
				if(str_count == str_len) str_count = 0;
				if(current_p.row == N - 1 && current_p.col == N -1)
				break;
				current_p.row++ , current_p.col--;
				remember_direction = check;
			}
			else{
				if(remember_direction == 1 || remember_direction == -2)
					current_p.row++ , current_p.col--;				
				else if(remember_direction == -1 || remember_direction == 2)
					current_p.row-- , current_p.col++;				
			}

			
		}
	
	return table;
}
void print(char **table , int N){		//print table.
	int i,j;
	for(i = 0 ; i < N ; i++){
		for(j = 0 ; j < N ; j++){
			printf("%c ",table[i][j]);
		}
		printf("\n");
	}
}
void main(){
	char **table = NULL;
	int N;
	do{				//in this program MAX case is 10. it is acceptable to do much big case, but then, program needs
		//many memory space for tempar variables like string,start_point....
		printf("PUT SIZE : ");
		scanf("%d",&N);
	}while(N >= MAX);
	table = make_table(table , N);		//allocate memory.
	table = zig_zag(table , N);			//this makes table which is based on zigzag mechanism.
	print(table , N);
}
