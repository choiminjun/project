#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Search(FILE *fp ,FILE *rp, char *src_key , char *replace_key){
	int line_char_position[100][2] = {0,};
	int horizontal , match_count , src_index ;	
	int temp_line_p , temp_horizontal_p , line_char_count;
	int temp , count , i;
	int src_length = strlen(src_key);
	char temp_c[20] = {NULL,};		//for saving char to write replace file when search failed.
	int temp_c_count = 0;
	char c = NULL;
	int check = 0;
	horizontal = match_count = src_index = count = temp_line_p = temp_horizontal_p = line_char_count = 0;
	while( (c = fgetc(fp))!=EOF){
		check = 0;		//check 0 means not tried to search , 1 means serch success , 2 means search tried but failed.
		if(c == src_key[0]){
			check = 2;
			//temp = temp_char_p;
			while(1){
				if(c == src_key[src_index])
					src_index++,count++;
				else		//if it doesn't match anymore , esape while loop.
					break;	
				if(strlen(src_key) == count){
					line_char_position[ line_char_count ][0] = temp_line_p;
					line_char_position[ line_char_count++][1] = temp_horizontal_p;
					temp_horizontal_p += strlen(src_key);
					match_count++;
					check = 1;
					break;
				}
				temp_c[temp_c_count++] = c;
				c = fgetc(fp);
			}
		}
		if(check == 0)			//when doesn't tried search so just put c to replace file.
			fputc(c , rp);
		else if(check == 1){	//when search successed  so write replace key in replace file instead of search key.
			for(i = 0; i < strlen(replace_key) ; i++)
				fputc(replace_key[i] , rp);
		}
		else if(check == 2){	/**when tried search, but search failed. so for this case, program keeped temp_c array 
								which keeps to start trying search , end of search failed. so program can write this to replace file instead
								using fseek!
								**/
			for(i = 0 ; i < temp_c_count; i++)
				fputc(temp_c[i] , rp);
			fputc(c, rp);
		}
		if(c ==10){	//when pointer meets end of line. 파일끝이면 '\0' 이라고하면 먹혀야 하는데 
					//먹히지 않아 디버깅결과 아스키값이 10이라하여 이렇게 하게되었습니다.
			temp_horizontal_p = 0;//update position to zero.
			temp_line_p++;
		}
		else{
			temp_horizontal_p++;
		}
		src_index = count = temp_c_count  = 0;
	}
	for(i = 0 ; i < line_char_count ; i++)	//start position is (1,1). print line and horizontal.
		printf("%d ST Line : %d  Horizon : %d \n",i+1 , line_char_position[i][0]+1 , line_char_position[i][1]+1); 
	return match_count;
}
int alphabetic_number(FILE *f){
	char c;
	int count = 0;
	while( (c = fgetc(f)) != EOF)
		if( (c >= 'a' && c <= 'z') || ( c >='A' && c <= 'Z') )
			count++;
	return count;
}
void main(){
	FILE *fp = NULL;
	FILE *rp_file = NULL;
	int match;
	char search_key[20] = {NULL,};
	char replace_key[20] = {NULL,};
	fp = fopen("original.txt","r");
	rp_file = fopen("replaced.txt","w");
	if( (fp == NULL) || (rp_file == NULL ) )
		printf("file open error\n"),exit(-1);
	printf("Put search key : ");
	scanf("%s",search_key) , fflush(stdin);
	printf("Put replace key : ");
	scanf("%s",replace_key), fflush(stdin);

	match = Search(fp, rp_file , search_key, replace_key);	//call function.
	
	printf("\n\n<MATCH> : %d \n", match);
	fclose(fp);
	fclose(rp_file);
	fp = fopen("original.txt","r");			//reopen file.
	rp_file = fopen("replaced.txt","r");
	printf("\n\n\n<<ALPHABETIC NUMBER>>\n"); 
	printf("ORIGINAL : %d		REPLACED : %d \n",alphabetic_number(fp),alphabetic_number(rp_file));	//print alphabetic
	fclose(fp);
	fclose(rp_file);
	
}