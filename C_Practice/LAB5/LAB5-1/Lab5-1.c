//최민준
//공백 , 특수문자에 적용하지 않고 대소문자 구별
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void main(){
	FILE *fp = NULL;
	FILE *fp1 = NULL;
	int position , temp;
	char c;
	int i;
	if( (fp = fopen("original.txt","r") ) == NULL)
		printf("file open error") , exit(-1);
	if( (fp1 = fopen("cypher.txt","w") ) == NULL)
		printf("file open error") , exit(-1);
	printf("Put change position : ");
	scanf("%d",&position);
	while( (c = fgetc(fp)) != EOF ){	//cyphering text.			
		if(c >= 'a' && c <= 'z') 
			c = ( ( (c + position) - 'a') % 26 ) + 'a' ;
		else if(c >='A' && c <= 'Z'){
			c = ( ( (c + position) - 'A') % 26 ) + 'A';
		}
		fputc(c,fp1);	
	}
	fclose(fp) , fclose(fp1);
	if( (fp = fopen("cypher.txt","r") ) == NULL)	//decrypting text.
		printf("file open error") , exit(-1);
	if( (fp1 = fopen("decrypted.txt","w") ) == NULL)
		printf("file open error") , exit(-1);
	while( (c = fgetc(fp)) != EOF ){			
		if(c >= 'a' && c <= 'z'){ 
			if(c - position < 'a')
				c = ( 'z' - ( position - (c - 'a' + 1) ) ) ; //access from tail
			else
				c = ( ( (c - position) - 'a') % 26 ) + 'a' ;
		}
		else if(c >='A' && c <= 'Z'){
			if(c - position < 'A')
				c = ( 'Z' - ( position - (c - 'A' +1 ) ) );
			else
				c = ( ( (c - position) - 'A') % 26 ) + 'A';
		}
		fputc(c,fp1);	
	}	
    fclose(fp),fclose(fp1);
}



