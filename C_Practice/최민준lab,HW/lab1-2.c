
#include <stdio.h>
struct CLASS{
	char class_name[20];
	int class_credit;
	char class_grade;
}classes[10];
float GPA(struct CLASS[] , int n);
void main(){
	int num_of_class , i;
	float total_GPA = 0;
	int total_credit = 0;
	printf("How many class? : ");
	scanf("%d",&num_of_class);
	for(i = 0 ; i < num_of_class ; i++){
		printf("%d ST Class Name : ", i+1);
		scanf("%s",classes[i].class_name);
		printf("%d ST Class credit : ", i + 1);
		scanf("%d",&classes[i].class_credit);
		fflush(stdin);		//for esare buffer.
		printf("%d ST Class grade : " , i + 1);
		scanf("%c",&classes[i].class_grade);
	}
	for(i = 0 ; i < num_of_class; i++)
		total_credit += classes[i].class_credit;
	total_GPA = GPA(classes , num_of_class) / (float)total_credit;
	printf("GPA : %f \n",total_GPA);
}
float GPA(struct CLASS a[],int n){		//struct CLASS a[]로 구조체를 넘겨 받고 int n 으로 수업의 개수를 넘겨받음.
	int i;
	float GPA = 0;
	for( i = 0 ; i < n ; i++){
		if(a[i].class_grade == 'A')
			GPA += 4.0 * a[i].class_credit;
		else if(a[i].class_grade == 'B')
			GPA += 3.0 * a[i].class_credit;
		else if(a[i].class_grade == 'C')
			GPA += 2.0 * a[i].class_credit;
		else if(a[i].class_grade == 'D')
			GPA += 1.0 * a[i].class_credit;
		else
			GPA += 0.0 * a[i].class_credit;
	}
	return GPA;
}
