#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void binary(int num){
	char *bin;
	int mask = 0x800;
	int count,i,temp;
	count = i = temp = 0;
	temp = num;
	while(temp > 1)
		count++ , temp /= 2;
	bin = (char*)malloc(sizeof(char) * (count+1));
	while(mask){
		if( (mask & num)!=0)
			bin[i] = '1';
		else
			bin[i] = '0';
		mask >>= 1;
		i++;
	}
	printf("BIN : %d \n" , atoi(bin));
	
}
void hex(int num){
	char str[] = {"0123456789ABCDEF"};
	char *hex = NULL;
	int mask = 0x800;
	int count = 0,i = 0;
	int x[4] = {0,};
	int result = 0;
	int hex_count = 0;
	int temp;
	int check = 0;
	temp = num;
	while(temp > 1)
		count++ , temp = temp/16;
	hex = (char *)malloc(sizeof(char) * (count+1));
	while(mask){
		for(i=3 ; i >= 0 ; i--){v 
			if(num & mask)
				x[i] = 1;
			else
				x[i] = 0;
			mask >>=1;	
		}
		result = (x[0]*1) + (x[1]*2) +(x[2]*4) +(x[3]*8);
		if(result == 0 &&check == 1)
			hex[hex_count++] = str[result];
		else if(result!=0){
			hex[hex_count++] = str[result];
			check = 1;
		}
	}
	printf("HEX : ");
	for( i = 0; i < hex_count ; i++)
		printf("%c",hex[i]);
	printf("\n");
}
void main(){
	int num = 0;
	while(num <=16){
		binary(num);
		hex(num);
		num++;
		break;
	}
}
