//RECUSION DIAGRAM.
//inorder의 recursion 다이어그램을 그리시오
//binary search 깊게 알기..
/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int bottom , top;
float stack[20];
float pop(){
	float temp = stack[--top];
	stack[top] = 0;
	printf("%f hav popped\n",temp);
	return temp;
}
void push(float key){
	stack[top++] = key;
	printf("%f have inserted\n",stack[top - 1]);
	
}
void main(){
	int i;
	char example[20] = {NULL,};
	float temp1 ,temp2;
	int result = 0;
	bottom = top = temp1 = temp2 =  0;
	printf("put operation\n");
	scanf("%s",example);
	
	for(i = 0 ; i < strlen(example) ; i++){
		if(example[i] >= '0' && example[i] <='9'){
			push( (example[i] - '0'));
		}
		else if(example[i] == '+'){  //if add
			printf("add operation\n");
			push(pop() + pop());
		}
		else if(example[i] == '*'){
			printf("multiply\n");
			push(pop() * pop() );
		}
		else if(example[i] == '-'){
			printf("minus\n");
			push(-pop() + pop() );
		}
		else if(example[i] == '/'){
			temp1 = pop();
			temp2 = pop();
			printf("divide\n");
			push(temp1/temp2);
		}
	}

}
*/


//FIB recursion and iterative.
/*
#include <stdio.h>
int fib(int n){
	if(n ==1 || n ==2)
		return 1;
	else
		return fib(n-1) + fib(n-2);
}

void main(){
	int n;
	int result;
	printf("put n \n");
	scanf("%d",&n);
	result = fib(n);
	printf("RESULT :  %d\n",result);
}
*/


//iterative.
/*
#include <stdio.h>
int fib(int n){
	int i= 0;
	int total = 0;
	int prev = 0;
	for( i = 0 ; i < n ; i++){
		if(i == 0){
			total = 1;
			prev = 1;
		}else if(i == 1){
			total = 1;
			prev = 2;
		}
		else{
			 total += total + prev;
			 prev = total;
		}	
	}	
}

int fibo(int n){			//교수님 로직.
	int a =1 , b = 1 , c;
	int i;
	if(n == 1 || n ==2)
		return 1;
	for(i = 0 ; i < n - 2; i++){
		c = a + b;
		a = b;
		b = c;
	}
	return c;
}
void main(){
	int n;
	int result;
	printf("put n \n");
	scanf("%d",&n);
	result = fib(n);
	printf("RESULT :  %d\n",result);
}
*/


//practicing

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main(){
	char test[10] = {'2','3','4','*','2','*','+'};
	char temp[5] = {0,};
	int i,top = -1;
	int stack[10] = {0,};
	for( i = 0 ; i < strlen(test) ; i ++){
		if(test[i] >'0' && test[i] <='9'){
			temp[0] = test[i];
			stack[++top] = atoi(temp);
		}
		else{	//when time to calculate operation.            
			if(test[i] == '*'){
				stack[top-1] = stack[top-1] * stack[top];
				stack[top] = 0;
				top--;
			}
			else if(test[i] == '+'){
				stack[top-1] = stack[top-1] + stack[top];
				stack[top] = 0;
				top--;
			}
		}
	}
	printf("RESULT : %d \n",stack[0]);
}
