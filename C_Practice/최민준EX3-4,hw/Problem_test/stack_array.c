//배열은 함수에 주소를 넘겨주기 때문에 함수에서 값을 변경한것이 메인함수에서도 적용된다.

#include <stdio.h>
int isEmpty(int front){
	if(front == -1)
		return 0;
	else
		return 1;
}
int Delete(int stack[] , int front , int back){	//this is for deleting.
	if(isEmpty(front) ==1){
		printf("%d had deleted\n",stack[back]);
		stack[back] = 0;
		back--;
		return back;
	}
	else
		printf("stack is empty\n");

}
int Insert(int stack[],int front , int back){
	int newkey;
	printf("put key \n");
	scanf("%d",&newkey);
	stack[++back] = newkey;				
	printf("%d had Inserted\n",stack[back]);
	return back;			//for updatng recent back position.
}
void main(){
	int front , back ,Case;
	int stack[20]={0,};
	front = back = Case = -1;
	while(Case != 0){
		printf("Insert : 1 , Delete : - 1 \n");
		scanf("%d",&Case);
		if(Case == 1){
			back = Insert(stack,front,back);
			front = 0;			//when insert had called, this means front can't be -1.
		}
		else if(Case == -1){
			back = Delete(stack,front,back);
			if(back == -1) front = -1;	//back == -1 means there is no number in stack, so front have to be -1 for using isEmpty function.
		}
		else
			break;
	}
	for(front = 0; front <= back ; front++)
		printf("%d \n",stack[front]);
}
