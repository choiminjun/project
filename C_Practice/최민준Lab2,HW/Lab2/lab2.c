/*�ֹ���(LAB2)
This program architecture
-> In for loop,each time loop allocates memory and 
by using ptr(head) which goes throug until end, they compare with next key.
If next key is bigger than current position, insert current position.
*/
#include <stdio.h>
#include <stdlib.h>
struct NUM{
	int key;
	struct NUM *next;
};
void main(){
	struct NUM* node = NULL;
	struct NUM* head = NULL;
	struct NUM* ptr = NULL;
	struct NUM* del = NULL; //for delete.
	struct NUM* temp = NULL;
	int i;
	int nums[10] = {17,39,11,9,42,12,15,8,13,41};
	for( i = 0 ; i < 10 ; i++){
		node = (struct NUM*)malloc(sizeof(struct NUM));
		if(node == NULL){
			printf("malloc failed");
			exit(0);
		}
		if(head == NULL){
			node ->key = nums[i];
			node ->next = NULL;
			head = node;
			//ptr = node;
			continue;
		}
		else{
			node ->key = nums[i];
			node ->next = NULL;
			//ptr = node;
			temp = ptr = head;
			while(ptr){
				if(node ->key < ptr ->key){
					if(ptr == head){	//this is case when new_node is going to head position.
						node ->next = head;
						head = node;
					}
					else{		
						temp ->next = node;
						node ->next = ptr;
					}
					break; //insert had done.
				}
				else if(ptr ->next == NULL){ //this is when node goes to tail.
					ptr ->next = node;
					break;  //insert had done.
				}
				temp = ptr;	//this is for next connection.
				ptr = ptr ->next; //for next comparision.
			}
		}
	}

	while(head){
		printf("%d	",head->key);
		del = head;			//save delete address.
		head = head ->next;
		free(del);			//after passing head point, delete.
	}

}