/*√÷πŒ¡ÿ
Implementation Insert_function by using Double pointer.
*/
#include <stdio.h>
#include <stdlib.h>
struct NODE{
	int key;
	struct NODE *next;
};
int InsertKey(int insert_key,struct NODE *head, struct NODE *ptr);
void print(struct NODE *head,int input_key,int result);
void main(){
	struct NODE *node = NULL;   //initializing.
	struct NODE *head = NULL;
	struct NODE *ptr = NULL;
	int result;                 //this is for judgment.
	node = (struct NODE*)malloc(sizeof(struct NODE) * 7);
	if(node == NULL){
		printf("malloc failed");
		exit(0);
	}
	node[0].key = 100 ,node[0].next = &node[1]; //connection of 3 nodes.
	node[1].key =250 ,node[1].next = &node[2];
	node[2].key = 457 ,node[2].next = NULL;
	head = node , ptr = node[2].next;
	print(head,150,InsertKey(150,&head,&node[3]));  //insert.
	print(head,300,InsertKey(300,&head,&node[4]));
	print(head,50,InsertKey(50,&head,&node[5]));
	print(head,500,InsertKey(500,&head,&node[6]));
}
//using double pointer.
int InsertKey(int insert_key,struct NODE **head, struct NODE *new_node){
	struct NODE *ptr = (*head); //beacause of double pointer initialize pointer of pointers address.
	struct NODE *prev = (*head);
	if(new_node == NULL){   //defensive coding.
		printf("there is no new_node , system fail");
		return -1;
		exit(0);
	}
	else{
		new_node ->key = insert_key;
		new_node ->next = NULL;
		while(ptr){
			if(new_node ->key < ptr ->key){
				if(ptr == (*head)){ //this is when new_node is going to head position.
					new_node ->next = (*head);
					(*head) = new_node;
				}
				else{		//insert in the middle of node.
					prev ->next = new_node;	//prev had remained in the end of while loop, so can be used for connection.
					new_node->next = ptr;
				}
				return 0;	//insertion success.
			}	
			else if(new_node ->key == ptr ->key)
				return -1;
			else if(ptr ->next == NULL){//this is when new_node is linkedto tail.
				ptr ->next = new_node;
				return 0;
			}
			prev = ptr;	//remember for next connection.
			ptr = ptr ->next;	//for next comparison.
		}
	}
}
void print(struct NODE *head,int input_key,int result){	//print wheather insertion success or not.
	if(result == 0)
		printf("\n<%d> Insert Success!\n",input_key);
	else{
		printf("\n<%d>  Insert Failed! Key already exist\n",input_key);
	}
	while(head){		//confirm wheather ascending order or not.
		printf("%d	",head ->key);
		head = head ->next;
	}	
}
