//200932224 최민준 


#include <stdio.h>
#include <stdlib.h>
#define MAX 10
#define Time_Quantom 10
#define FALSE   0
#define TRUE    1
typedef struct PROCESS Process;

struct PROCESS{
	int pid;
	int priority;
	int arrival_time;
	int burst_time;
	Process *next;
};

Process *head = NULL; 
Process *rear = NULL;
Process* Sorting(Process *);		//sorting function.
int index = 0 , count_time = 0 , process_runtime = 0;
int Running(Process *pc);		//Process Running Function.
int Insert(int index	, Process *pc	);
int Delete(int index	, Process *pc);

int main ( int argc, char *argv[5] )
{	
	FILE *fp;
	Process pc[MAX];
	Process *ptr = NULL;

	int i , j;
	i = 0;
	fp = fopen(argv[2] , "r");
	if(fp ==NULL){
		printf("Could not open the file\n");
		exit(0);
	}
	while(fscanf(fp ,"%d	%d	%d	%d",&pc[i].pid	,&pc[i].priority , &pc[i].arrival_time , &pc[i].burst_time) != EOF)
		i++;
	ptr = pc;				//assign structure address.
	ptr = Sorting(ptr);		//call sorting function
	printf("Scheduling : %s\n",argv[1]); printf("==========================================================\n");		//print first line.
	Running(pc);
}

Process* Sorting(Process *pc){		//list arrival time to ascending order.
	int i , j;
	for(i = 0 ; i < MAX ; i++)
		for(j = 0 ; j< MAX ; j++){	
			Process temp;
			if(pc[j].arrival_time	>	pc[j+1].arrival_time){
				temp = pc[j];
				pc[j] = pc[j+1];
				pc[j+1] = temp;
			}
		}
		return pc;
}
int Running( Process *pc)		//using ascending ordered arrival time, make funtion.
{
	int i = 0;
	int res;

	while(1)		//아직 잘모름.
	{
		while(count_time !=(pc[index].arrival_time) ){
			printf("<time %d> ----system is idle ----\n" , count_time);
			count_time++;								//printf("<time %d> ----system is idle ----\n" , count_time) 일단 취소
		}
		if(index>MAX-1)
			break;
		res =Insert( index++	, pc);		//when process x's arrival time and time is same , call Insert function.
		if(res==0)
			break;
	}
	return 0;
}


int Insert(int index	, Process *pc	){
	Process *ptr =NULL;
	ptr= (Process*)malloc(sizeof(Process));
	if(ptr == NULL){
		printf("malloc failed");
		exit(0);}
	ptr = &pc[(index++)];			//index++ is for to face nextindexs arrivaltime.
	if(head == NULL){
		head = ptr;
		rear = head;
		head ->next = NULL;
	}
	else{
		rear ->next = ptr;
		rear = rear ->next;
		rear ->next = NULL;
	}
	printf("<time %d> [new arrival] process %d\n" , count_time , rear ->pid);
	if(head != rear)			//process is running , so just link and stop!!
		return;
	while(1)
	{

		while(process_runtime	<	Time_Quantom){
			if(count_time == pc[index].arrival_time)		//이렇게 하면 동시간대에 프로세서가 중복되게 도착할때 , 두가지다 같은 time에 출력하고 , 인서트할수 있다. 세개 이상부터는 음..잘모르겠다.
				Insert(index++ , pc);					//후위 ++ 연산자를 써야 가능 , 전위 연산자를 사용할 경우 , 틀리다!.
			printf("<time %d> process %d is running\n" , count_time , head ->pid);
			(head->burst_time)--;
			count_time++;						//이것은 딜리트 펑션뒤에? 정확히 모르겟네. 
			process_runtime++;

			if(	head->burst_time == 0)								//this is different when comared with FCFS!!! 
			{
				Delete(index , pc);			//this node finished , so call delete function.
				break;
			}
			else if(process_runtime	>=	Time_Quantom){

				Process *ptr = head;
				if(head->next == NULL){		//마지막 프로세서일때
					while(head->burst_time != 0){
						head->burst_time--;					
						printf("<time %d> process %d is running\n" , count_time++ , head ->pid);
					}
					return 0;
				}
				head = head ->next;
				rear ->next = ptr;
				rear =rear->next;
				rear ->next = NULL;			//for searching , beacuse this goes to last position.
				ptr = NULL;

				printf("------------------------------------------- (Context - Switch) \n");
				break;
			}				
		}
		
		if(index == MAX && head == NULL)
			break;
		process_runtime = 0;	

	}
}


int Delete(int index ,Process *pc){
	Process *deleteNode = NULL;//for deleting , make temporaliy delete node.
	deleteNode = head;
	head = head ->next;		//go to the next process.
	//free(deleteNode);
	
	
	printf("<time %d> process %d is finished\n" , count_time -1, deleteNode ->pid);
	if(index == MAX && head == NULL)
	{
		printf("<time %d> All process is finished\n" , count_time -1, deleteNode ->pid);
		return 0;
	}
	printf("------------------------------------------- (Context - Switch) \n");
	//free(deleteNode);//free the used process.
	if(head == NULL){		//if head is null , go back to running function and find the new nodes!!!
		Running(pc);
		return 1;
	}			
	//free(deleteNode);  왜안되는거야 시팍
	
}
//free(deleteNode);

