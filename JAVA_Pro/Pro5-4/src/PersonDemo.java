public class PersonDemo {
	public static void main(String[] args) {
		Person s1 = new Person("Minjun", 26);		//set values.
		Person s2 = new Person("Hongu", 27);
		System.out.println("Ther are two example for test case");
		System.out.println(s1.getName() + " " + s1.getAge());
		System.out.println(s2.getName() + " " + s2.getAge());
		System.out.println();
		judge(s1.getName(), s2.getName());		//whether two peoples names are same or not.
		judge(s1.getAge(), s2.getAge());		//whether two peoples ages are same or not.
		judge(s1.getName(), s1.getAge(), s2.getName(), s2.getAge());  //whether two peoples names and ages are same or not.
	}

	/**
	 * Using method's Overloads 
	 */

	//judging names are same or not.
	public static void judge(String A_Name, String B_Name) {
		if (A_Name.equalsIgnoreCase(B_Name))
			System.out.println("Two people have same name");
		else
			System.out.println("They have different name");
	}
	

	//judging ages are same or not.
	public static void judge(int A_Age, int B_Age) {
		if (A_Age == B_Age)
			System.out.println("Two people's age are same.");
		else
			System.out.println("There Age is Not same");
	}

	//judging names and ages are same or not.
	public static void judge(String A_Name, int A_Age, String B_Name, int B_Age) {
		if (A_Name.equalsIgnoreCase(B_Name) && (A_Age == B_Age)) {
			System.out.println("Two peoples Name and Age are Same");
		} else {
			System.out.println("Two people are not same people");

			if (A_Age > B_Age) {
				System.out.println(A_Name + " is older than " + B_Name);
			} else if (A_Age < B_Age) {
				System.out.println(A_Name + " is younger than " + B_Name);
			} else {
				System.out.println("There Age is same, but name is different");
			}
		}
	}
}
