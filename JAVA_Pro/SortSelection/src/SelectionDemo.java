public class SelectionDemo {
	public static int arr[];

	public static void main(String[] args) {
		arr = new int[20];
		Selection s = new Selection();
		arr = s.setValue(arr);
		arr = s.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
