import java.util.HashSet;

public class SudokuPuzzle {
	int[][] board; // represents current state, 0 indicates blank square.
	boolean[][] start; // boolean values that indicates which squares in board
						// are true.

	/**
	 * This is for initializing sudoku.
	 */

	public SudokuPuzzle() {
		board = new int[9][9];
		start = new boolean[9][9];
	}

	/**
	 * Initializing by using random, num of init values are limited in 25.
	 */
	public void init() {
		int num_of_initValue = (int) (Math.random() * 25);
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				board[row][col] = 0;
				start[row][col] = false;
			}
		}

		int count = 0;
		/**
		 * Initialize variables based on sudoku rule until count equals
		 * num_of_initValue.
		 */
		while (count < num_of_initValue) {
			int temp_row = (int) (Math.random() * 9);
			int temp_col = (int) (Math.random() * 9);
			if (board[temp_row][temp_col] == 0) {
				int temp_value = (int) (Math.random() * 10);
				addGuess(temp_row, temp_col, temp_value);
				if (checkPuzzle() == true) {
					addInitial(temp_row, temp_col, temp_value);
					count++;
				} else
					// if checkPuzzle() returns false.
					reset();

			}

		}

	}

	public String toString() {
		String print = "";
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				print += board[row][col] + " ";
			}
			print += "\n";
		}
		return print;
	}

	/**
	 * add value to board.
	 */
	public void addInitial(int row, int col, int value) {
		board[row][col] = value;
		start[row][col] = true;
	}

	public void addGuess(int row, int col, int value) {
		board[row][col] = value;
	}

	public boolean checkPuzzle() {
		HashSet<Integer> check_duplicate = new HashSet<Integer>();
		// Hashset is used for checking wheater violates sudoku rule or
					// not.
		int row = 0;
		int col = 0;

		/**
		 * checking through based on 3X3 Matrix.
		 */
		for (int block = 0; block < board.length; block++) {

			check_duplicate.clear();		//clear hashset.
			/**
			 * 주어진 board가 81개이고, 이것은 3 X 3 블락내에서 1 ~ 9 까지 중복된 값이 존재하면 되지 않으므로 3
			 * X 3 블락을 9개로 나누어서 탐사하기 위해서는 밑에 주어진 if문이 주어져야 프로그램은 탐색할 수 있다.
			 */
			if (block <=2)
				row = 0;
			else if (block >=3 && block <= 5)
				row = 3;
			else if (block >= 6 && block < board.length)
				row = 6;
			
			col = (3 * (block % 3));
			int standard_row = row + 3;
			int standard_col = col + 3;
			for (; row < standard_row; row++) {
				for (; col < standard_col; col++) {
					if (board[row][col] != 0) {
						// this means there is duplicated values in 3X3 Matrix.
						if (check_duplicate.contains(board[row][col]))
							return false;
						else
							check_duplicate.add(board[row][col]);
					}
				}
				col = (3 * (block % 3));
			}
		}

		/**
		 * Checking through based on row.
		 */
		for (row = 0; row < board.length; row++) {
			check_duplicate.clear(); //clear hashset.

			for (col = 0; col < board[row].length; col++) {
				if (board[row][col] != 0) {
					if (check_duplicate.contains(board[row][col]))
						return false;
					else
						check_duplicate.add(board[row][col]);
				}
			}
		}

		/**
		 * Checking through based on col.
		 */
		for (col = 0; col < board[0].length; col++) {
			check_duplicate.clear();//clear hashset.

			for (row = 0; row < board.length; row++) {
				if (board[row][col] != 0) {
					if (check_duplicate.contains(board[row][col]))
						return false;
					else
						check_duplicate.add(board[row][col]);
				}
			}
		}

		return true; // this means program passed or checking loop.
	}

	// return value.
	public int getValueIn(int row, int col) {
		return board[row][col];
	}

	/**
	 * Check wheather board is full or not by using start attribute.
	 * 
	 * @return
	 */
	public boolean isFull() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (start[row][col] != true)
					return false;
			}
		}
		return true;
	}

	/**
	 * Reset nonpermanent square which starts value is not true(not satisfying
	 * sudoku rule).
	 */
	public void reset() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (start[row][col] != true)
					board[row][col] = 0;
			}
		}
	}
}
