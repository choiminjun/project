public class InsertionSort {

	public int[] sort(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int standard = arr[i]; // get standard value for compare.
			int standard_index = i - 1; // standard index of comparing

			while (standard_index >= 0 && standard < arr[standard_index]) {
				swap(standard_index + 1, standard_index, arr); // shift right.
				standard_index--;
			}
			arr[standard_index + 1] = standard; // save standard value.
		}
		return arr;
	}

	/**
	 * swap values in array.
	 */
	public void swap(int i, int j, int[] arr) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public void display(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
