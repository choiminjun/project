/**
 Program to display triangle.
 Author : Choi Min Jun. 200932224
 Email Address: choiminjun0720@gmail.com
 quiz2.
 Last Changed : 28/03/2014.
 */
package npro1;

import java.util.Scanner;

public class testCircle {
	public static void main(String[] args) {
		int line;
		Scanner keyboard = new Scanner(System.in);
		System.out
				.println("Enter the number of astersik for the base of ther triangle : ");
		line = keyboard.nextInt();
		if (line < 0)
			System.out.println("error");
		else {
			for (int i = 1; i <= line; i++) {
				for (int temp = 0; temp < i; temp++) {
					System.out.print("*");
				}
				System.out.println();
			}
		}
	}
}