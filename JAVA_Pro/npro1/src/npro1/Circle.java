package npro1;

import java.util.Scanner;

public class Circle {
	private double radius;
	private double area;
	private double diameter;

	public void setRadius(double r) {
		if (r > 0)
			this.radius = r;
		else
			System.out.println("Error");
	}

	public double getRadius() {
		return radius;
	}

	public double computeDiameter() {
		return 2 * this.radius;
	}

	public double computeArea() {
		return 3.14 * this.radius * this.radius;
	}

}
