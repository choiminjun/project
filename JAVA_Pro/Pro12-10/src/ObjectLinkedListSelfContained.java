public class ObjectLinkedListSelfContained {
	private ListNode head;

	public ObjectLinkedListSelfContained() {
		this.head = null;
	}

	public void showList() {
		ListNode position = head;
		while (position != null) {
			String str = position.getData(position.data);
			System.out.println(str);
			position = position.link;
		}
	}

	public int length() {
		int count = 0;
		ListNode position = head;
		while (position != null) {
			count++;
			position = position.link;
		}
		return count;
	}

	public void addANodeToStart(Employee addData) {
		head = new ListNode(addData, head);
	}

	public void deleteHeadNode() {
		if (head != null)
			head = head.link;
		else {
			System.out.println("Deleting from an empty list.");
			System.exit(0);
		}
	}
/*
	public boolean onList(int targetSocialNumber) {
		if(find(targetSocialNumber) != null )
			return true;
		else
			return false;
		
	}
*/
	public Employee find(int targetSocialNumber) {
		boolean found = false;
		ListNode position = head;
		while ((position != null) && !found) {
			Employee dataAtPosition = position.data;
			if (dataAtPosition.getId() == targetSocialNumber)
				found = true;
			else
				position = position.link;
		}
		if(found == false){
			Employee temp = null;
			return temp;
		}
		return position.data;
	}

	private class ListNode {
		private Employee data;
		private ListNode link;

		public ListNode() {
			link = null;
			data = null;
		}

		public ListNode(Employee newData, ListNode linkValue) {
			data = newData;
			link = linkValue;
		}

		/**
		 * Return String which displays Employee's info by traversing nodes from head to tail.
		 * @param data
		 * @return
		 */
		public String getData(Employee data) {
			String str = "ID: " + data.getId() + " Name: "
					+ data.getName() + " Department: " + data.getDepartment()
					+ " expLevel: " + data.getExpLevel();
			return str;
		}
	}
}
