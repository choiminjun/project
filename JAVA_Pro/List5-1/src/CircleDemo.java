
public class CircleDemo {
	public static void main(String[] args){
		Circle s = new Circle();
		s.setRadius(3.5);
		double diameter = s.computeDiameter();
		double area = s.computeArea();
		
		System.out.println("The Radius is" + s.getRadius());
		System.out.println("The diameter is "+ diameter);
		System.out.println("The area is " + area);
	}
}
