public class Species {
	private String name;
	private int population;
	private double growthRate;

	public void setSpecies(String newName, int newPopulation,
			double newGrowthRate) {
		name = newName;
		population = newPopulation;
		growthRate = newGrowthRate;
	}

	public boolean equals(Species otherObject) {	//variable is Object.
		System.out.println(this);
		
		if(this == otherObject)
			System.out.println("SAME");
		
		if (this.name.equals(otherObject.name)
				&& (this.population == otherObject.population)
				&& (this.growthRate == otherObject.growthRate))
			return true;
		else
			return false;
	}
}
