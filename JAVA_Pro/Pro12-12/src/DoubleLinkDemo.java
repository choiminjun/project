/**
 * Program:12-12 DoubleLinked List.
 *  Author:Choi Min Jun 
 *  Last Changed: 06/06/2014
 * @author MINJUN
 */
import java.util.Scanner;
public class DoubleLinkDemo {
	public static void main(String[] args){
		PracDoubleLink s1 = new PracDoubleLink();
		String a = "yes";
		while(a.equalsIgnoreCase("yes")){
			Scanner keyboard = new Scanner(System.in);
			String addData;
			System.out.println("Put String");
			addData = keyboard.nextLine();
			s1.addANodeToStart(addData);
			System.out.println("you want put more nodes?");
			a = keyboard.next();
		}
		s1.showList();
		System.out.println();
		s1.showList2();
	}	
}
