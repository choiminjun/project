import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>SudokuPuzzleDemoTest</code> contains tests for the class <code>{@link SudokuPuzzleDemo}</code>.
 *
 * @generatedBy CodePro at 14. 6. 16 오후 3:49
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class SudokuPuzzleDemoTest {
	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		SudokuPuzzleDemo.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.util.NoSuchElementException
		//       at java.util.Scanner.throwFor(Scanner.java:838)
		//       at java.util.Scanner.next(Scanner.java:1461)
		//       at java.util.Scanner.nextInt(Scanner.java:2091)
		//       at java.util.Scanner.nextInt(Scanner.java:2050)
		//       at SudokuPuzzleDemo.main(SudokuPuzzleDemo.java:25)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Test
	public void testMain_2()
		throws Exception {
		String[] args = new String[] {};

		SudokuPuzzleDemo.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.util.NoSuchElementException
		//       at java.util.Scanner.throwFor(Scanner.java:838)
		//       at java.util.Scanner.next(Scanner.java:1461)
		//       at java.util.Scanner.nextInt(Scanner.java:2091)
		//       at java.util.Scanner.nextInt(Scanner.java:2050)
		//       at SudokuPuzzleDemo.main(SudokuPuzzleDemo.java:25)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Test
	public void testMain_3()
		throws Exception {
		String[] args = new String[] {};

		SudokuPuzzleDemo.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.util.NoSuchElementException
		//       at java.util.Scanner.throwFor(Scanner.java:838)
		//       at java.util.Scanner.next(Scanner.java:1461)
		//       at java.util.Scanner.nextInt(Scanner.java:2091)
		//       at java.util.Scanner.nextInt(Scanner.java:2050)
		//       at SudokuPuzzleDemo.main(SudokuPuzzleDemo.java:25)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Test
	public void testMain_4()
		throws Exception {
		String[] args = new String[] {};

		SudokuPuzzleDemo.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.util.NoSuchElementException
		//       at java.util.Scanner.throwFor(Scanner.java:838)
		//       at java.util.Scanner.next(Scanner.java:1461)
		//       at java.util.Scanner.nextInt(Scanner.java:2091)
		//       at java.util.Scanner.nextInt(Scanner.java:2050)
		//       at SudokuPuzzleDemo.main(SudokuPuzzleDemo.java:25)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Test
	public void testMain_5()
		throws Exception {
		String[] args = new String[] {};

		SudokuPuzzleDemo.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.util.NoSuchElementException
		//       at java.util.Scanner.throwFor(Scanner.java:838)
		//       at java.util.Scanner.next(Scanner.java:1461)
		//       at java.util.Scanner.nextInt(Scanner.java:2091)
		//       at java.util.Scanner.nextInt(Scanner.java:2050)
		//       at SudokuPuzzleDemo.main(SudokuPuzzleDemo.java:25)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:49
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SudokuPuzzleDemoTest.class);
	}
}