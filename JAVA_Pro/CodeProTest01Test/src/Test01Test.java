import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>Test01Test</code> contains tests for the class <code>{@link Test01}</code>.
 *
 * @generatedBy CodePro at 14. 5. 27 오후 7:48
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class Test01Test {
	/**
	 * Run the int add(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 7:48
	 */
	@Test
	public void testAdd_1()
		throws Exception {
		Test01 fixture = new Test01();
		int x = 2;
		int y = 1;

		int result = fixture.add(x, y);

		// add additional test code here
		assertEquals(3, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 7:48
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 7:48
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 7:48
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(Test01Test.class);
	}
}