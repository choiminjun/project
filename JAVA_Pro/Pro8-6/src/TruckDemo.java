/**
 * Program:8-6.
 *  Author:Choi Min Jun 
 *  Last Changed: 31/05/2014
 * @author MINJUN
 */
public class TruckDemo {
	public static void main(String[] args) {
		Truck a = new Truck("minjun", "Hyundai", 20, 30.5, 50.5);

		Truck b = new Truck("minjun1", "Hyundai", 20, 30.5, 50.5);
		if (a.equals(b))
			System.out.println("They are same Truck.");
		else
			System.out.println("They are not same Truck!!.");
	}
}
