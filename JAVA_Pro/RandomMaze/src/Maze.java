
public class Maze {
	int maze[][]; // maze with walls that block
	int check_visited[][]; // memorization to avoid duplicates
	int dest_x;
	int dest_y;

	public Maze(int row, int col) {
		maze = new int[row][col];
		check_visited = new int[row][col];
		this.init(row, col); // initialize maze
	}

	private void init(int row, int col) {
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				maze[i][j] = check_visited[i][j] = 0;
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				if ((int)(Math.random() *4) % 3 != 0)
					maze[i][j] = 1;
		dest_x = row - 1;
		dest_y = col - 1;
		maze[dest_x][dest_y] = 0;
		maze[0][0] = 0;
	}

	public void dfs(int row, int col) {
        
		if ((row < 0) || (col < 0) || row > dest_x || col > dest_y)
			return;
		if (maze[row][col] == 1 || check_visited[row][col] != 0)
			return; // failure
		System.out.println(" Visit:(" + row + "," + col + ")"); // output
		if (row == dest_x && col == dest_y) { // goal coordinates found
			System.out.println("goal found");
			display();
			System.exit(1);
		}
		check_visited[row][col] = 1; // memorize, not to be check_visited again
        
		try {
			dfs(row, col + 1);
			dfs(row, col - 1); // calls to neighbors
			dfs(row + 1, col);
			dfs(row - 1, col); // calls to neighbors
		} catch (Exception ArrayIndexOfBoundsException) {
			;
		}
	}

	public void display() {
		for (int row = 0; row < maze.length; row++) {
			for (int col = 0; col < maze[row].length; col++) {
				System.out.print(maze[row][col] + " ");
			}
			System.out.println();
		}
	}
}



