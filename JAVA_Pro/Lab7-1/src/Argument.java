/**
 * Using command line argument.
 * @author MINJUN
 */
public class Argument {
	public static void main(String[] args) {
		int tot = 0;
		for (int i = 0; i < args.length; i++){
			tot += Integer.parseInt(args[i]);
			System.out.println(args[i]);
		}
		System.out.println("Average is.. " + tot / (double) args.length);
	}
}
