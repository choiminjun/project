/**
 * Program:11-4 Calculating sum.
 *  Author:Choi Min Jun 
 *  Last Changed: 05/06/2014
 * @author MINJUN
 */
import java.util.Scanner;
public class RecursiveSum {
	public static void main(String[] args){
		int n = 0;
		String check = null;
		Scanner keyboard = new Scanner(System.in);
		do{
			System.out.println("Input value of n to get sum");
			n = keyboard.nextInt();
			if(n >= 0){
				System.out.println(n + "! is equal to ");
				int sumResult = sum(n);
				System.out.println(" Result:" + sumResult);
			}
			System.out.println("You want more try?");
			check = keyboard.next();
		}while(check.equalsIgnoreCase("y"));
	}
	public static int sum(int n){
		if(n == 1){
			System.out.print(n);
			return 1;
		}else{
			System.out.print(n + " + ");
			return ( n + sum(n - 1) );
		}
	}
}
