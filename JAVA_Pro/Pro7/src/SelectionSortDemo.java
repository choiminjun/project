/**
 * Programe:Selection Sort Using two Classes.
 *  Author:Choi Min Jun 
 *  Last Changed: 02/05/2014
 * @author MINJUN
 */
public class SelectionSortDemo {
	public static void main(String[] args) {
		int[] arr = new int[] { 10, 2, 8, 7, 2, 3, 4, 5, 11, 12, 15 };
		System.out.println("BEFORE");
		display(arr); // before sorting.
		ArraySorter.selectionSort(arr);
		System.out.println("\nAfter");
		display(arr); // after sorting.
	}

	public static void display(int[] dis_Arr) {
		for (int i = 0; i < dis_Arr.length; i++) {
			System.out.print(dis_Arr[i] + " ");
		}
	}
}
