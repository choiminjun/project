/**
 * This is class that includes swap method which swap two values in array by
 * using two int parameters.
 * 
 * @author MINJUN
 * 
 */
public class ArraySorter {
	public static int min = 0; // this is used for finding minimum value.
	public static int min_position = 0; // this is used for swap method to
										// switch value.

	public static void selectionSort(int[] arr) {
		for (int index = 0; index < arr.length - 1; index++) {
			min_position = index;
			for (int findmin = index + 1; findmin < arr.length; findmin++) {
				if (arr[min_position] > arr[findmin]) {
					min_position = findmin; // get min_position in laset
											// sequence of array.
				} else {
					;
				}
			}
			ArraySorter.swap(index, min_position, arr);
		}
	}

	public static void swap(int i, int j, int[] arr) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

}
