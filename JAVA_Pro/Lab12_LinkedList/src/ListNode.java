
public class ListNode {
	public String data;
	private ListNode link;
	
	public ListNode(){
		data = null;
		link = null;
	}
	public ListNode(String data, ListNode link){
		this.data = data;
		this.link = link;
	}
	public void setData(String data){
		this.data = data;
	}
	public void setLink(ListNode link){
		this.link = link;
	}
	public String getData(){
		return this.data;
	}
	public ListNode getLink(){
		return this.link;
	}
	
}
