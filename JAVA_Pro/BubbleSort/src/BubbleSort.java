
public class BubbleSort {
	/**
	 * sort array using bubble sort in ascending order.
	 */
	/*
	public int[] sort(int[] arr){
		for(int i = 0 ; i < arr.length ; i++){
			for(int j = 0 ; j < arr.length-1 ; j++){
				if(arr[j] > arr[j+1]){	//switch value in array by ascending order.
					swap(j,j+1,arr);
				}
			}
		}
		return arr;
	}
	*/
	public int[] sort(int[] arr){
		for(int i = 0; i < arr.length - 1; i++){
			for(int j = 0 ; j < arr.length -(i+1); j++){
				if(arr[j] > arr[j+1] ){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		return arr;
	}
	/**
	 * display array 
	 */
	public void swap(int i , int j , int[] arr){
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	public void display(int[] arr){
		for(int i = 0 ;  i < arr.length ; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
}
