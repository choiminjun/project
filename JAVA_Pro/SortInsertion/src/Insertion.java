public class Insertion {
	public int[] sort(int[] arr) {
		int i, j;
		for (i = 1; i < arr.length; i++) {
			int sort_data = arr[i];
			for (j = i - 1; j >= 0; j--) {

				if (sort_data < arr[j])
					arr[j + 1] = arr[j];
				else
					break;
			}
			arr[j + 1] = sort_data;
		}
		return arr;
	}
	public int[] setValue(int[] arr) {
		for (int i = 0; i < arr.length; i++)
			arr[i] = (int) (Math.random() * 21);
		return arr;
	}
}
