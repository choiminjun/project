/**
 Program displays grade.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 4 - 6.
 Last Changed : 26/03/2014.
 */
import java.util.ArrayList;
import java.util.Scanner;
public class DisplayGrade {
	public static void main(String[] args){
		ArrayList<Integer> arr =  new ArrayList<Integer>(); 
		Scanner keyboard = new Scanner(System.in);
		int num;
		do{
			System.out.println("Put grade score :");
			num = keyboard.nextInt();
			if(num >= 0)
				arr.add(num);
			else
				System.out.println("End");
		}while(num >= 0);
	
		int tot_num_of_grades = arr.size();
		int a, b, c, d, f;			//each indicate grade of subject.
		a = b = c = d = f = 0;
		for(int i = 0 ; i < arr.size() ; i++){
			if(arr.get(i) >= 90)
				a++;
			else if(arr.get(i) >= 80)
				b++;
			else if(arr.get(i) >=70	)
				c++;
			else if(arr.get(i) >= 60)
				d++;
			else
				f++;
		}
		
		System.out.println("Total number of grades = " + tot_num_of_grades);
		System.out.println("Number of A's = " + a);
		System.out.println("Number of B's = " + b);
		System.out.println("Number of C's = " + c);
		System.out.println("Number of D's = " + d);
		System.out.println("Number of F's = " + f);
	}
	
}
