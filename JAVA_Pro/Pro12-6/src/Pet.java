import java.util.Scanner;

public class Pet {
	private String name;
	private int age;
	private double weight;

	/**
	 * constructor using overloading.
	 */
	public Pet() {
		name = "No name yet.";
		age = 0;
		weight = 0;
	}

	public Pet(String initialName) {
		name = initialName;
		age = 0;
		weight = 0;

	}

	public Pet(int initialAge) {
		name = "No name yet.";
		weight = 0;
		if (initialAge < 0) {
			System.out.println("Eroro:Negarive age.");
			System.exit(0);
		} else
			age = initialAge;
	}

	public Pet(double initialWeight) {
		name = "No name yet";
		age = 0;
		if (initialWeight < 0) {
			System.out.println("Error: Negative weight.");
			System.exit(0);
		} else {
			weight = initialWeight;
		}

	}

	public Pet(String initialName, int initialAge, double initialWeight) {
		name = initialName;
		if ((initialAge < 0) || (initialAge < 0)) {
			System.out.println("Error: Negative age or weight.");
			System.exit(0);

		} else {
			age = initialAge;
			weight = initialWeight;
		}
	}

	public void setWeight(double newWeight) {
		if (newWeight < 0) {
			System.out.println("Error: Negative weight.");
			System.exit(0);
		} else {
			weight = newWeight;
		}
	}

	/**
	 * Accesoor method.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public double getWeight() {
		return weight;
	}

	// write method.
	public void writeOutput() {
		System.out.println("Name : " + name);
		System.out.println("Age: " + age + " years.");
		System.out.println("Weight : " + weight + "pounds.");
	}

	public void setAge(int newAge) {
		if (newAge < 0) {
			System.out.println("Error: Negarive age.");
			System.exit(0);
		} else {
			age = newAge;
		}
	}

	public void setPet(String newName, int newAge, double newWeight) {
		name = newName;
		if ((newAge < 0) || (newWeight < 0)) {
			System.out.println("Error : Negarive age or weight.");
			System.exit(0);
		} else {
			age = newAge;
			weight = newWeight;
		}
	}

	/**
	 * Overriding.
	 */
	public String toString() {
		String str = "Pet Name: " + name + " Age: " + age + " Weight: "
				+ weight;
		return str;
	}
}
