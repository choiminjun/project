/**
 * Program:12-6 Sort Using File
 * Author:Choi Min
 * Jun Last Changed: 05/06/2014
 * 
 * @author MINJUN
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class PetArrayList {
	public static void main(String[] args) {
		ArrayList<Pet> list = null;
		String infileName, outfileName; // file name.
		Scanner in = null; // file reader.
		PrintWriter out = null; // file writer.
		try {
			Scanner keyboard = new Scanner(System.in);
			System.out.println("Put ReadFileName");
			infileName = keyboard.nextLine(); // get fileName.
			list = new ArrayList<Pet>();
			in = new Scanner(new File(infileName)); // allocate file for
													// Scanner.

			while (in.hasNextLine()) {
				String name = in.next();
				int age = in.nextInt();
				double weight = in.nextDouble();
				Pet newPet = new Pet(name, age, weight); // using constructor.
				list.add(newPet);
			}
			// call sort method.
			list = sortByWeight(list);
			in.close(); // close Scanner.

			System.out.println("Put Output fileName");
			outfileName = keyboard.nextLine();
			out = new PrintWriter(new FileOutputStream(outfileName, true)); // true
																			// means
																			// that
																			// printwriter
																			// can
																			// write
																			// numofpet
																			// to
																			// implicate
																			// range
																			// of
																			// weight
																			// of
																			// them

			for (int i = 0; i < list.size(); i++) {
				out.println(list.get(i).toString()); // write file.
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * get string of num of pets weights.
		 */
		String strPercent = getPercentageOfWeight(list);
		out.println(strPercent);
		out.close(); // close PrintWriter.
	}

	/**
	 * Sort list by using compare method and return arrayList.
	 * 
	 * @param list
	 * @return
	 */
	public static ArrayList<Pet> sortByWeight(ArrayList<Pet> list) {
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size() - 1; j++) {
				if (list.get(j).getWeight() > list.get(j + 1).getWeight()) {
					Pet temp = list.get(j + 1);
					list.set(j + 1, list.get(j));
					list.set(j, temp);
				}
			}
		}
		return list;
	}

	/**
	 * Get Percentage and number of Pets.
	 */
	private static String getPercentageOfWeight(ArrayList<Pet> list) {

		int numUnderFivePounds, numBetweenFivetoTenPounds, numUpperTenPounds;
		numUnderFivePounds = numBetweenFivetoTenPounds = numUpperTenPounds = 0;

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getWeight() < 5)
				numUnderFivePounds++;
			else if (list.get(i).getWeight() >= 5
					&& list.get(i).getWeight() <= 10)
				numBetweenFivetoTenPounds++;
			else
				numUpperTenPounds++;
		}
		int tot = numUnderFivePounds + numBetweenFivetoTenPounds
				+ numUpperTenPounds;
		String str = "Num,Percent Under 5Pounds : " + numUnderFivePounds
				+ " , " + numUnderFivePounds / (double) tot + "\n";
		str += "Num,Percent Between 5Pounds to 10Pounds: "
				+ numBetweenFivetoTenPounds + " , " + numBetweenFivetoTenPounds
				/ (double) tot + "\n";
		str += "Num,Percent Upper 10Pounds: " + numUpperTenPounds + " , "
				+ numUpperTenPounds / (double) tot + "\n";
		return str;
	}
}
