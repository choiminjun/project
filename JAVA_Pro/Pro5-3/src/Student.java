public class Student {
	private int quiz1;
	private int quiz2;
	private int midterm;
	private int finalterm;
	private String finalgrade;
//using constructor.
	public Student() {
		quiz1 = 0;
		quiz2 = 0;
		midterm = 0;
		finalterm = 0;
	}

	/**
	 * public Student(int a, int b, int c, int d){ this.quiz1 = a; this.quiz2 =
	 * b; this.midterm = c; this.finalterm = d; }
	 **/
	public void setScore(int a, int b, int c, int d) {
		this.quiz1 = a;
		this.quiz2 = b;
		this.midterm = c;
		this.finalterm = d;
	}

	public String CalculateScore() {
		double temp = (((quiz1 + quiz2)*10) * 0.25) + (midterm * 0.25)
				+ (finalterm * 0.5);
		if(temp >= 90)
			finalgrade = "A";
		else if( temp >= 80)
			finalgrade = "B";
		else if(temp >= 70)
			finalgrade = "C";
		else if(temp >= 60)
			finalgrade = "D";
		else
			finalgrade = "F";
		
		return finalgrade;	//return finalgrade.
	}
}
