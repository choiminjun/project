/**
 Program to Calculate grade
  Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 5 - 3.
 Last Changed : 04/04/2014.
 */
import java.util.Scanner;

public class StudentDemo {
	public static int a, b, c, d; // this is for saving temp test score.

	public static void main(String[] args) {
		Student s = new Student();
		getScore(); // get score by using method.
		s.setScore(a, b, c, d); // set score.
		String result = s.CalculateScore();
		System.out.println("The Student's Total Grade is " + result);
	}

	public static void getScore() {
		System.out.println("Put Quiz1, Quiz2, Mid, Final Scores");
		Scanner keyboard = new Scanner(System.in);
		a = keyboard.nextInt();
		b = keyboard.nextInt();
		c = keyboard.nextInt();
		d = keyboard.nextInt();
		boolean checkInput = judge(a, b, c, d); // check wheather input's are
		if (checkInput == false) { // appropriate.
			System.out.println("Your Input's are wrong try again");
			getScore();
		}
	}
/**
 * checking wheater inputs for test score is proper or not.
 */
	private static boolean judge(int a, int b, int c, int d) {
		if ((a >= 0 && a <= 10) && (b >= 0 && b <= 10) && (c >= 0 && c <= 100)
				&& (d >= 0 && d <= 100))
			return true;
		else
			return false;
	}

}
