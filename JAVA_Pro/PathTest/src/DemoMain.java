import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DemoMain {
	public static void main(String[] args) {
		extractUrlParts();
	}

	public static void extractUrlParts(){
		String testurl = "https://goodidea.tistory.com:8888/qr/aa/ddd.html?abc=def&ddd=fgf#sharp";
		
		Pattern urlPattern = Pattern.compile("^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$");


		Matcher mc = urlPattern.matcher(testurl);
		
		if(mc.matches()){
			for(int i = 0; i < mc.groupCount(); i++)
				System.out.println("group("+i+") " + mc.group(i));
		}
		else
			System.out.println("not found");
	}
}