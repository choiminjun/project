/**
 Program to add odd numbers.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 quiz1.
 Last Changed : 28/03/2014.
 */
package quiz1;

import java.util.Scanner;

public class addOdd {
	public static void main(String[] args){
		int end_num;
		int sum_of_odd = 0;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please input a positive integer as the end value :");
		end_num = keyboard.nextInt();
		
		if(end_num < 1){
			System.out.println("Error");
		}
		else{
			int count = 1;
			while(count <= end_num){
				if(count % 2 != 0 ){
					sum_of_odd += count;
				}
				count++;
			}
		}
		System.out.println("The summation is : " + sum_of_odd);
	}
}
