public class GradeDistribution {
	private static final int num_of_subject = (int) (Math.random() * 100 % 100);
	private static final String data = "ABCDF"; // this is used for setting out
												// of grades radommically.

	private char grade[] = new char[num_of_subject];

	/**
	 * set grades by using random methods.
	 */
	public void setLetterGrades() {
		for (int i = 0; i < num_of_subject; i++) {
			int index = (int) (Math.random() * 10) % data.length();
			grade[i] = data.charAt(index); // set grades for each subject.
		}
	}

	/**
	 * return each num of subject by method.
	 */
	public int readGradeA() {
		int count = 0; // for counting grade.
		for (int i = 0; i < num_of_subject; i++) {
			if (grade[i] == 'A')
				count++;
		}
		return count;
	}

	public int readGradeB() {
		int count = 0;
		for (int i = 0; i < num_of_subject; i++) {
			if (grade[i] == 'B')
				count++;
		}
		return count;
	}

	public int readGradeC() {
		int count = 0;
		for (int i = 0; i < num_of_subject; i++) {
			if (grade[i] == 'C')
				count++;
		}
		return count;
	}

	public int readGradeD() {
		int count = 0;
		for (int i = 0; i < num_of_subject; i++) {
			if (grade[i] == 'D')
				count++;
		}
		return count;
	}

	public int readGradeF() {
		int count = 0;
		for (int i = 0; i < num_of_subject; i++) {
			if (grade[i] == 'F')
				count++;
		}
		return count;
	}
}
