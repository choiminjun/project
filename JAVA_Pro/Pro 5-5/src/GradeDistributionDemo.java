/**
 * Program drawing graph. Author : Choi Min Jun. Email Address:
 * choiminjun0720@gmail.com Programming Projects 5 - 5. Last Changed :
 * 04/04/2014.
 */

public class GradeDistributionDemo {
	private static int A, B, C, D, F;
	private static int tot;
	private static int asterisk_per_one_subject;

	public static void main(String[] args) {
		GradeDistribution s = new GradeDistribution();

		s.setLetterGrades(); // set grades.

		A = s.readGradeA(); // get num of grades which are applicable for.
		B = s.readGradeB();
		C = s.readGradeC();
		D = s.readGradeD();
		F = s.readGradeF();

		tot = A + B + C + D + F; // get tot num of subject.

		asterisk_per_one_subject = (int) (100 / tot); // calculating asterisk.

		drawGraph(); // draw graph using method.
	}

	private static void drawGraph() {
		System.out
				.println("0    10    20    30    40    50    60    70    80    90    100%");
		System.out
				.println("|    |     |     |     |     |     |     |     |     |     |");
		System.out
				.println("************************************************************");
		for (int i = 0; i < (int) Math // using Math method for �ø�.
				.ceil((double) (A * asterisk_per_one_subject) / 2); i++)
			System.out.print("*");

		System.out.print(" A\n");

		for (int i = 0; i < (int) Math
				.ceil((double) (B * asterisk_per_one_subject) / 2); i++)
			System.out.print("*");
		System.out.print(" B\n");

		for (int i = 0; i < (int) Math
				.ceil((double) (C * asterisk_per_one_subject) / 2); i++)
			System.out.print("*");
		System.out.print(" C\n");

		for (int i = 0; i < (int) Math
				.ceil((double) (D * asterisk_per_one_subject) / 2); i++)
			System.out.print("*");
		System.out.print(" D\n");

		for (int i = 0; i < (int) Math
				.ceil((double) (F * asterisk_per_one_subject) / 2); i++)
			System.out.print("*");
		System.out.print(" F\n");

		System.out.print("A: " + A + " B: " + B + " C: " + C + " D: " + D
				+ "F: " + F);

	}
}
