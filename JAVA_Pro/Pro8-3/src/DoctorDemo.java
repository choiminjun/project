/**
 * Program:8-4.
 *  Author:Choi Min Jun 
 *  Last Changed: 30/05/2014
 * @author MINJUN
 */
public class DoctorDemo {
	public static void main(String[] args){
		Doctor a = new Doctor("Obstetrican",20.5);	//object
		Doctor b = new Doctor("Pediatrician", 35.3);
		if(a.equals(b))
			System.out.println("They are same");
		else
			System.out.println("They are not same");
	}
}
