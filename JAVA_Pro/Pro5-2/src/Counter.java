
public class Counter {
	private static int count;	//only one variable, this is used for counting.
	
	public void setCount(){
		this.count = 0;
	}
	public void countUp(){
		this.count++;
		displayCount();			//each time when up/down method calls, display current value.
	}
	public void countDown(){
		if(checkNonnegative() == true)		//checking whether count is negative or not.
			this.count--;
		else
			System.out.println("Count can't be negative!");
		displayCount();
	}
	private boolean checkNonnegative() {
		if(count <= 0)
			return false;
		else
			return true;
	}
	public int getCount(){
		return count;
	}
	public void displayCount(){			//displaying current value.
		System.out.println("Current count value is " + count);
	}
	
}
