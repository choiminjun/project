package test01;
//Using array list
import java.util.ArrayList;

public class test01 {
	public static void main(String[] args){
		String[] str = new String[10];
		str[0] = "hi";
		str[1] = "my";
		str[2] = "name";
		str[3] = "is";
		ArrayList<String> arr = getElements(str);
		System.out.println(arr);
		
		ArrayList<String> remove_result = removeElements(arr,"name");
		System.out.println(remove_result);
		
	}
	public static ArrayList<String> getElements(String[] str){
		ArrayList<String> list = new ArrayList<String>(str.length);
		for(int i = 0 ; i < str.length; i++){
			list.add(i, str[i]);
		}
		return list;
	}
	
	public static ArrayList<String> removeElements(ArrayList<String> list, String del){
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).toString().equals(del)){
				list.remove(i);
			}
		}
		return list;
	}
}
