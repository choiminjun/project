import java.util.Scanner;
public class Square {
	private double height;
	private double width;
	private double surfaceArea;
	
	public void setHeight(double x){
		this.height = x;
	}
	public void setWidth(double x){
		this.width = x;
	}
	public double computeArea(){
		double area = this.height * this.width;
		return area;
	}
}
