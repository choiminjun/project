import java.util.Scanner;

/**
 Program to display each number of four-digit numbers.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 2 - 4.
 Last Changed : 15/03/2014.
 */
public class Divisable {
	public static void main(String[] args){
		int x , y;
		boolean check = false;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input two numbers");
		x = keyboard.nextInt();
		y = keyboard.nextInt();
		check  = judging(x , y);
		if(check == true)
			System.out.println("Number " + x + " is divisable by " + y);
		else

			System.out.println("Number " + x + " is not divisable by " + y);
	}
	public static boolean judging(int x , int y){
		if(x % y == 0)
			return true;
		else
			return false;
			
	}
}
