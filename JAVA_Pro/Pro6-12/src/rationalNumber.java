public class rationalNumber {
	private int a;
	private int b;
	private double rational; // rational variable,

	/**
	 * constructor using ovelloads.
	 */
	public rationalNumber() {
		rational = 0 / 1;
	}

	public rationalNumber(int A, int B) {
		a = A;
		b = B;
		simple(a, b);// for getting GCD.
	}

	private void simple(int a, int b) {
		int GCD;
		GCD = getGCD(a, b);
		if (b > 0) {
			rational = ((double) (a / GCD)) / ((double) (b / GCD));
		}
	}

	/**
	 * Calculate GCD for two parameters.
	 */
	private int getGCD(int x, int y) {
		int value;
		int remain;
		int temp;
		if (y > x) {
			temp = x;
			x = y;
			y = temp;
		}

		value = x / y;// set value.
		remain = x % y;// set remainder.

		if (remain != 0)// if mods result is not 0
		{
			return getGCD(y, remain);// call GCD method recursively.
		} else if (remain == 0)// if remain is 0,return y.
		{
			return y;
		}
		return 0;
	}

	public double getValue()// getValue method : 유리수값을 double 값으로 return 하기 위한
							// method.
	{
		return rational;
	}

	// string method.
	public String toString() {
		String stringA;
		String stringB;
		String result;
		stringA = Integer.toString(a);
		stringB = Integer.toString(b);

		result = stringA + "/" + stringB;
		return result;// return value.

	}

}
