public class PointDemo {
	public static void main(String[] args) {
		Point begin = new Point(1, 1);
		Point end = new Point(2, 2);
		// System.out.println(s.toString());
		// System.out.println(s);
		Line b = new Line(begin, end);
		System.out.println(b.toString());
		System.out.println(b.getLength());
		
		
		Line c = new Line(0 , 0 , 10 ,10);
		System.out.println(c.toString());
		System.out.println(c.getLength());
		
		System.out.println("\n\n");
		
		Point3D d= new Point3D(1,2,3);
		System.out.println(d.toString());
		
		Point3D e = new Point3D();
		e.setZ(5);
		e.setX(1);
		e.setY(2);
		System.out.println(e.toString());
	}
}
