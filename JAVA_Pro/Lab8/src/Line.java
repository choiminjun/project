public class Line {
	Point begin;
	Point end;
/**
 * Integer 로 받은 것들은 memory allocation을 하지 않아도
 * 되지만 Object로 받은 것들은 new 를 선언하지 않아도 된다.
 *
 *	**Why? -> Because of has a relationship.
 */
	public Line() {
		begin = new Point();
		end = new Point();
	}
	public Line(int x1,int y1,int x2,int y2){
		begin = new Point(x1,y1);
		end = new Point(x2,y2);
	}
	public Line(Point begin, Point end) {
		this.begin = begin;
		this.end = end;
	}

	public Point getBegin() {
		return begin;
	}

	public Point getEnd() {
		return end;
	}

	public void setBegin(Point temp) {
		this.begin = temp;
	}

	public void setBeginX(int x) {
		this.begin.setX(x);
	}

	public void setBeginY(int y) {
		this.begin.setY(y);
	}

	public void setBeginXY(int x, int y) {
		this.begin.setX(x);
		this.begin.setY(y);
	}

	public void setEnd(Point temp) {
		this.end = temp;
	}

	public void setEndX(int x) {
		this.end.setX(x);
	}

	public void setEndY(int y) {
		this.end.setY(y);
	}

	public void setEndXY(int x, int y) {
		this.end.setX(x);
		this.end.setY(y);
	}

	public String toString() {
		String str = "(" + this.begin.getX() + "," + this.begin.getY()
				+ ") to " + "(" + this.end.getX() + "," + this.end.getX() + ")";
		return str;
	}

	public double getLength() {
		double length = ((this.begin.getX() - this.end.getX())
				* (this.begin.getX() - this.end.getX()) + (this.begin.getY() - this.end
				.getY()) * (this.begin.getY() - this.end.getY()));
		return Math.sqrt(length);
	}
}
