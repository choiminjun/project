
public class Triangle extends Shape{
	int base,length;
	public Triangle(){
		super();
		this.base = 0;
		this.length =0;
	}
	public Triangle(String color,int base, int length){
		super(color);
		this.base = base;
		this.length = length;
	}
	public double getArea(){
		return ((double)(base * length)/ 2);
	}
	public String toString(){
		String str = "("+this.color +"," + this.base + "," + this.length + ")";
		return str;
	}
}
