
public class ShapeDemo {
	public static void main(String[] args){
		Shape[] shape = new Shape[2];
		shape[0] = new Triangle("red",4,5);
		shape[1] = new Rectangle("blue",4,5);
		for(Shape s: shape){
			System.out.println(s);
			System.out.println("Area is "+ s.getArea());
		}
	}
}
