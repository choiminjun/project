package nuna.main;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver{


   @Override
   public void onReceive(Context context, Intent intent) {
      // TODO Auto-generated method stub
      NotificationManager notifier = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
      Notification notify = new Notification();
      Intent intent2 = new Intent(context, Timetable_alarm.class);
      PendingIntent pender;// = PendingIntent.getActivity(context, 1, intent2, 0);
      
      Calendar calendar = new GregorianCalendar();

      if( calendar.get(Calendar.DAY_OF_WEEK)== 2 )
         pender = PendingIntent.getActivity(context, 2 , intent2, 0);
      else if( calendar.get(Calendar.DAY_OF_WEEK)== 3 )
         pender = PendingIntent.getActivity(context, 3, intent2, 0);
      else if( calendar.get(Calendar.DAY_OF_WEEK)== 4 )
         pender = PendingIntent.getActivity(context, 4, intent2, 0);
      else if( calendar.get(Calendar.DAY_OF_WEEK)== 5 )
         pender = PendingIntent.getActivity(context, 5, intent2, 0);
      else if( calendar.get(Calendar.DAY_OF_WEEK)== 6 )
         pender = PendingIntent.getActivity(context, 6, intent2, 0);

      notify.vibrate = new long[]{200,200,500,300};
      notifier.notify(1, notify);
      Intent i = new Intent(context, AlarmPage.class);

      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

      context.startActivity(i);

   }



}