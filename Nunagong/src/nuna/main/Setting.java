package nuna.main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Setting extends Activity {
	ToggleButton hideFree;
	HttpPost httppost;
	HttpResponse response, response_timetable;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	public String SERVER_ADDRESS = ServerAddress.APACHE_SERVER_ADDRESS;
	private String db_name = "timetable11.db";
	Button help;

	// Database를 생성 관리하는 클래스
	private Timetable_Helper helper;
	Editor toEdit;
	SharedPreferences sh_Pref;
	SQLiteDatabase db;
	Cursor cur;
	private boolean check = false;
	private Timetable_Helper dbHelper;
	public void sharedPreferences()
	   {
	      sh_Pref = getSharedPreferences("hide",MODE_PRIVATE);
	      toEdit = sh_Pref.edit();
	      toEdit.putBoolean("hideF", check);
	      toEdit.commit();
	   }
	public void applySharedPreferences()
	   {
	      sh_Pref = getSharedPreferences("hide", MODE_PRIVATE);
	      if(sh_Pref != null )
	      {
	    	  boolean temp = sh_Pref.getBoolean("hideF", false);
	    	  hideFree.setChecked(temp);

	      }
	   }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		hideFree = (ToggleButton) findViewById(R.id.toggleButton);
		help = (Button) findViewById(R.id.helper);
		

		hideFree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openDatabase();
				AlertDialog dialBox2 = createDialogBox2();
				dialBox2.show();
			}
		});
		applySharedPreferences();
		help.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent("help"));
				
			}
		});
	}

	private String userID = "";
	private String userPW = "";
	String timetable_id = "";

	private void openDatabase() {
		dbHelper = new Timetable_Helper(this);
		db = dbHelper.getWritableDatabase();
		cur = dbHelper.getAll();
		cur.moveToFirst();

		if (hideFree.isChecked())
			timetable_id = "99,";
		else if((cur != null) && (!cur.isAfterLast()))
		{
			timetable_id= cur.getInt(0) + ",";
			while (cur.moveToNext() && cur != null && (!cur.isAfterLast())) {
				timetable_id += cur.getInt(0) + ",";
			}
		}
		
		// Toast temp = Toast.makeText(this, timetable_id, 2);
		// temp.show();
	}
	private class LoginForY extends AsyncTask<String, Long, Void> {

		private ProgressDialog dialog = new ProgressDialog(Setting.this);

		protected void onPreExecute() {
			this.dialog.setMessage("Checking Login...");
			this.dialog.show();
		}

		protected Void doInBackground(String... params) {

			try {

				httpclient = new DefaultHttpClient();
				httppost = new HttpPost(SERVER_ADDRESS + "/logincheck.php");
				nameValuePairs = new ArrayList<NameValuePair>(2);

				userID = inputID.getText().toString();
				userPW = inputPW.getText().toString();

				nameValuePairs.add(new BasicNameValuePair("id", userID));
				nameValuePairs.add(new BasicNameValuePair("password", userPW));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				final String response = httpclient.execute(httppost,
						responseHandler);
				// login_state = true;
				// update timetable in database.

				System.out.println("Response:" + response);
				System.out.println("timetable:" + timetable_id);
				runOnUiThread(new Runnable() {
					public void run() {
						dialog.dismiss();
					}
				});
				if (response.equalsIgnoreCase("User Found"))
				{
					runOnUiThread(new Runnable() 
					{
						public void run() {
							Toast.makeText(Setting.this, "등록 완료!",
									Toast.LENGTH_SHORT).show();
							if(hideFree.isChecked())
								check = true;
							else
								check = false;
							sharedPreferences();

						}
					});

					// publishProgress();
					try {
						httpclient = new DefaultHttpClient();
						httppost = new HttpPost(SERVER_ADDRESS
								+ "/updatetimetable.php");
						nameValuePairs = new ArrayList<NameValuePair>(2);
						nameValuePairs
								.add(new BasicNameValuePair("id", userID));
						nameValuePairs.add(new BasicNameValuePair(
								"timetable_id", timetable_id));

						httppost.setEntity(new UrlEncodedFormEntity(
								nameValuePairs));
						response_timetable = httpclient.execute(httppost);
						// ResponseHandler<String> responseHandler = new
						// BasicResponseHandler();
						final String response_timetable = httpclient.execute(
								httppost, responseHandler);

						System.out.println("Response2:" + response_timetable);

					} catch (Exception e) {
						e.printStackTrace();
					}
				} 
				else {
					Toast.makeText(Setting.this, "로그인실패", Toast.LENGTH_LONG);
					runOnUiThread(new Runnable() 
					{
						public void run() {
							hideFree.setChecked(false);
							Toast.makeText(Setting.this, "로그인실패", Toast.LENGTH_LONG);
							if(hideFree.isChecked())
								check = true;
							else
								check = false;
							sharedPreferences();
						}
					});
				}

				// im.close();
				// return response;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				;
			}

			return null;
		}

		protected void onPostExecute(final Void value) {
			super.onPostExecute(value);
		}
	}
	private class LoginForN extends AsyncTask<String, Long, Void> {

		private ProgressDialog dialog = new ProgressDialog(Setting.this);

		protected void onPreExecute() {
			this.dialog.setMessage("Checking Login...");
			this.dialog.show();
		}

		protected Void doInBackground(String... params) {

			try {

				httpclient = new DefaultHttpClient();
				httppost = new HttpPost(SERVER_ADDRESS + "/logincheck.php");
				nameValuePairs = new ArrayList<NameValuePair>(2);

				userID = inputID.getText().toString();
				userPW = inputPW.getText().toString();

				nameValuePairs.add(new BasicNameValuePair("id", userID));
				nameValuePairs.add(new BasicNameValuePair("password", userPW));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				final String response = httpclient.execute(httppost,
						responseHandler);
				// login_state = true;
				// update timetable in database.

				System.out.println("Response:" + response);
				System.out.println("timetable:" + timetable_id);
				runOnUiThread(new Runnable() {
					public void run() {
						dialog.dismiss();
					}
				});
				if (response.equalsIgnoreCase("User Found"))
				{
					runOnUiThread(new Runnable() 
					{
						public void run() {
							Toast.makeText(Setting.this, "등록 완료!",
									Toast.LENGTH_SHORT).show();
							if(hideFree.isChecked())
								check = true;
							else
								check = false;
							sharedPreferences();

						}
					});
					// publishProgress();
					try {
						httpclient = new DefaultHttpClient();
						httppost = new HttpPost(SERVER_ADDRESS
								+ "/updatetimetable.php");
						nameValuePairs = new ArrayList<NameValuePair>(2);
						nameValuePairs
								.add(new BasicNameValuePair("id", userID));
						nameValuePairs.add(new BasicNameValuePair(
								"timetable_id", timetable_id));

						httppost.setEntity(new UrlEncodedFormEntity(
								nameValuePairs));
						response_timetable = httpclient.execute(httppost);
						// ResponseHandler<String> responseHandler = new
						// BasicResponseHandler();
						final String response_timetable = httpclient.execute(
								httppost, responseHandler);

						System.out.println("Response2:" + response_timetable);

					} catch (Exception e) {
						e.printStackTrace();
					}
				} 
				else {
					runOnUiThread(new Runnable() 
					{
						public void run() {
							hideFree.setChecked(true);
							Toast.makeText(Setting.this, "로그인실패", Toast.LENGTH_LONG);
							if(hideFree.isChecked())
								check = true;
							else
								check = false;
							sharedPreferences();
						}
					});
				}

				// im.close();
				// return response;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				;
			}

			return null;
		}

		protected void onPostExecute(final Void value) {
			super.onPostExecute(value);
		}
	}


	EditText inputID, inputPW;

	protected AlertDialog createDialogBox2() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this); // Get
		View view = this.getLayoutInflater().inflate(R.layout.dialog_login,
				null);
		builder.setTitle("로그인이 필요합니다");
		// LayoutInflater inflater = getLayoutInflater();
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		inputID = (EditText) view.findViewById(R.id.edtloginiddialbox);
		inputPW = (EditText) view.findViewById(R.id.edtloginpassworddialbox);
		builder.setView(view)
				// Add action buttons
				.setPositiveButton(R.string.login,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) { // sign
							if(hideFree.isChecked())
								new LoginForY().execute();
							else
								new LoginForN().execute();
								// new FindFriend().execute();
							}

						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// LoginDialogFragment.this.getDialog().cancel();
								if(hideFree.isChecked())
								{
									hideFree.setChecked(false);
									
								}
								else
								{
									hideFree.setChecked(true);
									
								}
								
							}
						});
		return builder.create();
	}

}
