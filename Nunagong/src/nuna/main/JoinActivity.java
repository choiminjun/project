package nuna.main;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import nuna.main.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class JoinActivity extends Activity {
	/**
	 * private static String myaddress = null; public Join(){ try { myaddress =
	 * InetAddress.getLocalHost().getHostAddress(); } catch
	 * (UnknownHostException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 */
	Button joinOk;
	EditText edtname, edtid, edtpass, edtphone;

	private static final String SERVER_ADDRESS = ServerAddress.APACHE_SERVER_ADDRESS;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join);
		ActionBar actionBar = getActionBar();
		actionBar.setTitle("회원가입");
		Drawable d1 = getResources().getDrawable(R.drawable.barimage);
		actionBar.setBackgroundDrawable(d1);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		joinOk = (Button) findViewById(R.id.okJoin);

		edtname = (EditText) findViewById(R.id.editName);
		edtid = (EditText) findViewById(R.id.editID);
		edtpass = (EditText) findViewById(R.id.editPW);
		edtphone = (EditText) findViewById(R.id.editPN);

		joinOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (edtname.getText().toString().equals("")
						|| edtid.getText().toString().equals("")
						|| edtpass.getText().toString().equals("")) {
					Toast.makeText(JoinActivity.this, "입력 오류 입니다. 다시 입력하세요",
							Toast.LENGTH_SHORT).show();
					return;
				}
				runOnUiThread(new Runnable() {
					public void run() {
						String name = edtname.getText().toString();
						String id = edtid.getText().toString();
						String password = edtpass.getText().toString();
						String phone = edtphone.getText().toString();

						try {
							URL url = new URL(SERVER_ADDRESS + "/join.php?"
									+ "name="
									+ URLEncoder.encode(name, "UTF-8") + "&id="
									+ URLEncoder.encode(id, "UTF-8")
									+ "&password="
									+ URLEncoder.encode(password, "UTF-8")
									+ "&phone="
									+ URLEncoder.encode(phone, "UTF-8"));

							url.openStream();
							// url.openConnection();
							String result = getXmlData("insertresult.xml",
									"result");

							if (result.equals("1")) {
								Toast.makeText(JoinActivity.this, "가입완료! 다시 등록하세요!",
										Toast.LENGTH_SHORT).show();

								edtname.setText("");
								edtid.setText("");
								edtpass.setText("");

								/**
								 * startActivity(new
								 * Intent(Join.this,Login.class));
								 */
								finish();
							} else {
								Toast.makeText(JoinActivity.this, "회원 가입실패!",
										Toast.LENGTH_SHORT).show();
							}
						} catch (Exception e) {
							Log.e("Error", e.getMessage());
						}// try catch종료.
					}// run 종료.
				});
			}
		});
	}// oncreate 종료.

	
	/**
	 * Get result data from insertresult.xml in mamp
	 */
	private String getXmlData(String filename, String str) {
		String rss = SERVER_ADDRESS + "/";
		String ret = "";				//this is for inserting value.
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			URL server = new URL(rss + filename);
			InputStream is = server.openStream();
			xpp.setInput(is, "UTF-8");

			int eventType = xpp.getEventType();

			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					if (xpp.getName().equals(str)) {
						ret = xpp.nextText();
					}
				}
				eventType = xpp.next();
			}
		} catch (Exception e) {
			Log.e("Error", e.getMessage());
		}
		return ret;
	}
}
