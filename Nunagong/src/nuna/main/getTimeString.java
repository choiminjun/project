package nuna.main;

import java.util.Calendar;

public class getTimeString {
	public static String getTime(String temp) {

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(calendar.DAY_OF_WEEK);
		int hour = calendar.get(calendar.HOUR_OF_DAY);
		int minute = calendar.get(calendar.MINUTE);
		String[] temptable = temp.split(",");
		int[] tempt = new int[10];
		int c = 0;
		
		for (int i = day - 1; i < 51; i += 5) 
		{
			for (int j = 0; j < temptable.length; j++)
			{
				if (i == Integer.parseInt(temptable[j])) 
				{
					tempt[c] = i;
					c++;
				}
			}
		}
		

		if (temp.equals("99"))
			return "비밀입니다";
		if (tempt[0] == 0)
			return "수업없는날!";

		int Lastindex = 0;
		for (int i = 0; i < 10; i++) {
			if (tempt[i] == 0) {
				Lastindex = i - 1;
				break;
			}
		}
		
		int tempclasshour = 0;
		int[] starth = new int[10];
		int[] noclassh = new int[10];
		for (int i = 0; i < 10; i++) {
			starth[i] = noclassh[i] = 0;
		}
		if (tempt[Lastindex] % 5 == 0)
			tempclasshour = (tempt[Lastindex] / 5) + 9;
		else
			tempclasshour = (tempt[Lastindex] / 5) + 10;
		if (hour >= tempclasshour)
			return "오늘수업끝!";
		else {
			for (int i = 0; i < 10; i++) {
				if (tempt[i] != 0) {
					if (tempt[i] % 5 == 0)
						starth[i] = (tempt[i] / 5) + 8;
					else
						starth[i] = (tempt[i] / 5) + 9;
				}

			}
			// make no class time array
			int x = 0, y = 0;
			for (int i = 9; i <= 18; i++) {
				if (i != starth[x]) {
					noclassh[y] = i;
					y++;
				} else
					x++;
			}
			int check = 0;

			// ////Algorithm start!
			String result = "";
			for (int i = 0; i < starth.length; i++) {
				if (starth[i] == hour) {
					// //class time
					check = 1;
					int tempindex = 0;
					for (int j = 0; j < 10; j++) {
						if (hour < noclassh[j]) {
							tempindex = j;
							break;
						}
					}
					int lefttime = (60 - minute)
							+ (60 * (noclassh[tempindex] - hour - 1));
					return "공강 까지 " + lefttime + " 분 남음";
				}
			}
			if (check == 0) {
				// no class time
				int tempindex = 0;
				for (int j = 0; j < 10; j++) {
					if (hour < starth[j]) {
						tempindex = j;
						break;
					}
				}
				int lefttime = (60 - minute)
						+ (60 * (starth[tempindex] - hour - 1));
				return "수업 시작 전까지 " + lefttime + " 분 남음";
			}
		}
		return "ERROR";
	}


}
