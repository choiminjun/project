package nuna.main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import nuna.main.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

@SuppressLint("NewApi")
public class Timetable extends Activity implements OnClickListener {
	HttpPost httppost;
	HttpResponse response, response_timetable;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	public String SERVER_ADDRESS = ServerAddress.APACHE_SERVER_ADDRESS;

	private final String tag = "timetable.class";

	private String db_name = "timetable11.db";

	private Timetable_Helper helper;
	SQLiteDatabase db;
	Cursor cur;
	private Timetable_Helper dbHelper;
	LinearLayout buttonlayout;
	LinearLayout lay[] = new LinearLayout[10];
	LinearLayout lay_time;
	String colors[] = { "#FF0000", "#8B0000", "#A52A2A", "#A0522D", "#CD5C5C",
			"#BC8F8F", "#F08080", "#FF7F50", "#FF6347", "#F4A460", "#FFA07A",
			"#CD853F", "#D2691E", "#FF4500", "#FFA500", "#FF8C00", "#D2B48C",
			"#FFDAB9", "#FFE4C4", "#FFDEAD", "#DEB887", "#B8860B", "#DAA520",
			"#FFD700", "#FFFF00", "#FAFAD2", "#EEE8AA", "#F0E68C", "#BDB76B",
			"#7CFC00", "#ADFF2F", "#32CD32", "#808000", "#6B8E23", "#2E8B57",
			"#3CB371", "#8FBC8F", "#90EE90", "#00FF7F", "#00FA9A", "#008080",
			"#20B2AA", "#B0E0E6", "#87CEFA", "#00CED1", "#6495ED", "#4169E1",
			"#6A5ACD", "#DDA0DD", "#D8BFD8", "#DA70D6" };
	Spinner chour;
	String chours[] = { "1시간", "2시간", "3시간", "4시간", "5시간", "6시간" };
	private int tempClassHour = 1;
	String time_line[] = { "1교시\n09:00", "2교시\n10:00", "3교시\n11:00",
			"4교시\n12:00", "5교시\n13:00", "6교시\n14:00", "7교시\n15:00",
			"8교시\n16:00", "9교시\n17:00", "10교시\n18:00" };
	String day_line[] = { "시간", "Mon", "Tue", "Wed", "Thu", "Fri" };

	TextView time[] = new TextView[time_line.length];
	TextView day[] = new TextView[day_line.length];
	TextView data[] = new TextView[(time_line.length * day_line.length) + 1];
	TextView db_data[] = new TextView[(time_line.length * day_line.length) + 1];

	EditText put_subject;
	EditText put_classroom;
	Button settingalarm;
	Button registertable;
	Button deletetable;
	// Drawable d1;
	int db_id;
	String db_classroom, db_subject;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timetable); // get layout file.

		ActionBar actionBar = getActionBar();
		actionBar.setTitle("시간표");
		Drawable d1 = getResources().getDrawable(R.drawable.barimage);
		actionBar.setBackgroundDrawable(d1);

		// get database path
		String dbPath = getApplicationContext().getDatabasePath(db_name)
				.getPath();

		Log.i("my db path=", "" + dbPath);

		// local database..
		helper = new Timetable_Helper(this);
		int counter = helper.getCounter();
		Log.i(tag, "counter = " + counter);

		@SuppressWarnings("deprecation")
		LayoutParams params_1 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);
		params_1.weight = 1;
		params_1.width = getLcdSizeWidth() / 6;
		params_1.height = getLcdSizeHeight() / 14;
		params_1.setMargins(1, 1, 1, 1);
		params_1.gravity = 1;

		@SuppressWarnings("deprecation")
		LayoutParams params_2 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);
		params_2.weight = 1;
		params_2.width = getLcdSizeWidth() / 6;
		params_2.height = getLcdSizeHeight() / 20;
		params_2.setMargins(1, 1, 1, 1);

		lay_time = (LinearLayout) findViewById(R.id.lay_time);
		lay[0] = (LinearLayout) findViewById(R.id.lay_0);
		lay[1] = (LinearLayout) findViewById(R.id.lay_1);
		lay[2] = (LinearLayout) findViewById(R.id.lay_2);
		lay[3] = (LinearLayout) findViewById(R.id.lay_3);
		lay[4] = (LinearLayout) findViewById(R.id.lay_4);
		lay[5] = (LinearLayout) findViewById(R.id.lay_5);
		lay[6] = (LinearLayout) findViewById(R.id.lay_6);
		lay[7] = (LinearLayout) findViewById(R.id.lay_7);
		lay[8] = (LinearLayout) findViewById(R.id.lay_8);
		lay[9] = (LinearLayout) findViewById(R.id.lay_9);

		// setting the timetable with array of day and time.
		for (int i = 0; i < day.length; i++) {
			day[i] = new TextView(this);
			day[i].setText(day_line[i]);
			day[i].setGravity(Gravity.CENTER);
			day[i].setBackgroundColor(Color.parseColor("#8099CCFF"));
			day[i].setTextSize(10);
			lay_time.addView(day[i], params_2);
		}

		for (int i = 0; i < time.length; i++) {
			time[i] = new TextView(this);
			time[i].setText(time_line[i]);
			time[i].setGravity(Gravity.CENTER);
			time[i].setBackgroundColor(Color.parseColor("#EAEAEA"));
			time[i].setTextSize(10);
			lay[i].addView(time[i], params_1);
		}
		settingalarm = (Button) findViewById(R.id.setalarms);
		registertable = (Button) findViewById(R.id.registable);
		deletetable = (Button) findViewById(R.id.deltable);
		settingalarm.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent("Timetable_alarm"));
			}
		});
		registertable.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				openDatabase();
				AlertDialog dialBox2 = createDialogBox2();
				dialBox2.show();
			}
		});
		deletetable.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AlertDialog dialBox = createDialogBox();
				dialBox.show();
			}
		});

		cur = helper.getAll(); // get all datas from Timetable_Helper class.
		cur.moveToFirst();
		// 시간표 그리기
		for (int i = 0, id = 1; i < lay.length; i++) {
			for (int j = 1; j < day_line.length; j++) {
				data[id] = new TextView(this);
				data[id].setId(id);// data[0] = 0
				data[id].setTextSize(10);
				data[id].setOnClickListener(this);
				data[id].setGravity(Gravity.CENTER);
				data[id].setBackgroundColor(Color.parseColor("#EAEAEA"));
				if ((cur != null) && (!cur.isAfterLast())) {
					db_id = cur.getInt(0);
					db_subject = cur.getString(1);
					db_classroom = cur.getString(2);
					if (data[id].getId() == db_id) {
						data[id].setText(db_subject + "\n" + db_classroom);
						if (id - 5 > 0) {
							/**
							 * If data id's are more than 5, 연속적인 과목의 시간표일 경우
							 * 과목의 제목을 이용하여 같을경우 요일기준 column을 따라가며 같은 색으로
							 * 그려준다(backgroundcolor).
							 */
							if (data[id].getText().toString()
									.equals(data[id - 5].getText().toString())) {
								for (int k = 0; k <= 10; k++) {
									if ((id - (5 * k) <= 0))
										break;
									else {
										if (data[id]
												.getText()
												.toString()
												.equals(data[id - (5 * k)]
														.getText().toString()))
											data[id].setBackgroundColor(Color
													.parseColor(colors[id
															- (5 * k)]));
									}
								}
							} else {
								data[id].setBackgroundColor(Color
										.parseColor(colors[id]));
							}
						} else {
							data[id].setBackgroundColor(Color
									.parseColor(colors[id]));
						}

						cur.moveToNext();
					}
				} else if (cur.isAfterLast()) {
					cur.close();
				}
				lay[i].addView(data[id], params_1); // Add the view after every
													// row has finished.
				id++;
			}// End of Second For
		}// End of First For
	}// End of OnCreate Method

	/*
	 * private void CreateMenu(Menu menu) { MenuItem mnu1 = menu.add(0, 0, 0,
	 * "Alarm"); mnu1.setIcon(R.drawable.alarms);
	 * mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM); MenuItem mnu2 =
	 * menu.add(0, 1, 1, "Setting"); mnu2.setIcon(R.drawable.delete);
	 * mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM); MenuItem mnu3 =
	 * menu.add(0, 2, 2, "Help"); mnu3.setIcon(R.drawable.registertimetable);
	 * mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM); }
	 * 
	 * private boolean MenuChoice(MenuItem item) { switch (item.getItemId()) {
	 * case 0: startActivity(new Intent("Timetable_alarm")); return true; case
	 * 1: AlertDialog dialBox = createDialogBox(); dialBox.show(); return true;
	 * case 2: Toast.makeText(this, "You clicked on Item 3", Toast.LENGTH_LONG)
	 * .show(); return true; } return false; }
	 */
	private AlertDialog createDialogBox() {
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
				// set message, title, and icon
				.setTitle("정말?")
				.setMessage("시간표를 모두 지우겠습니까?")
				.setIcon(R.drawable.question)
				// set three option buttons
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								for (int i = 1; i < 51; i++) {
									helper.delete(i);
									data[i].setText(null);
									data[i].setBackgroundColor(Color
											.parseColor("#EAEAEA"));
								}
							}
						})// setPositiveButton
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

					}
				})// setNegativeButton
				.create();
		return myQuittingDialogBox;
	}// createDialogBox

	public void onDestroy() {
		super.onDestroy();
		helper.close();
	}

	@Override
	public void onClick(View view) {
		Cursor cursor = null;
		cursor = helper.getAll(); // get from local database..
		int get[] = new int[51]; // allocate the array
		if (cursor != null) {
			Log.i(tag, "cursor is not null");
			cursor.moveToFirst();
			for (int i = 0; i < 51; i++) {
				get[i] = 0;
			}
			while (!cursor.isAfterLast()) {
				get[cursor.getInt(0)] = cursor.getInt(0);
				Log.i(tag, "get " + get[cursor.getInt(0)]);
				cursor.moveToNext();
			}
			for (int i = 1; i < 51; i++) {
				Log.i(tag,
						"get[i] =" + get[i] + "   view.getid =" + view.getId()
								+ "   data[i].getId() =" + data[i].getId());
				if ((get[i] == view.getId())) {
					update_timetable_dig(view.getId());
					break;
				} else if ((view.getId() == data[i].getId())) {
					add_timetable_dig(view.getId());
					break;
				}
			}// End of For
		}// End of if(cursor!=null)
	}// End of OnClick

	public void update_timetable_dig(final int id) {
		final LinearLayout lay = (LinearLayout) View.inflate(Timetable.this,
				R.layout.timetable_input_dig, null);

		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("수업 바꾸기");

		// ad.setIcon(R.drawable.timetable);
		ad.setView(lay);
		put_subject = (EditText) lay.findViewById(R.id.input_subject);
		put_classroom = (EditText) lay.findViewById(R.id.input_classroom);

		Cursor c;
		c = helper.getAll();
		if (c != null) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				if (c.getInt(0) == id) {
					put_subject.setText(c.getString(1));
					put_classroom.setText(c.getString(2));
					break;
				}
				c.moveToNext();
			}
		}
		put_subject.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				put_subject.setText(null);
			}
		});
		put_classroom.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				put_classroom.setText(null);
			}
		});
		ad.setPositiveButton("수정", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				int get_id = data[id].getId();
				helper.updatehw(get_id, put_subject.getText().toString(),
						put_classroom.getText().toString(), "");
				data[id].setText("" + put_subject.getText() + "\n"
						+ put_classroom.getText());
			}
		});
		ad.setNegativeButton("삭제", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				helper.delete(id);
				data[id].setText(null);
				data[id].setBackgroundColor(Color.parseColor("#EAEAEA"));
			}
		});
		ad.show();
	}// End of dialog

	public void add_timetable_dig(final int id) {
		final LinearLayout lay = (LinearLayout) View.inflate(Timetable.this,
				R.layout.timetable_input_dig, null);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("시간표");
		ad.setView(lay);
		chour = (Spinner) lay.findViewById(R.id.classhour);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, chours);
		chour.setAdapter(adapter);
		chour.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int index = chour.getSelectedItemPosition();
				tempClassHour = index + 1; // 수업시간 index는 0 부터 시작...
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				tempClassHour = 1;
			}
		});
		ad.setPositiveButton("저장", new DialogInterface.OnClickListener() {
			EditText put_subject = (EditText) lay
					.findViewById(R.id.input_subject);
			EditText put_classroom = (EditText) lay
					.findViewById(R.id.input_classroom);

			public void onClick(DialogInterface dialog, int which) {
				int get_id = data[id].getId();
				for (int i = 0; i < tempClassHour; i++)
					if (get_id + (5 * i) < 51) {
						if (data[get_id + (5 * i * 1)].getText().toString()
								.equals(""))
							helper.addhw(get_id + (5 * i), put_subject
									.getText().toString(), put_classroom
									.getText().toString(), "");
						else
							helper.updatehw(get_id + (5 * i), put_subject
									.getText().toString(), put_classroom
									.getText().toString(), "");
					}
				for (int i = 0; i < tempClassHour; i++) {
					if (get_id + (5 * i) < 51) {
						data[id + (5 * i)].setText("" + put_subject.getText()
								+ "\n" + put_classroom.getText());
						data[id + (5 * i)].setBackgroundColor(Color
								.parseColor(colors[id]));
					}
				}
			}
		});
		ad.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		ad.show();
	}// End of dialog

	@SuppressWarnings("deprecation")
	public int getLcdSizeWidth() {
		// TODO Auto-generated method stub
		return ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay().getWidth();
	}// End of getLcdSizeWidth Method

	@SuppressWarnings("deprecation")
	public int getLcdSizeHeight() {
		// TODO Auto-generated method stub
		return ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay().getHeight();
	}// End of getLcdSizeHeight Method

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		CreateMenu(menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private void CreateMenu(Menu menu) {
		MenuItem mnu1 = menu.add(0, 0, 0, "Timetable");
		mnu1.setTitle("시간표");
		mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu2 = menu.add(0, 1, 1, "AddHW");
		mnu2.setTitle("과제");
		mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu3 = menu.add(0, 2, 2, "Nunagong");
		mnu3.setTitle("너의공강");
		mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	private boolean MenuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			startActivity(new Intent("Timetable"));
			finish();
			return true;
		case 1:
			startActivity(new Intent("AddHW"));
			finish();
			return true;
		case 2:
			Intent intent = new Intent(Timetable.this,
					BetweenClassesActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return false;
	}

	private String userID = "";
	private String userPW = "";
	String timetable_id = "";

	private void openDatabase() {
		dbHelper = new Timetable_Helper(this);
		db = dbHelper.getWritableDatabase();
		cur = dbHelper.getAll();
		cur.moveToFirst();

		if ((cur != null) && (!cur.isAfterLast()))
			timetable_id = cur.getInt(0) + ",";
		while (cur.moveToNext() && cur != null && (!cur.isAfterLast())) {
			timetable_id += cur.getInt(0) + ",";
		}
	}

	private class Login extends AsyncTask<String, Long, Void> {

		private ProgressDialog dialog = new ProgressDialog(Timetable.this);

		protected void onPreExecute() {

			userID = inputID.getText().toString();
			userPW = inputPW.getText().toString();
			if (userID != null && userPW != null) {
				this.dialog.setMessage("Checking Login...");
				this.dialog.show();
				System.out.println(userID);
				
			} else {
				this.dialog.setMessage("로그인 정보를 바르게 입력하세요...");
				this.dialog.show();
			}
		}

		protected Void doInBackground(String... params) {

			try {

				httpclient = new DefaultHttpClient();
				httppost = new HttpPost(SERVER_ADDRESS + "/logincheck.php");
				nameValuePairs = new ArrayList<NameValuePair>(2);
				/*
				 * userID = inputID.getText().toString(); userPW =
				 * inputPW.getText().toString();
				 */
				nameValuePairs.add(new BasicNameValuePair("id", userID));
				nameValuePairs.add(new BasicNameValuePair("password", userPW));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				final String response = httpclient.execute(httppost,
						responseHandler);
				// login_state = true;
				// update timetable in database.

				System.out.println("Response:" + response);
				System.out.println("timetable:" + timetable_id);
				runOnUiThread(new Runnable() {
					public void run() {
						dialog.dismiss();
					}
				});
				if (response.equalsIgnoreCase("User Found")) {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(Timetable.this, "등록 완료!",
									Toast.LENGTH_SHORT).show();

						}
					});

					// publishProgress();
					try {
						httpclient = new DefaultHttpClient();
						httppost = new HttpPost(SERVER_ADDRESS
								+ "/updatetimetable.php");
						nameValuePairs = new ArrayList<NameValuePair>(2);
						nameValuePairs
								.add(new BasicNameValuePair("id", userID));
						nameValuePairs.add(new BasicNameValuePair(
								"timetable_id", timetable_id));

						httppost.setEntity(new UrlEncodedFormEntity(
								nameValuePairs));
						response_timetable = httpclient.execute(httppost);
						// ResponseHandler<String> responseHandler = new
						// BasicResponseHandler();
						final String response_timetable = httpclient.execute(
								httppost, responseHandler);
						//for testing..
						System.out.println("Response2:" + response_timetable);

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(Timetable.this, "등록 실패!",
									Toast.LENGTH_SHORT).show();

						}
					});
					/*
					 * Toast.makeText(Timetable.this, "등록 실패!",
					 * Toast.LENGTH_SHORT) .show();
					 */
				}

				/**
				 * //Thread.sleep(1000); String urlPath = SERVER_ADDRESS +
				 * "/logincheck.php"; HttpPost request = new HttpPost(urlPath);
				 * Vector<NameValuePair> nameValue = new
				 * Vector<NameValuePair>(); nameValue.add(new
				 * BasicNameValuePair("id", inputID.getText() .toString()));
				 * nameValue.add(new BasicNameValuePair("password", inputPW
				 * .getText().toString())); HttpEntity enty = new
				 * UrlEncodedFormEntity(nameValue, HTTP.UTF_8);
				 * request.setEntity(enty); HttpClient client = new
				 * DefaultHttpClient(); HttpResponse res =
				 * client.execute(request);
				 * 
				 * HttpEntity entityResponse = res.getEntity(); InputStream im =
				 * entityResponse.getContent(); BufferedReader reader = new
				 * BufferedReader( new InputStreamReader(im, HTTP.UTF_8));
				 * 
				 * String response = reader.readLine();
				 */

				// im.close();
				// return response;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				;
			}

			return null;
		}

		/**
		 * Response to publishProgress().
		 */

		/**
		 * protected void onProgressUpdate(Long... value) {
		 * super.onProgressUpdate(value); updatetimetable();
		 * 
		 * }
		 */

		/**
		 * private void updatetimetable() { try{ httpclient = new
		 * DefaultHttpClient(); httppost = new HttpPost(SERVER_ADDRESS +
		 * "/updatetimetable.php"); nameValuePairs = new
		 * ArrayList<NameValuePair>(2); nameValuePairs.add(new
		 * BasicNameValuePair("id", userID)); nameValuePairs .add(new
		 * BasicNameValuePair("timetable",timetable_id));
		 * 
		 * httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		 * response = httpclient.execute(httppost); ResponseHandler<String>
		 * responseHandler = new BasicResponseHandler(); final String response =
		 * httpclient.execute(httppost, responseHandler);
		 * 
		 * System.out.println("Response:" + response);
		 * 
		 * }catch(Exception e){ e.printStackTrace(); } }
		 */
		protected void onPostExecute(final Void value) {
			super.onPostExecute(value);
			/**
			 * if (value.equalsIgnoreCase("Find Success")) { runOnUiThread(new
			 * Runnable() { public void run() {
			 * Toast.makeText(BetweenClassesActivity.this, "Find Success",
			 * Toast.LENGTH_SHORT).show(); } }); } else {
			 * Toast.makeText(BetweenClassesActivity.this, "Find Fail",
			 * Toast.LENGTH_SHORT).show(); }
			 */
		}
	}

	/**
	 * if user clicks timetable.
	 */
	/**
	 * public void login(View v){ try{ mSoundManager.playSound(1); Intent intent
	 * = new Intent(getApplicationContext(),LoginActivity.class);
	 * startActivity(intent); }catch(RuntimeException e){ Toast.makeText(this,
	 * "Error", Toast.LENGTH_SHORT); } }
	 */
	EditText inputID, inputPW;

	protected AlertDialog createDialogBox2() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this); // Get
		View view = this.getLayoutInflater().inflate(R.layout.dialog_login,
				null);
		builder.setTitle("로그인이 필요합니다");
		// LayoutInflater inflater = getLayoutInflater();
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		inputID = (EditText) view.findViewById(R.id.edtloginiddialbox);
		inputPW = (EditText) view.findViewById(R.id.edtloginpassworddialbox);
		builder.setView(view)
				// Add action buttons
				.setPositiveButton(R.string.login,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) { // sign
								new Login().execute();

								// new FindFriend().execute();
							}

						})
				.setNeutralButton(R.string.join,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								startActivity(new Intent(Timetable.this,
										JoinActivity.class));
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// LoginDialogFragment.this.getDialog().cancel();
							}
						});
		return builder.create();
	}

} // END of Timetable Class
