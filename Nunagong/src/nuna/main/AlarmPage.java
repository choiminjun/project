package nuna.main;

import nuna.main.R;

import android.app.ActionBar;
import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

public class AlarmPage extends Activity{

   SoundPool snd;
   int id;
   MediaPlayer mp;
   int close = 0;
   public void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON|WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
      setContentView(R.layout.alarm);
      Button f = (Button)findViewById(R.id.finish);
      ActionBar actionBar = getActionBar();
		actionBar.hide();

      mp = MediaPlayer.create(this, R.raw.back);
      mp.setLooping(true);
      mp.start();
      
      f.setOnClickListener(new OnClickListener(){

         public void onClick(View v){
            //finishActivity(AlarmPage.class);
            mp.stop();
            mp.release();
            
               finish();

         }
      });

   }

}