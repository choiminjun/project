package nuna.main;

/**
 * 중앙서버에서 자신의 시간표와 친구의 시간표를 비교하여 공강시간을 체크하는 클래스이다.
 * 두 사람의 공강 시간을 구하는 원리 시간표의 각 행과 열에 각각 하나의 아이디를 주어 id:1,2,3,4,5..이런식으로 주어
 * 구하는 것이다.
 * 게다가 수업은 월화수목금 총 5일 이므로 월화수목금 각각의 시간표가 각 5씩 증가하기 때문에 상대적으로 쉽게 구현할 수 있었다.
 * 이 클래스의 주된 기능은 다음과 같다.
 * 
 * 
 * 1.친구의 공강시간 알기
 * 2.친구 추가
 * 3.친구 동기화(자신의 휴대폰에 저장된 휴대폰 번호를 이용하여 중앙 서버에 있는 번호와 비교하여 일치할 경우 친구가 자동으로 추가되는 방식).
 * 4.친구 삭제.( onLongClickListener)
 * 5.친구 전체 삭제
 * 6.전체 삭제.(onClickListener)
 */
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
//import nunamain package
import nuna.main.friendsdata.CustomAdapter;
import nuna.main.friendsdata.DLog;
import nuna.main.friendsdata.DbOpenHelper;
import nuna.main.friendsdata.InfoClass;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class BetweenClassesActivity extends Activity {
	public String SERVER_ADDRESS = ServerAddress.APACHE_SERVER_ADDRESS;
	Button addFriend;
	Button addallFriend;
	Button deleteallFriend;
	EditText friendname, friendphone;
	String name, phone;
	String[] cont = new String[2];
	HttpPost httppost;
	HttpResponse response, timetable_response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	/**
	 * Databases
	 */
	private static final String TAG = "DataBaseActivity";
	private DbOpenHelper mDbOpenHelper;
	private Cursor mCursor;
	private InfoClass mInfoClass;
	public ArrayList<InfoClass> mInfoArray;
	public CustomAdapter mAdapter;
	private Cursor phones;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friends);
		ActionBar actionBar = getActionBar();
		actionBar.setTitle("너의공강");
		Drawable d1 = getResources().getDrawable(R.drawable.barimage);
		actionBar.setBackgroundDrawable(d1);
		View friendsview = this.getLayoutInflater().inflate(R.layout.friends,
				null);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		addallFriend = (Button) findViewById(R.id.btn_addall);
		addFriend = (Button) findViewById(R.id.btn_add);
		deleteallFriend = (Button) findViewById(R.id.btn_deleteall);
		addFriend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog dialBox = createDialogBox();
				dialBox.show();
			}
		});
		addallFriend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new FindAllFriend().execute();
			}
		});
		deleteallFriend.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AlertDialog dialBox2 = createDialogBox2();
				dialBox2.show();
			}
			// Intent d = new Intent(); //testing..
		});

		/**
		 * DataBases Get friends data from local databases.
		 */
		setLayout();

		mDbOpenHelper = new DbOpenHelper(this);
		mDbOpenHelper.open(); // call method

		mInfoArray = new ArrayList<InfoClass>();

		mCursor = null;
		mCursor = mDbOpenHelper.getAllColumns();
		DLog.e(TAG, "COUNT = " + mCursor.getCount());
	} // oncreate dialog.

	public void onStart() {
		super.onStart();
		new StartFriend().execute(cont);
	}

	private class StartFriend extends AsyncTask<String, Long, Void> {
		private ProgressDialog dialog = new ProgressDialog(
				BetweenClassesActivity.this);

		protected void onPreExecute() {
			this.dialog.setMessage("Matching Friends...");
			this.dialog.setCanceledOnTouchOutside(false);
			this.dialog.show();
		}

		@Override
		protected Void doInBackground(String... params) {
			// get friends data by using phone number until the cursor ends.
			while (mCursor.moveToNext()) {
				String phone = mCursor.getString(mCursor
						.getColumnIndex("contact"));
				String name = mCursor.getString(mCursor.getColumnIndex("name"));
				try {
					httpclient = new DefaultHttpClient();
					httppost = new HttpPost(SERVER_ADDRESS
							+ "/getfriendtimetable.php");

					nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("name", name));
					nameValuePairs.add(new BasicNameValuePair("phonenumber",
							phone));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					timetable_response = httpclient.execute(httppost);
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					String timetable_response = httpclient.execute(httppost,
							responseHandler);
					// friend_timetable = response;
					// login_state = true;

					timetable_response = timetable_response.substring(0,
							timetable_response.length() - 1);       //because of comma.

					timetable_response = getTime(timetable_response); // calculate
																		// time..
					// for making ArrayList
					mInfoClass = new InfoClass(mCursor.getInt(mCursor
							.getColumnIndex("_id")), mCursor.getString(mCursor
							.getColumnIndex("name")), mCursor.getString(mCursor
							.getColumnIndex("contact")), timetable_response);
					mInfoArray.add(mInfoClass);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mCursor.close(); // when
			for (InfoClass i : mInfoArray) {
				DLog.d(TAG, "ID = " + i._id);
				DLog.d(TAG, "name = " + i.name);
				DLog.d(TAG, "contact = " + i.contact);
				DLog.d(TAG, "timetable = " + i.timetable);
			}
			// dialog.dismiss();

			mAdapter = new CustomAdapter(getBaseContext(), mInfoArray);
			runOnUiThread(new Runnable() {
				public void run() {
					mListView.setAdapter(mAdapter); // show friends left time
					dialog.dismiss();
				}
			});
			return null;
		}
	}

	private AlertDialog createDialogBox2() {
		AlertDialog confirmingDialog = new AlertDialog.Builder(this)
				.setTitle("아싸가되겠다!")
				.setMessage("모든 친구를 삭제 하시겠습니까?")
				.setPositiveButton("네ㅠㅠ",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								/**
								 * Local DB에 있는 모든친구 삭제.
								 */
								int count = 1;
								while (mDbOpenHelper.deleteColumn(count)) {
									count++;

								}
								finish();
								Toast.makeText(BetweenClassesActivity.this,
										count - 1 + " 명의 친구가 사라졌습니다...",
										Toast.LENGTH_LONG).show();
							}
						})
				.setNegativeButton("아닌데?",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								;// nothing happens...
							}
						}).create();
		return confirmingDialog;
	}

	/**
	 * cofirming one friend deletion....
	 * 
	 * @return
	 */
	private AlertDialog createDialogBox4(final int position) {
		InfoClass temp = mInfoArray.get(position);
		String name = temp.name; // friends name
		final String phone = temp.contact; // friends phone.

		AlertDialog confirmDialog = new AlertDialog.Builder(this)
				.setTitle("연락").setMessage(name + " 에게 연락 하시겠습니까?")
				.setPositiveButton("전화", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent callIntent = new Intent(Intent.ACTION_CALL);

						callIntent.setData(Uri.parse("tel:" + phone));

						startActivity(callIntent);
					}
				}).create();

		return confirmDialog;

	}

	private AlertDialog createDialogBox3(final int position) {
		AlertDialog confirmingDialog = new AlertDialog.Builder(this)
				.setTitle("싸웟어?")
				.setMessage("이 친구를 정말 삭제 하시겠습니까?")
				.setPositiveButton("그래", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						/**
						 * Local DB에 있는 친구중 선택된 친구 삭제.
						 */

						DLog.e(TAG, "position = " + position);
						// InfoClass t = mInfoArray.get(position);
						// int tID = t._id;
						boolean result = mDbOpenHelper
								.deleteColumn(position + 1); // position+
																// 1
						DLog.e(TAG, "result = " + result);
						// //////////////////////////////changed..
						/**
						 * 중간값이 지워질 경우 추후에 문제가 생길수 있기 때문에, 해당 column의 deletion에
						 * 영향받는 column의 id 값을 하나씩 줄여준다.
						 */
						// if deletion success..
						if (result) {
							for (int syncPosition = position; syncPosition < mInfoArray
									.size(); syncPosition++) {
								InfoClass temp = mInfoArray.get(syncPosition);
								mDbOpenHelper.updateColumn(temp._id, temp.name,
										temp.contact, temp.timetable);
							}
							/**
							 * Setting the InfoArray SQLLiteDatabase를 지움과 동시에
							 * Adapter에서도 제거 해준다.
							 */
							mInfoArray.remove(position);
							mAdapter.setArrayList(mInfoArray);
							mAdapter.notifyDataSetChanged();
							DLog.d(TAG,
									"Adapter count : " + mAdapter.getCount());
							DLog.d(TAG, "Cursor count : " + mCursor.getCount());
							for (InfoClass i : mInfoArray) {
								DLog.d(TAG, "ID = " + i._id);
								DLog.d(TAG, "name = " + i.name);
								DLog.d(TAG, "contact = " + i.contact);
								DLog.d(TAG, "timetable = " + i.timetable);
							}
							finish();
							Toast.makeText(BetweenClassesActivity.this,
									" 친구가 사라졌습니다..", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(getApplicationContext(), "지우기 실패.",
									Toast.LENGTH_LONG).show();
						}
						// return false;
					}
				})
				.setNegativeButton("잘못누름;",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								;// nothing happens...
							}
						}).create();
		return confirmingDialog;
	}

	/**
	 * show dialog box which to add friends.
	 * 
	 * @return
	 */
	protected AlertDialog createDialogBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this); // Get
		View view = this.getLayoutInflater().inflate(
				R.layout.dialog_addfriends, null);
		friendname = (EditText) view.findViewById(R.id.edtnamefriends);
		friendphone = (EditText) view.findViewById(R.id.edtphonenumberfriends);
		builder.setTitle("친구의 시간표를 등록하세요");
		builder.setView(view)
				// Add action buttons
				.setPositiveButton(R.string.addfriend,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) { // sign
								// startActivity(new
								// Intent(BetweenClassesActivity.this,JoinActivity.class));
								name = friendname.getText().toString();
								phone = friendphone.getText().toString();
								cont[0] = name;
								cont[1] = phone;
								new FindFriend().execute(cont);
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// LoginDialogFragment.this.getDialog().cancel();
							}
						});
		return builder.create();
	}

	private class FindFriend extends AsyncTask<String, Long, Void> {
		private ProgressDialog dialog = new ProgressDialog(
				BetweenClassesActivity.this);

		protected void onPreExecute() {
			this.dialog.setMessage("Finding Friends...");
			this.dialog.setCanceledOnTouchOutside(false);
			this.dialog.show();
		}

		@Override
		protected Void doInBackground(String... params) {
			try {
				// 이부분에서 연락처를 동기화 하고 반복문으로 유저를 검색
				String name1 = params[0];
				String phone1 = params[1];
				/**
				 * Local database에서 현제 저장되어있는 친구의 번호와 자신이 입력한 번호가 중복된 것이 있는지 찾
				 * 
				 */

				httpclient = new DefaultHttpClient();
				httppost = new HttpPost(SERVER_ADDRESS + "/friendcheck.php");

				nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("username", name1));

				nameValuePairs
						.add(new BasicNameValuePair("phonenumber", phone1));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				final String response = httpclient.execute(httppost,
						responseHandler);
				// login_state = true;

				System.out.println("Response:" + response);
				runOnUiThread(new Runnable() {
					public void run() {
						dialog.dismiss();
					}
				});

				if (response.equalsIgnoreCase("Find Success")) {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(BetweenClassesActivity.this,
									"다시 들어가세요", Toast.LENGTH_LONG).show();
							/**
							 * Inser info Databases in local.
							 */

							Cursor c = mDbOpenHelper.getAllColumns();
							c.moveToFirst();

							name = friendname.getText().toString();
							phone = friendphone.getText().toString();
							// System.out.println(name +"!!!!!!!!!!!"+phone);
							mDbOpenHelper.insertColumn(name, phone, "12345");
							/**
							 * temp mDbOpenHelper.insertColumn(
							 * name,phone,"12345");
							 */
							mInfoArray.clear();

							// doWhileCursorToArray();

							mAdapter.setArrayList(mInfoArray);
							mAdapter.notifyDataSetChanged();

							mCursor.close();

						}
					});
				} else {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(BetweenClassesActivity.this,
									"친구를 못찾았습니다. 다시 확인하세요", Toast.LENGTH_SHORT)
									.show();
						}
					});
				}
				// return response;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * Response to publishProgress().
		 */
		protected void onPostExecute(final Void value) {
			try {
				super.onPostExecute(value);
				finish();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private class FindAllFriend extends AsyncTask<String, Long, String[][]> {

		private ProgressDialog dialog = new ProgressDialog(
				BetweenClassesActivity.this);

		protected void onPreExecute() {
			this.dialog.setMessage("주소록 동기화중...");
			this.dialog.setCanceledOnTouchOutside(false);
			this.dialog.show();
		}

		@Override
		protected String[][] doInBackground(String... params) {
			// 핸드폰 번호많을 가져오기 위해서.. 집전화X.
			phones = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					null, null, null);
			phones.moveToFirst();
			String[][] contacts = new String[2][20]; // get name and phone
														// number.
			for (int i = 0; i < 20; i++) {
				contacts[0][i] = null;
				contacts[1][i] = null;
			}
			String Name = null;
			String phoneNumber = null;
			int c = 0;
			do {
				try {
					// 이부분에서 연락처를 동기화 하고 반복문으로 유저를 검색
					Name = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					phoneNumber = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					phoneNumber = phoneNumber.replaceAll("-", "");
					// System.out.println(phoneNumber +
					// "????!!!!??!?!?!?!?!!?");
					httpclient = new DefaultHttpClient();
					httppost = new HttpPost(SERVER_ADDRESS + "/friendcheck.php");

					nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs
							.add(new BasicNameValuePair("username", Name));

					nameValuePairs.add(new BasicNameValuePair("phonenumber",
							phoneNumber));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					response = httpclient.execute(httppost);
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					final String response = httpclient.execute(httppost,
							responseHandler);
					// login_state = true;

					System.out.println("Response:" + response);

					if (response.equalsIgnoreCase("Find Success")) {
						contacts[0][c] = Name;
						contacts[1][c] = phoneNumber;
						c++;
					} else {
						// System.out.println("FFFF" + phone);
					}

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} while (phones.moveToNext());
			phones.close();
			runOnUiThread(new Runnable() {
				public void run() {
					dialog.dismiss();

				}
			});

			return contacts;
		}

		@Override
		/**
		 * Response to publishProgress().
		 */
		/*
		 * protected void onProgressUpdate(Long... value){
		 * super.onProgressUpdate(value); dialog.dismiss(); }
		 */
		protected void onPostExecute(final String[][] temp) { // temp <---
																// contacts
																// returned in
																// dobackground..
			try {
				int index = 0;
				while (!temp[0][index].equals(null)) { // until the name ends..
					mDbOpenHelper.insertColumn(temp[0][index], temp[1][index],
							"12345");
					mInfoArray.clear();
					mAdapter.setArrayList(mInfoArray);
					mAdapter.notifyDataSetChanged(); // for listview..

					index++;
				}
				mCursor.close();

				Toast.makeText(BetweenClassesActivity.this, "다시 들어가세요",
						Toast.LENGTH_LONG).show();
				finish();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(BetweenClassesActivity.this, "다시 들어가세요",
						Toast.LENGTH_LONG).show();
				finish();
			}

		}
	}

	/**
	 * Database Activity.
	 */

	@Override
	protected void onDestroy() {
		mDbOpenHelper.close();
		super.onDestroy();
	}

	private ListView mListView;

	private void setLayout() {
		mListView = (ListView) findViewById(R.id.lv_list);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				AlertDialog dialBox4 = createDialogBox4(position);
				dialBox4.show();
			}

		});

		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				AlertDialog dialBox3 = createDialogBox3(position);
				dialBox3.show();

				return true;

			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		CreateMenu(menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private void CreateMenu(Menu menu) {
		MenuItem mnu1 = menu.add(0, 0, 0, "Timetable");
		mnu1.setTitle("시간표");
		mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu2 = menu.add(0, 1, 1, "AddHW");
		mnu2.setTitle("과제");
		mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu3 = menu.add(0, 2, 2, "Nunagong");
		mnu3.setTitle("너의공강");
		mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	private boolean MenuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			startActivity(new Intent("Timetable"));
			finish();
			return true;
		case 1:
			startActivity(new Intent("AddHW"));
			finish();
			return true;
		case 2:
			Intent intent = new Intent(BetweenClassesActivity.this,
					BetweenClassesActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return false;
	}

	public static String getTime(String temp) {

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(calendar.DAY_OF_WEEK);
		int hour = calendar.get(calendar.HOUR_OF_DAY);
		int minute = calendar.get(calendar.MINUTE);
		
		
		String[] temptable = temp.split(",");
		int[] tempt = new int[10];
		int c = 0;

		for (int i = day - 1; i < 51; i += 5) {
			for (int j = 0; j < temptable.length; j++) {
				if (i == Integer.parseInt(temptable[j])) {
					tempt[c] = i;
					c++;
				}
			}
		}

		if (temp.equals("99")) // when user wants to hide his/her schedule
			return "비밀입니다";
		if (tempt[0] == 0)
			return "수업없는날!";

		int Lastindex = 0;
		for (int i = 0; i < 10; i++) {
			if (tempt[i] == 0) {	//when meets finish time.
				Lastindex = i - 1;
				break;
			}
		}

		int tempclasshour = 0;
		int[] starth = new int[10];
		int[] noclassh = new int[10];
		for (int i = 0; i < 10; i++) {
			starth[i] = noclassh[i] = 0;
		}
		
		
		if (tempt[Lastindex] % 5 == 0)
			tempclasshour = (tempt[Lastindex] / 5) + 9;
		else
			tempclasshour = (tempt[Lastindex] / 5) + 10;
		
		if (hour >= tempclasshour)
			return "오늘수업끝!";
		else {
			for (int i = 0; i < 10; i++) {
				if (tempt[i] != 0) {
					if (tempt[i] % 5 == 0)
						starth[i] = (tempt[i] / 5) + 8;
					else
						starth[i] = (tempt[i] / 5) + 9;
				}
			}
			// make no class time array
			int x = 0, y = 0;
			for (int i = 9; i <= 18; i++) {
				if (i != starth[x]) {
					noclassh[y] = i;
					y++;
				} else
					x++;
			}
			int check = 0;

			for (int i = 0; i < starth.length; i++) {
				if (starth[i] == hour) {
					// //class time
					check = 1;
					int tempindex = 0;
					for (int j = 0; j < 10; j++) {
						if (hour < noclassh[j]) {
							tempindex = j;
							break;
						}
					}
					int lefttime = (60 - minute)
							+ (60 * (noclassh[tempindex] - hour - 1));
					return "공강 까지 " + lefttime + " 분 남음";
				}
			}
			if (check == 0) {
				// no class time
				int tempindex = 0;
				for (int j = 0; j < 10; j++) {
					if (hour < starth[j]) {
						tempindex = j;
						break;
					}
				}
				int lefttime = (60 - minute)
						+ (60 * (starth[tempindex] - hour - 1));
				return "수업 시작 전까지 " + lefttime + " 분 남음";
			}
		}
		return "ERROR";
	}
}
