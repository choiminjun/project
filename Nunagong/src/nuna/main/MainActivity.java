package nuna.main;
/**
 * Display which followed by splash activity.
 * And catchs user's click action.
 */
import nuna.main.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	Button btntimetable, btnaddassignment, btnclasses,btnpreferences;


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		btntimetable = (Button) findViewById(R.id.btnTimetable);
		btntimetable.setOnClickListener(this);
		btnaddassignment = (Button) findViewById(R.id.btnAddassignment);
		btnaddassignment.setOnClickListener(this);
		btnclasses = (Button) findViewById(R.id.btnClasses);
		btnclasses.setOnClickListener(this);
		btnpreferences = (Button) findViewById(R.id.btnPreference);
		btnpreferences.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		//When user clicks 시간표.
		if (v.getId() == btntimetable.getId()) {
			startActivity(new Intent("Timetable"));
		}
		//When user clicks 과제추가.
		if (v.getId() == btnaddassignment.getId()) {
			startActivity(new Intent("AddHW"));
		}
		//When user clicks 너의공강.
		if (v.getId() == btnclasses.getId()) {
				Intent intent = new Intent(MainActivity.this,
						BetweenClassesActivity.class);
				//BetweenClasses.class 화면으로 전환.
				startActivity(intent);
		}
		//When user clicks 설정.
		if (v.getId() == btnpreferences.getId()) {
			startActivity(new Intent("Setting"));
		}
	}
}
