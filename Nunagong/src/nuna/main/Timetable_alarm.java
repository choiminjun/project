package nuna.main;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

public class Timetable_alarm extends Timetable {
	String[] first_subjects = new String[51];
	int[] first_times = new int[5];
	int[] ids = new int[51];
	private final String a_tag = "timetable.class";

	String[] alarmHour = { "1시간", "2시간", "3시간", "4시간", "5시간", "6시간" };
	String[] alarmMinute = new String[60];

	Spinner hourSpinner;
	Spinner minuteSpinner;
	private String dName = "timetable.db";
	private Switch mySwitch[] = new Switch[5];
	String days[] = { "월", "화", "수", "목", "금" };
	String alarmTime = "";
	String tempAlarmTime = "";
	int firstTemp[];
	SharedPreferences sh_Pref;
	Editor toEdit;
	int indexT, indexM;
	Button removeAlarm;
	Button checkAlarm;

	// Boolean[] check = new Boolean[]{false, false,false,false,false};
	int m, t;

	String time1 = "";
	String time2 = "";
	String time3 = "";
	String time4 = "";
	String time5 = "";

	boolean a, b, c, d, e;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timetable_alarm);
		mySwitch[0] = (Switch) findViewById(R.id.alarm1);
		mySwitch[1] = (Switch) findViewById(R.id.alarm2);
		mySwitch[2] = (Switch) findViewById(R.id.alarm3);
		mySwitch[3] = (Switch) findViewById(R.id.alarm4);
		mySwitch[4] = (Switch) findViewById(R.id.alarm5);
		checkAlarm = (Button) findViewById(R.id.button1);
		removeAlarm = (Button) findViewById(R.id.remove);

		removeAlarm.setOnClickListener(this);
		checkAlarm.setOnClickListener(this);
		for (int i = 0; i < 5; i++)
			mySwitch[i].setOnClickListener(this);

		for (int i = 0; i < 60.; i++)
			alarmMinute[i] = "" + i + " 분";
		Cursor c = null;
		Timetable_Helper a_helper;
		String dPath = getApplicationContext().getDatabasePath(dName).getPath();
		Log.i("my db path=", "" + dPath);

		for (int i = 0; i < 51; i++) {
			ids[i] = -1;
			first_subjects[i] = "empty";
		}

		a_helper = new Timetable_Helper(this);
		int counter = a_helper.getCounter();
		Log.i(a_tag, "counter = " + counter);

		a_helper.search_data();
		c = a_helper.getAll(); //get all timetable datas from local databases..
		c.moveToFirst();
		for (int i = 0; i < 51; i++) { // 10
			if ((c != null) && (!c.isAfterLast())) {
				ids[i] = c.getInt(0);
				first_subjects[i] = c.getString(1);
				c.moveToNext();
			} else if (c.isAfterLast()) {
				c.close();
			}
		}// End of First For
		getFirstTimes();
		String tempTime = "";
		for (int i = 0; i < 5; i++) {
			if (first_times[i] != -1) {
				if (ids[first_times[i]] <= 5)
					tempTime = "09:00";
				else if (ids[first_times[i]] <= 10)
					tempTime = "10:00";
				else if (ids[first_times[i]] <= 15)
					tempTime = "11:00";
				else if (ids[first_times[i]] <= 20)
					tempTime = "12:00";
				else if (ids[first_times[i]] <= 25)
					tempTime = "13:00";
				else if (ids[first_times[i]] <= 30)
					tempTime = "14:00";
				else if (ids[first_times[i]] <= 35)
					tempTime = "15:00";
				else if (ids[first_times[i]] <= 40)
					tempTime = "16:00";
				else if (ids[first_times[i]] <= 45)
					tempTime = "17:00";
				else if (ids[first_times[i]] <= 50)
					tempTime = "18:00";
				mySwitch[i].setText(days[i] + " : "
						+ first_subjects[first_times[i]] + " -> " + tempTime);
			}
		}

		hourSpinner = (Spinner) findViewById(R.id.hour);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, alarmHour);
		hourSpinner.setAdapter(adapter);
		hourSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int index = hourSpinner.getSelectedItemPosition();
				tempAlarmTime = alarmHour[index];
				indexT = index;
				sharedPreferences();
			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		minuteSpinner = (Spinner) findViewById(R.id.minute);
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, alarmMinute);
		minuteSpinner.setAdapter(adapter1);
		minuteSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int index = minuteSpinner.getSelectedItemPosition();
				tempAlarmTime += alarmMinute[index];
				indexM = index;
				sharedPreferences();
			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		applySharedPreferences();
	}

	public void sharedPreferences() {
		sh_Pref = getSharedPreferences("Time", MODE_PRIVATE);
		toEdit = sh_Pref.edit();
		toEdit.putInt("time", indexT);
		toEdit.putInt("min", indexM);
		if (mySwitch[0].isChecked())
			toEdit.putBoolean("mon", true);
		else
			toEdit.putBoolean("mon", false);

		if (mySwitch[1].isChecked())
			toEdit.putBoolean("tue", true);
		else
			toEdit.putBoolean("tue", false);

		if (mySwitch[2].isChecked())
			toEdit.putBoolean("wed", true);
		else
			toEdit.putBoolean("wed", false);

		if (mySwitch[3].isChecked())
			toEdit.putBoolean("thu", true);
		else
			toEdit.putBoolean("thu", false);

		if (mySwitch[4].isChecked())
			toEdit.putBoolean("fri", true);
		else
			toEdit.putBoolean("fri", false);

		// //toEdit.putBoolean("tue", check[1]);
		// toEdit.putBoolean("wed", check[2]);
		// toEdit.putBoolean("thu", check[3]);
		// toEdit.putBoolean("fri", check[4]);
		toEdit.putString("f1", time1);
		toEdit.putString("f2", time2);
		toEdit.putString("f3", time3);
		toEdit.putString("f4", time4);
		toEdit.putString("f5", time5);

		toEdit.commit();

	}

	public void applySharedPreferences() {
		sh_Pref = getSharedPreferences("Time", MODE_PRIVATE);
		String t1 = "", t2 = "", t3 = "", t4 = "", t5 = "";
		if (sh_Pref != null) {
			t = sh_Pref.getInt("time", 0);
			m = sh_Pref.getInt("min", 0);
			a = sh_Pref.getBoolean("mon", false);
			b = sh_Pref.getBoolean("tue", false);
			c = sh_Pref.getBoolean("wed", false);
			d = sh_Pref.getBoolean("thu", false);
			e = sh_Pref.getBoolean("fri", false);
			t1 = sh_Pref.getString("f1", "");
			t2 = sh_Pref.getString("f2", "");
			t3 = sh_Pref.getString("f3", "");
			t4 = sh_Pref.getString("f4", "");
			t5 = sh_Pref.getString("f5", "");

			hourSpinner.setSelection(t);
			minuteSpinner.setSelection(m);
			mySwitch[0].setChecked(a);
			mySwitch[1].setChecked(b);
			mySwitch[2].setChecked(c);
			mySwitch[3].setChecked(d);
			mySwitch[4].setChecked(e);

			time1 = "";
			time2 = "";
			time3 = "";
			time4 = "";
			time5 = "";

			if (a == true)
				time1 += t1;
			if (b == true)
				time2 += t2;
			if (c == true)
				time3 += t3;
			if (d == true)
				time4 += t4;
			if (e == true)
				time5 += t5;
		}
	}

	public void onClick(View v) {

		int first;

		// Calendar calendar = new GregorianCalendar();
		Calendar original = Calendar.getInstance();
		original.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar3 = Calendar.getInstance();
		calendar3.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar4 = Calendar.getInstance();
		calendar4.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar5 = Calendar.getInstance();
		calendar5.setTimeInMillis(System.currentTimeMillis());

		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		int hours = original.get(Calendar.HOUR_OF_DAY);
		int minutes = original.get(Calendar.MINUTE);
		int tempHour = t;
		int tempMin = m;

		if (t == indexT && m == indexM) {
			tempHour = t;
			tempMin = m;
		} else {
			tempHour = indexT;
			tempMin = indexM;
		}
		if (v.getId() == mySwitch[0].getId()) {
			first = getFirstTimeForAlarm(ids[first_times[0]], first_times[0]);

			// get the time for alarm.
			if (tempMin == 0) {
				first = first - (tempHour + 1);
				tempMin = 0;
			} else {
				first = first - (tempHour + 2);
				tempMin = 60 - tempMin;
			}

			time1 = "";
			time1 += Integer.toString(first);
			time1 += " : ";
			time1 += Integer.toString(tempMin);

			Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 2, intent,
					0);

			if (mySwitch[0].isChecked()) {// 6
											// check[0] = true;
				calendar.set(Calendar.HOUR_OF_DAY, first);
				calendar.set(Calendar.MINUTE, tempMin);
				calendar.set(Calendar.SECOND, 0);

				long a = System.currentTimeMillis();
				long b = calendar.getTimeInMillis();

				if (original.get(Calendar.DAY_OF_WEEK) == 2) {
					if (a > b)
						b += 1000 * 60 * 60 * 24 * 7;
					
					Toast.makeText(getApplication(), "Alarm set on Monday",
							Toast.LENGTH_SHORT).show();
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);					
				} else {
					int date = original.get(Calendar.DAY_OF_WEEK) - 2;
					if (date < 0)
						date *= -1;

					b += 1000 * 60 * 60 * 24 * date;
					Toast.makeText(getApplication(), "Alarm set on Monday",
							Toast.LENGTH_SHORT).show();
					// am.setRepeating(AlarmManager.RTC_WAKEUP, b,
					// 1000*24*60*60*7, sender);
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				}
			} else {
				am.cancel(sender);
				// check[0] = false;
				Toast.makeText(getApplication(), "Alarm deleted on Monday",
						Toast.LENGTH_SHORT).show();
				time1 = "";
			}
			sharedPreferences();
		} else if (v.getId() == mySwitch[1].getId()) {
			first = getFirstTimeForAlarm(ids[first_times[1]], first_times[1]);

			// get the time for alarm.
			if (tempMin == 0) {
				first = first - (tempHour + 1);
				tempMin = 0;
			} else {
				first = first - (tempHour + 2);
				tempMin = 60 - tempMin;
			}

			time2 = "";
			time2 += Integer.toString(first);
			time2 += " : ";
			time2 += Integer.toString(tempMin);

			Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 3, intent,
					0);

			if (mySwitch[1].isChecked()) {// 6

				// check[1] = true;
				calendar2.set(Calendar.HOUR_OF_DAY, first);
				calendar2.set(Calendar.MINUTE, tempMin);
				calendar2.set(Calendar.SECOND, 0);

				long a = System.currentTimeMillis();
				long b = calendar2.getTimeInMillis();

				if (original.get(Calendar.DAY_OF_WEEK) == 3) {
					if (a > b)
						b += 1000 * 60 * 60 * 24 * 7;
					Toast.makeText(getApplication(), "Alarm set on Tuesday",
							Toast.LENGTH_SHORT).show();
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				} else {
					int date = original.get(Calendar.DAY_OF_WEEK) - 3;
					if (date < 0)
						date *= -1;

					b += 1000 * 60 * 60 * 24 * date;
					Toast.makeText(getApplication(), "Alarm set on Tuesday",
							Toast.LENGTH_SHORT).show();
					// am.setRepeating(AlarmManager.RTC_WAKEUP, b,
					// 1000*24*60*60*7, sender);
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				}
			} else {
				am.cancel(sender);
				// check[1] = false;
				Toast.makeText(getApplication(), "Alarm deleted on Tuesday",
						Toast.LENGTH_SHORT).show();
				time2 = "";
			}
			sharedPreferences();
		}

		else if (v.getId() == mySwitch[2].getId()) {
			first = getFirstTimeForAlarm(ids[first_times[2]], first_times[2]);

			// get the time for alarm.
			if (tempMin == 0) {
				first = first - (tempHour + 1);
				tempMin = 0;
			} else {
				first = first - (tempHour + 2);
				tempMin = 60 - tempMin;
			}
			time3 = "";
			time3 += Integer.toString(first);
			time3 += " : ";
			time3 += Integer.toString(tempMin);

			Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 4, intent,
					0);

			if (mySwitch[2].isChecked()) {// 6
											// check[2] = true;
				calendar3.set(Calendar.HOUR_OF_DAY, first);
				calendar3.set(Calendar.MINUTE, tempMin);
				calendar3.set(Calendar.SECOND, 0);

				long a = System.currentTimeMillis();
				long b = calendar3.getTimeInMillis();

				if (original.get(Calendar.DAY_OF_WEEK) == 4) {
					if (a > b)
						b += 1000 * 60 * 60 * 24 * 7;

					Toast.makeText(getApplication(), "Alarm set on Wednesday",
							Toast.LENGTH_LONG).show();
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				} else {
					int date = original.get(Calendar.DAY_OF_WEEK) - 4;
					if (date < 0)
						date *= -1;

					b += 1000 * 60 * 60 * 24 * date;
					Toast.makeText(getApplication(), "Alarm set on Wednesday",
							Toast.LENGTH_LONG).show();
					// am.setRepeating(AlarmManager.RTC_WAKEUP, b,
					// 1000*24*60*60*7, sender);
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				}
			} else {
				am.cancel(sender);
				// check[2] = false;
				time3 = "";
				Toast.makeText(getApplication(), "Alarm deleted on Wednesday",
						Toast.LENGTH_LONG).show();
			}
			sharedPreferences();

		} else if (v.getId() == mySwitch[3].getId()) {// 5
			first = getFirstTimeForAlarm(ids[first_times[3]], first_times[3]);

			// get the time for alarm.
			if (tempMin == 0) {
				first = first - (tempHour + 1);
				tempMin = 0;
			} else {
				first = first - (tempHour + 2);
				tempMin = 60 - tempMin;
			}
			// first = 23;
			// tempMin = 55;
			time4 = "";
			time4 += Integer.toString(first);
			time4 += " : ";
			time4 += Integer.toString(tempMin);

			Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 5, intent,
					0);

			if (mySwitch[3].isChecked()) {// 6
											// check[3] = true;
				calendar4.set(Calendar.HOUR_OF_DAY, first);
				calendar4.set(Calendar.MINUTE, tempMin);
				calendar4.set(Calendar.SECOND, 0);

				long a = System.currentTimeMillis();
				long b = calendar4.getTimeInMillis();

				if (original.get(Calendar.DAY_OF_WEEK) == 5) {
					if (a > b)
						b += 1000 * 60 * 60 * 24 * 7;

					Toast.makeText(getApplication(), "Alarm set on Thursday1",
							Toast.LENGTH_LONG).show();
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				} else {
					int date = original.get(Calendar.DAY_OF_WEEK) - 5;
					if (date < 0)
						date *= -1;

					b += 1000 * 60 * 60 * 24 * date;
					Toast.makeText(getApplication(), "Alarm set on Thursday",
							Toast.LENGTH_LONG).show();
					// am.setRepeating(AlarmManager.RTC_WAKEUP, b,
					// 1000*24*60*60*7, sender);
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				}
			} else {
				am.cancel(sender);
				// check[3] = false;
				Toast.makeText(getApplication(), "Alarm deleted on Thursday",
						Toast.LENGTH_LONG).show();
				time4 = "";
			}
			sharedPreferences();
		} else if (v.getId() == mySwitch[4].getId()) {
			first = getFirstTimeForAlarm(ids[first_times[4]], first_times[4]);

			// get the time for alarm.
			if (tempMin == 0) {
				first = first - (tempHour + 1);
				tempMin = 0;
			} else {
				first = first - (tempHour + 2);
				tempMin = 60 - tempMin;
			}
			// first = 11;
			// tempMin = 9;

			time5 = "";
			time5 += Integer.toString(first);
			time5 += " : ";
			time5 += Integer.toString(tempMin);

			Intent intent = new Intent(this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 6, intent,
					0);

			if (mySwitch[4].isChecked()) {// 6
											// check[4] = true;
				calendar5.set(Calendar.HOUR_OF_DAY, first);
				calendar5.set(Calendar.MINUTE, tempMin);
				calendar5.set(Calendar.SECOND, 0);

				long a = System.currentTimeMillis();
				long b = calendar5.getTimeInMillis();

				if (original.get(Calendar.DAY_OF_WEEK) == 6) {
					if (a > b)
						b += 1000 * 60 * 60 * 24 * 7;

					String d = Integer.toString(first);
					Toast.makeText(getApplication(), "Alarm set on Friday",
							Toast.LENGTH_LONG).show();
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				} else {
					int date = original.get(Calendar.DAY_OF_WEEK) - 6;
					if (date < 0)
						date *= -1;

					b += 1000 * 60 * 60 * 24 * date;
					String d = Integer.toString(first);
					Toast.makeText(getApplication(), "Alarm set on Friday",
							Toast.LENGTH_LONG).show();
					// am.setRepeating(AlarmManager.RTC_WAKEUP, b,
					// 1000*24*60*60*7, sender);
					am.setRepeating(AlarmManager.RTC_WAKEUP, b, 1000 * 24 * 60
							* 60 * 7, sender);
				}
			} else {
				am.cancel(sender);
				// check[4] = false;
				Toast.makeText(getApplication(), "Alarm deleted on Friday",
						Toast.LENGTH_LONG).show();
				time5 = "";
			}
			sharedPreferences();
		} else if (v.getId() == removeAlarm.getId()) {
			mySwitch[0].setChecked(false);
			mySwitch[1].setChecked(false);
			mySwitch[2].setChecked(false);
			mySwitch[3].setChecked(false);
			mySwitch[4].setChecked(false);
			// check[0] = false;
			// check[1] = false;
			// check[2] = false;
			// check[3] = false;
			// check[4] = false;
			Toast.makeText(getApplication(), "Alarms all deleted",
					Toast.LENGTH_SHORT).show();
			sharedPreferences();
		} else if (v.getId() == checkAlarm.getId()) {
			String alarm = "";

			if (time1.length() > 2)
				alarm += "Mon -> " + time1 + "\n";
			if (time2.length() > 2)
				alarm += "Tue -> " + time2 + "\n";
			if (time3.length() > 2)
				alarm += "Wed -> " + time3 + "\n";
			if (time4.length() > 2)
				alarm += "Thu -> " + time4 + "\n";
			if (time5.length() > 2)
				alarm += "Fri -> " + time5;

			if (alarm.length() < 2)
				alarm += "No alarm selected";
			new AlertDialog.Builder(this)
					.setMessage(alarm)
					.setTitle("Check Alarm")
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
								}
							}).show();
			sharedPreferences();
		}
		// sharedPreferences();
	}

	private Context getContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getFirstTimeForAlarm(int a, int b) {
		int fTime = 0;

		if (b != -1) {
			if (a <= 5)
				fTime = 9;
			else if (a <= 10)
				fTime = 10;
			else if (a <= 15)
				fTime = 11;
			else if (a <= 20)
				fTime = 12;
			else if (a <= 25)
				fTime = 13;
			else if (a <= 30)
				fTime = 14;
			else if (a <= 35)
				fTime = 15;
			else if (a <= 40)
				fTime = 16;
			else if (a <= 45)
				fTime = 17;
			else if (a <= 50)
				fTime = 18;
		}

		return fTime;
	}

	public void getFirstTimes() {
		int check = 0;
		for (int i = 1; i <= 46; i += 5) {
			for (int k = 0; k < 51; k++) {
				if (i == ids[k]) {
					first_times[0] = k;
					check = 1;
					break;
				}
			}
			if (check == 1)
				break;
		}
		check = 0;
		for (int i = 2; i <= 47; i += 5) {
			for (int k = 0; k < 51; k++) {
				if (i == ids[k]) {
					first_times[1] = k;
					check = 1;
					break;
				}
			}
			if (check == 1)
				break;
		}
		check = 0;
		for (int i = 3; i <= 48; i += 5) {
			for (int k = 0; k < 51; k++) {
				if (i == ids[k]) {
					first_times[2] = k;
					check = 1;
					break;
				}
			}
			if (check == 1)
				break;
		}
		check = 0;
		for (int i = 4; i <= 49; i += 5) {
			for (int k = 0; k < 51; k++) {
				if (i == ids[k]) {
					first_times[3] = k;
					check = 1;
					break;
				}
			}
			if (check == 1)
				break;
		}
		check = 0;
		for (int i = 5; i <= 50; i += 5) {
			for (int k = 0; k < 51; k++) {
				if (i == ids[k]) {
					first_times[4] = k;
					check = 1;
					break;
				}
			}
			if (check == 1)
				break;
		}
		check = 0;
	}
} // END of Timetable Class