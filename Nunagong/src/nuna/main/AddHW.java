package nuna.main;

import nuna.main.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AddHW extends Activity implements View.OnTouchListener{

	private final String tag = "timetable.class";
	private String db_name = "timetable11.db";
	private Timetable_Helper helper;
	String colors[] = { "#FF0000", "#8B0000", "#A52A2A", "#A0522D", "#CD5C5C",
			"#BC8F8F", "#F08080", "#FF7F50", "#FF6347", "#F4A460", "#FFA07A",
			"#CD853F", "#D2691E", "#FF4500", "#FFA500", "#FF8C00", "#D2B48C",
			"#FFDAB9", "#FFE4C4", "#FFDEAD", "#DEB887", "#B8860B", "#DAA520",
			"#FFD700", "#FFFF00", "#FAFAD2", "#EEE8AA", "#F0E68C", "#BDB76B",
			"#7CFC00", "#ADFF2F", "#32CD32", "#808000", "#6B8E23", "#2E8B57",
			"#3CB371", "#8FBC8F", "#90EE90", "#00FF7F", "#00FA9A", "#008080",
			"#20B2AA", "#B0E0E6", "#87CEFA", "#00CED1", "#6495ED", "#4169E1",
			"#6A5ACD", "#DDA0DD", "#D8BFD8", "#DA70D6" };
	SQLiteDatabase db;
	Cursor cur;
	int emptysub[] = new int [51];
	int emptyhw[] = new int [51];
	static int state = 10000;
	LinearLayout lay[] = new LinearLayout[10];
	LinearLayout lay_time;

	String time_line[]={"1교시\n09:00","2교시\n10:00","3교시\n11:00","4교시\n12:00","5교시\n13:00",
			"6교시\n14:00","7교시\n15:00","8교시\n16:00","9교시\n17:00","10교시\n18:00"};
	String day_line[]={"시간", "Mon","Tue","Wed","Thu","Fri"};

	TextView time[] = new TextView[time_line.length]; 
	TextView day[] = new TextView[day_line.length];
	TextView data[] = new TextView[(time_line.length * day_line.length)+1];
	TextView db_data[] = new TextView[(time_line.length * day_line.length)+1];

	Button add, del;
	String put_subject;
	String put_classroom;
	//EditText put_classroom;
	EditText put_hw;

	int db_id;
	String db_classroom,db_subject,db_hw;	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hwtable);
		ActionBar actionBar = getActionBar();
		actionBar.setTitle("과제추가");
		Drawable d1 = getResources().getDrawable(R.drawable.barimage);
		actionBar.setBackgroundDrawable(d1);

		String dbPath = getApplicationContext().getDatabasePath(db_name).getPath();
		Log.i("my db path=", ""+dbPath);

		helper = new Timetable_Helper(this); 
		int counter = helper.getCounter();
		Log.i(tag,"counter = "+counter);

		add = (Button)findViewById(R.id.addAssign);
		del= (Button)findViewById(R.id.delAssign);
		for(int u = 1; u < 51; u++)
		{
			emptysub[u] = 0;
			emptyhw[u] = 0;
		}
		//helper.search_datahw();

		@SuppressWarnings("deprecation")
		LayoutParams params_1 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);  
		params_1.weight = 1; 
		params_1.width=getLcdSizeWidth()/6; 
		params_1.height=getLcdSizeHeight()/14;
		params_1.setMargins(1, 1, 1, 1);
		params_1.gravity=1;

		@SuppressWarnings("deprecation")
		LayoutParams params_2 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);
		params_2.weight = 1; 
		params_2.width=getLcdSizeWidth()/6; 
		params_2.height=getLcdSizeHeight()/20;
		params_2.setMargins(1, 1, 1, 1);


		lay_time = (LinearLayout)findViewById(R.id.lay_time);
		lay[0] = (LinearLayout)findViewById(R.id.lay_0);
		lay[1] = (LinearLayout)findViewById(R.id.lay_1);
		lay[2] = (LinearLayout)findViewById(R.id.lay_2);
		lay[3] = (LinearLayout)findViewById(R.id.lay_3);
		lay[4] = (LinearLayout)findViewById(R.id.lay_4);
		lay[5] = (LinearLayout)findViewById(R.id.lay_5);
		lay[6] = (LinearLayout)findViewById(R.id.lay_6);
		lay[7] = (LinearLayout)findViewById(R.id.lay_7);
		lay[8] = (LinearLayout)findViewById(R.id.lay_8);
		lay[9] = (LinearLayout)findViewById(R.id.lay_9);


		for (int i = 0; i < day.length; i++) {
			day[i] = new TextView(this);
			day[i].setText(day_line[i]);
			day[i].setGravity(Gravity.CENTER);
			day[i].setBackgroundColor(Color.parseColor("#FAF4C0"));
			day[i].setTextSize(10);
			lay_time.addView(day[i], params_2);
		}

		for (int i = 0; i < time.length; i++) {
			time[i] = new TextView(this);
			time[i].setText(time_line[i]);
			time[i].setGravity(Gravity.CENTER);
			time[i].setBackgroundColor(Color.parseColor("#EAEAEA"));
			time[i].setTextSize(10);
			lay[i].addView(time[i],params_1);
		}

		cur =  helper.getAll();
		cur.moveToFirst();
	
		for (int i = 0, id=1; i < lay.length; i++) { 
			for (int j = 1; j <day_line.length; j++) {
				data[id] = new TextView(this);
				data[id].setId(id);//data[0]  =  0
				data[id].setTextSize(10);
				data[id].setOnTouchListener(this);
				data[id].setOnLongClickListener(new OnLongClickListener(){
					public boolean onLongClick(View view)
					{	
						Cursor cursor = null;
						cursor = helper.getAll();

						int get[] = new int[51];
						if(cursor!=null){
							Log.i(tag, "cursor is not null");
							cursor.moveToFirst();
							for(int i=0;i<51;i++){
								get[i]=0;
							}

							while(!cursor.isAfterLast()){
								get[cursor.getInt(0)] = cursor.getInt(0); 
								Log.i(tag, "get "+get[cursor.getInt(0)]);
								cursor.moveToNext();
							}

							for(int i=1;i<51;i++){
								Log.i(tag, "get[i] ="+get[i]+ 
										"   view.getid ="+view.getId()+ 
										"   data[i].getId() ="+data[i].getId());

								if((view.getId() == data[i].getId()) ){
									showhw(view.getId());
									break;

								}
							}
							//End of For
						}//End of   if(cursor!=null)
						return true;

					}
				});

				data[id].setGravity(Gravity.CENTER);
				data[id].setBackgroundColor(Color.parseColor("#EAEAEA"));
				
				if((cur!=null) && (!cur.isAfterLast())){
					db_id = cur.getInt(0);
					db_subject = cur.getString(1);
					db_hw = cur.getString(3);
					if(data[id].getId()==db_id){
						data[id].setText(db_subject+"\n"+db_hw);
						if(!db_subject.equals(""))
							emptysub[id] = 1;
						if(!db_hw.equals(""))
						{
							emptyhw[id] = 1;
							data[id].setBackgroundColor(Color.parseColor(colors[id]));
						}
						cur.moveToNext();
					}
				}
				else if(cur.isAfterLast()){
					cur.close();
				}
				lay[i].addView(data[id], params_1); 
				id++;
				
			}//End of Second For
		}//End of First For
		
		// For Button handle
		add.setOnClickListener(new OnClickListener(){
			public void onClick(View v)
			{
				if( state != 10000 && emptysub[state]==1){
					update_timetable_dig(state);
					
				}
				state = 10000;
			}
		});
		
		del.setOnClickListener(new OnClickListener(){
			public void onClick(View v)
			{
				if(state != 10000)
					delete(state);
				state = 10000;
			}
		});
		
	
	}//End of OnCreate Method

	public void onDestroy(){
		super.onDestroy();
		helper.close();
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		// TODO Auto-generated method stub
		for( int i = 1; i < 51; i++)
		{
			if(emptyhw[i] == 1)
			  data[i].setBackgroundColor(Color.parseColor(colors[i]));
			else
				data[i].setBackgroundColor(Color.parseColor("#EAEAEA"));
		}
		
		Cursor cursor = null;
		cursor = helper.getAll();

		int get[] = new int[51];
		if(cursor!=null){
			Log.i(tag, "cursor is not null");
			cursor.moveToFirst();
			for(int i=0;i<51;i++){
				get[i]=0;
			}

			while(!cursor.isAfterLast()){
				get[cursor.getInt(0)] = cursor.getInt(0); 
				//Log.i(tag, "get "+get[cursor.getInt(0)]);
				cursor.moveToNext();
			}

			for(int i=1;i<51;i++){
				//Log.i(tag, "get[i] ="+get[i]+ 
				//		"   view.getid ="+view.getId()+ 
				//		"   data[i].getId() ="+data[i].getId());

	
				if((view.getId() == data[i].getId()) ){
					
					data[i].setBackgroundColor(Color.parseColor("#99CCFF"));
					state = i;
					//break;
					return false;
				}
			}
			
			//End of For
		}//End of   if(cursor!=null)
		
		return true;

	}
	public void delete(final int id)
	{
		final LinearLayout lay = (LinearLayout)
				View.inflate(AddHW.this, R.layout.showhw, null);
	
		TextView hw = (TextView)lay.findViewById(R.id.show);
	
		Cursor c;
		c = helper.getAll();
		if(c!=null){
			c.moveToFirst();
			while(!c.isAfterLast()){

				if(c.getInt(0)== id){ 
					String subjectName = c.getString(1);
					String subjectClass = c.getString(2);
					helper.delete(id);
					helper.addhw(id, subjectName, subjectClass, "");
					helper.updatehw(id, subjectName, subjectClass, "");
					data[id].setText(c.getString(1));
					emptyhw[id] = 0;
					break;
				}
				c.moveToNext();
			}
		}
	//	hw.setText(put_hw.toString());
	}
	public void showhw(final int id)
	{
		final LinearLayout lay = (LinearLayout)
				View.inflate(AddHW.this, R.layout.showhw, null);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("과제노트");
		///ad.setIcon(R.drawable.timetable);
		ad.setView(lay);
		
		ad.show();
		

		TextView hw = (TextView)lay.findViewById(R.id.show);
		put_hw = (EditText)lay.findViewById(R.id.input_hw);
		Cursor c;
		c = helper.getAll();
		if(c!=null){
			c.moveToFirst();
			while(!c.isAfterLast()){

				if(c.getInt(0)== id){ 
					hw.setText(c.getString(3));
				//	put_subject.setText(c.getString(1));
					//put_classroom.setText(c.getString(2));
					break;
				}
				c.moveToNext();
			}
		}
	}
	public void update_timetable_dig(final int id){
		final LinearLayout lay = (LinearLayout)
				View.inflate(AddHW.this, R.layout.addhomework, null);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("과제넣기");
		//ad.setIcon(R.drawable.timetable);
		ad.setView(lay);
	    

		put_hw = (EditText)lay.findViewById(R.id.input_hw);
		Cursor c;
		c = helper.getAll();
		if(c!=null){
			c.moveToFirst();
			while(!c.isAfterLast()){

				if(c.getInt(0)== id){ 

					put_subject=c.getString(1);
					put_classroom=c.getString(2);
					break;
				}
				c.moveToNext();
			}
		}

		ad.setPositiveButton("store", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				int get_id = data[id].getId();  
				helper.updatehw(get_id,put_subject,put_classroom,put_hw.getText().toString());
			//	helper.addhw(get_id,put_subject,put_classroom,put_hw.getText().toString());
				emptyhw[id] = 1;
				data[id].setText(""+put_subject+ "\n" + put_hw.getText());
				data[id].setBackgroundColor(Color.parseColor(colors[id]));
			}});
		ad.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			
			}});
		ad.show();
	}//End of dialog 

	public void add_timetable_dig(final int id){
		
		final LinearLayout lay = (LinearLayout)View.inflate
				(AddHW.this, R.layout.addhomework, null);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("과제넣기");
		//ad.setIcon(R.drawable.timetable);
		ad.setView(lay);
		
		ad.setPositiveButton("store", new DialogInterface.OnClickListener() {
			//EditText put_subject = (EditText)lay.findViewById(R.id.input_subject);
			EditText put_hw = (EditText)lay.findViewById(R.id.input_hw);	
			public void onClick(DialogInterface dialog, int which) {
			
				int get_id = data[id].getId(); 
				String [] temp = new String[10];
				temp = put_hw.getText().toString().split(" ");
				helper.updatehw(get_id,put_subject,put_classroom,temp[0]);
				emptyhw[id] = 1;
				data[id].setText(""+put_subject+ "\n" + put_hw.getText());
				data[id].setBackgroundColor(Color.parseColor(colors[id]));
			}
		});
	
		ad.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		ad.show();
	}//End of dialog 

	
	@SuppressWarnings("deprecation")
	public int getLcdSizeWidth() {
		// TODO Auto-generated method stub
		return  ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
	}//End of getLcdSizeWidth Method

	@SuppressWarnings("deprecation")
	public int getLcdSizeHeight() {
		// TODO Auto-generated method stub
		return ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
	}//End of getLcdSizeHeight Method
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		CreateMenu(menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Timetable");
				mnu1.setTitle("시간표");
		mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu2 = menu.add(0, 1, 1, "AddHW");
				mnu2.setTitle("과제");
		mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu3 = menu.add(0, 2, 2, "Nunagong");
				mnu3.setTitle("너의공강");
		mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
			startActivity(new Intent("Timetable"));
			finish();
			return true;
		case 1:
			startActivity(new Intent("AddHW"));
			finish();
			return true;
		case 2:
			Intent intent = new Intent(AddHW.this,
					BetweenClassesActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return false;
	}


} //END of Timetable Class
