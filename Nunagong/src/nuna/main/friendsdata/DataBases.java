
package nuna.main.friendsdata;


import android.provider.BaseColumns;

// DataBase Table
public final class DataBases {
	
	public static final class CreateDB implements BaseColumns{
		public static final String NAME = "name";
		public static final String CONTACT = "contact";
		public static final String TIMETABLE = "timetable";
		public static final String _TABLENAME = "friend42";
		public static final String _CREATE = 
			"create table "+_TABLENAME+"(" 
					+_ID+" integer primary key, " 	
					+NAME+" text not null , " 
					+CONTACT+" text not null unique, " 
					+TIMETABLE+" text not null "
					
					+" );";
	
	}
	
}