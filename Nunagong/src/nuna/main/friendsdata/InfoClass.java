package nuna.main.friendsdata;


public class InfoClass {
	public int _id;
	public String name;
	public String contact;
	public String timetable;
	
	public InfoClass(){}
	
	public InfoClass(int _id , String name , String contact , String timetable){
		this._id = _id;
		this.name = name;
		this.contact = contact;
		this.timetable = timetable;
	}
	
}
