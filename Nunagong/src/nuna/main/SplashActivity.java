package nuna.main;
/**
 * Splash Activity which starts when user clicks app.
 */


import nuna.main.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class SplashActivity extends Activity {
	private static String TAG = SplashActivity.class.getName();
	private static long SLEEP_TIME = 3; // Sleep for some time
	AnimationDrawable mframeAnimation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Removes title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Removes notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash);
		// Start timer and launch main activity
		startAnimation();
		IntentLauncher launcher = new IntentLauncher();
		launcher.start();
	}

	private class IntentLauncher extends Thread {
		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */
		public void run() {
			try {
				// Sleeping
				
				Thread.sleep(SLEEP_TIME * 1000);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			// Start main activity
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			SplashActivity.this.startActivity(intent);
			SplashActivity.this.finish();
		}
	}
	private void startAnimation() {
		ImageView img = (ImageView) findViewById(R.id.sp);
		BitmapDrawable frame1 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage1);
		BitmapDrawable frame2 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage2);
		BitmapDrawable frame3 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage3);
		BitmapDrawable frame4 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage4);
		BitmapDrawable frame5 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage5);
		BitmapDrawable frame6 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage6);
		BitmapDrawable frame7 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage7);
		BitmapDrawable frame8 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage8);
		BitmapDrawable frame9 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage9);
		BitmapDrawable frame10 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage10);
		BitmapDrawable frame11= (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage11);
		BitmapDrawable frame12 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage12);
		BitmapDrawable frame13 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage13);
		BitmapDrawable frame14 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage14);
		BitmapDrawable frame15 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage15);
		BitmapDrawable frame16 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage16);
		BitmapDrawable frame17 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage17);
		BitmapDrawable frame18 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.splashimage18);
		
		// Get the background, which has been compiled to an AnimationDrawable
		// object.
		int reasonableDuration = 100;
		mframeAnimation = new AnimationDrawable();
		mframeAnimation.setOneShot(true); // loop continuously
		mframeAnimation.addFrame(frame1, reasonableDuration);
		mframeAnimation.addFrame(frame2, reasonableDuration);
		mframeAnimation.addFrame(frame3, reasonableDuration);
		mframeAnimation.addFrame(frame4, reasonableDuration);
		mframeAnimation.addFrame(frame5, reasonableDuration);
		mframeAnimation.addFrame(frame6, reasonableDuration);
		mframeAnimation.addFrame(frame7, reasonableDuration);
		mframeAnimation.addFrame(frame8, reasonableDuration);
		mframeAnimation.addFrame(frame9, reasonableDuration);
		mframeAnimation.addFrame(frame10, reasonableDuration);
		mframeAnimation.addFrame(frame11, reasonableDuration);
		mframeAnimation.addFrame(frame12, reasonableDuration);
		mframeAnimation.addFrame(frame13, reasonableDuration);
		mframeAnimation.addFrame(frame14, reasonableDuration);
		mframeAnimation.addFrame(frame15, reasonableDuration);
		mframeAnimation.addFrame(frame16, reasonableDuration);
		mframeAnimation.addFrame(frame17, reasonableDuration);
		mframeAnimation.addFrame(frame18, 300);
		img.setBackgroundDrawable(mframeAnimation);
		mframeAnimation.setVisible(true, true);
		mframeAnimation.start();
	}
	
}